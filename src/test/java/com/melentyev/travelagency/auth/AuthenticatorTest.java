package com.melentyev.travelagency.auth;

import com.melentyev.travelagency.entity.User;
import com.melentyev.travelagency.service.UserService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.Matchers.anyInt;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class AuthenticatorTest {

    @Mock
    private HttpServletRequest request;
    @Mock
    private HttpSession session;
    @Mock
    private UserService userService;

    private User user = new User("login", "password", "first", "second", "email");

    @Before
    public void setUp() throws Exception {
        Authenticator.userService = userService;
    }

    @Test
    public void testAuthorization() throws Exception {
        when(request.getSession()).thenReturn(session);
        Authenticator.authorization(request, user);
        verify(session).setAttribute("User", user);
    }

    @Test
    public void testGetUserFromSession() {
        when(request.getSession(false)).thenReturn(session);
        when(session.getAttribute("User")).thenReturn(user);
        User returnUser = Authenticator.getUserFromSession(request);
        verify(request).getSession(false);
        verify(session).getAttribute("User");
        assertTrue(returnUser.equals(user));
    }

    @Test
    public void testGetUserFromNullSession() throws Exception {
        when(request.getSession(false)).thenReturn(null);
        User returnUser = Authenticator.getUserFromSession(request);
        verify(request).getSession(false);
        assertTrue(returnUser == null);
    }

    @Test
    public void testIsNullUserValid() throws Exception {
        assertFalse(Authenticator.isUserValid(null));
    }

    @Test
    public void testNoExistUserInDBValid() throws Exception {
        when(userService.getUser(anyInt())).thenReturn(null);
        assertFalse(Authenticator.isUserValid(user));
    }

    @Test
    public void testBlockedUserValid() throws Exception {
        user.setBlocked(true);
        when(userService.getUser(anyInt())).thenReturn(user);
        assertFalse(Authenticator.isUserValid(user));
    }
}