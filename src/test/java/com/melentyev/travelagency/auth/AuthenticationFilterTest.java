package com.melentyev.travelagency.auth;

import com.melentyev.travelagency.entity.User;
import com.melentyev.travelagency.exception.DaoSystemException;
import com.melentyev.travelagency.service.UserService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import javax.servlet.FilterChain;
import javax.servlet.RequestDispatcher;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class AuthenticationFilterTest {

    private static final String PROFILE_PAGE = "/admin";
    private static final String TOURS_PAGE = "/tours";
    private static final String LOGIN_PAGE = "/login";
    private static final String REG_PAGE = "/signup";
    private static final String ERROR_MSG = "errorMsg";
    private static final String USER_BLOCKED = "userBlocked";
    private static final String ERROR_PAGE = "/error.jsp";

    @Mock
    private HttpServletRequest request;
    @Mock
    private HttpSession session;
    @Mock
    private HttpServletResponse response;
    @Mock
    private FilterChain chain;
    @Mock
    private UserService userService;
    @Mock
    private RequestDispatcher dispatcher;

    private AuthenticationFilter authenticationFilter = new AuthenticationFilter();
    private User user = new User("login", "password", "first", "second", "email");

    @Before
    public void setUp() throws Exception {
        Authenticator.userService = userService;
        when(request.getSession()).thenReturn(session);
        when(request.getSession(false)).thenReturn(session);
        when(request.getRequestDispatcher(anyString())).thenReturn(dispatcher);
    }

    @Test
    public void testUnAuthUserAccessToAdmin() throws Exception {
        when(session.getAttribute("User")).thenReturn(null);
        when(request.getRequestURI()).thenReturn(PROFILE_PAGE);
        authenticationFilter.doFilter(request, response, chain);
        verify(response).sendRedirect(LOGIN_PAGE);
    }

    @Test
    public void testUnAuthUserAccessToPublicPage() throws Exception {
        when(session.getAttribute("User")).thenReturn(null);
        when(request.getRequestURI()).thenReturn(TOURS_PAGE);
        authenticationFilter.doFilter(request, response, chain);
        verify(chain).doFilter(request, response);
    }

    @Test
    public void testAuthUserAccessToAdmin() throws Exception {
        when(session.getAttribute("User")).thenReturn(user);
        when(userService.getUser(anyInt())).thenReturn(user);
        when(request.getRequestURI()).thenReturn(PROFILE_PAGE);
        authenticationFilter.doFilter(request, response, chain);
        verify(chain).doFilter(request, response);
    }

    @Test
    public void testAuthUserAccessToPublicPage() throws Exception {
        when(session.getAttribute("User")).thenReturn(user);
        when(userService.getUser(anyInt())).thenReturn(user);
        when(request.getRequestURI()).thenReturn(TOURS_PAGE);
        authenticationFilter.doFilter(request, response, chain);
        verify(chain).doFilter(request, response);
    }

    @Test
    public void testAuthUserAccessToLogin() throws Exception {
        when(session.getAttribute("User")).thenReturn(user);
        when(userService.getUser(anyInt())).thenReturn(user);
        when(request.getRequestURI()).thenReturn(LOGIN_PAGE);
        authenticationFilter.doFilter(request, response, chain);
        verify(response).sendRedirect(TOURS_PAGE);
    }

    @Test
    public void testAuthUserAccessToRegPage() throws Exception {
        when(session.getAttribute("User")).thenReturn(user);
        when(userService.getUser(anyInt())).thenReturn(user);
        when(request.getRequestURI()).thenReturn(REG_PAGE);
        authenticationFilter.doFilter(request, response, chain);
        verify(response).sendRedirect(TOURS_PAGE);
    }


    @Test
    public void testAuthBlockedUserAccessToPage() throws Exception {
        user.setBlocked(true);
        when(session.getAttribute("User")).thenReturn(user);
        when(userService.getUser(anyInt())).thenReturn(user);
        when(request.getRequestURI()).thenReturn(PROFILE_PAGE);
        authenticationFilter.doFilter(request, response, chain);
        verify(session).invalidate();
        verify(request).setAttribute(ERROR_MSG, USER_BLOCKED);
        verify(request).getRequestDispatcher(ERROR_PAGE);
        verify(dispatcher).forward(request, response);
    }

    @Test
    public void testUserBlockedException() throws Exception {
        user.setBlocked(true);
        when(session.getAttribute("User")).thenReturn(user);
        when(userService.getUser(anyInt())).thenThrow(new DaoSystemException("Some error"));
        when(request.getRequestURI()).thenReturn(PROFILE_PAGE);
        authenticationFilter.doFilter(request, response, chain);
        verify(session).invalidate();
        verify(request).setAttribute(ERROR_MSG, USER_BLOCKED);
        verify(request).getRequestDispatcher(ERROR_PAGE);
        verify(dispatcher).forward(request, response);
    }

}