package com.melentyev.travelagency.filter;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import javax.servlet.FilterChain;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Map;

import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class FlashScopeFilterTest {
    private static final String FLASH_SESSION_KEY = "FLASH_SESSION_KEY";
    private static final String FLASH_SCOPE_REQUEST_ATTRIBUTE_PREFIX = "flashScope.";

    @Mock
    private HttpServletRequest request;
    @Mock
    private HttpSession session;
    @Mock
    private HttpServletResponse response;
    @Mock
    private FilterChain chain;

    private FlashScopeFilter flashScopeFilter;

    @Before
    public void setUp() throws Exception {
        when(request.getSession(false)).thenReturn(session);
        flashScopeFilter = new FlashScopeFilter();
    }

    @Test
    public void testSaveEmptySessionAttributeToRequest() throws Exception {
        when(session.getAttribute(FLASH_SESSION_KEY)).thenReturn(null);
        flashScopeFilter.saveSessionAttributeToRequest(request);
        verify(request).getSession(false);
        verify(session).getAttribute(FLASH_SESSION_KEY);
        verifyNoMoreInteractions(session);
        verifyNoMoreInteractions(request);
    }

    @Test
    public void testSaveSessionAttributeToRequest() throws Exception {
        Map<String, Object> map = new HashMap<>();
        map.put("some", "req");
        map.put("some2", "req2");
        when(session.getAttribute(FLASH_SESSION_KEY)).thenReturn(map);
        flashScopeFilter.saveSessionAttributeToRequest(request);
        verify(request).getSession(false);
        verify(session).getAttribute(FLASH_SESSION_KEY);
        verify(request).setAttribute("some", "req");
        verify(request).setAttribute("some2", "req2");
        verify(session).removeAttribute(FLASH_SESSION_KEY);
        verifyNoMoreInteractions(session);
        verifyNoMoreInteractions(request);
    }

    @Test
    public void testSaveEmptyRequestAttributeToSession() throws Exception {
        Hashtable<String, String> hashTable = new Hashtable<>();
        when(request.getAttributeNames()).thenReturn(hashTable.keys());
        flashScopeFilter.saveRequestAttributeToSession(request);
        verify(request).getAttributeNames();
        verifyNoMoreInteractions(session);
        verifyNoMoreInteractions(request);
    }

    @Test
    public void testSaveRequestAttributeToSession() throws Exception {
        Hashtable<String, String> hashtable = new Hashtable<>();
        hashtable.put("some", "req");
        hashtable.put("some2", "req2");
        hashtable.put("flashScope.s", "www");
        hashtable.put("flashScope.s2", "qqq");
        Map<String, Object> result = new HashMap<>();
        result.put("s", "www");
        result.put("s2", "qqq");
        when(request.getAttributeNames()).thenReturn(hashtable.keys());
        when(request.getAttribute("flashScope.s")).thenReturn("www");
        when(request.getAttribute("flashScope.s2")).thenReturn("qqq");
        flashScopeFilter.saveRequestAttributeToSession(request);
        verify(request).getAttributeNames();
        verify(request, times(2)).getAttribute(startsWith(FLASH_SCOPE_REQUEST_ATTRIBUTE_PREFIX));
        verify(request).getSession(false);
        verify(session).setAttribute(FLASH_SESSION_KEY, result);
        verifyNoMoreInteractions(session);
        verifyNoMoreInteractions(request);
    }

    @Test
    public void testFlashScopeFilter() throws Exception {
        Hashtable<String, String> hashtable = new Hashtable<>();
        hashtable.put("some", "req");
        hashtable.put("some2", "req2");
        hashtable.put("flashScope.s", "www");
        hashtable.put("flashScope.s2", "qqq");
        Map<String, Object> result = new HashMap<>();
        result.put("s", "www");
        result.put("s2", "qqq");
        when(request.getAttributeNames()).thenReturn(hashtable.keys());
        when(request.getAttribute("flashScope.s")).thenReturn("www");
        when(request.getAttribute("flashScope.s2")).thenReturn("qqq");
        when(session.getAttribute(FLASH_SESSION_KEY)).thenReturn(null);
        flashScopeFilter.doFilter(request, response, chain);
        verify(request, times(2)).getSession(false);
        verify(session).getAttribute(FLASH_SESSION_KEY);
        verify(chain).doFilter(request, response);
        verify(request).getAttributeNames();
        verify(request, times(2)).getAttribute(startsWith(FLASH_SCOPE_REQUEST_ATTRIBUTE_PREFIX));
        verify(request, times(2)).getSession(false);
        verify(session).setAttribute(FLASH_SESSION_KEY, result);
        verifyNoMoreInteractions(session);
        verifyNoMoreInteractions(request);
        verifyNoMoreInteractions(response);
        verifyNoMoreInteractions(chain);
    }
}