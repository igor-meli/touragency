package com.melentyev.travelagency.filter;

import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import javax.servlet.FilterConfig;
import java.util.Arrays;
import java.util.Hashtable;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@RunWith(Parameterized.class)
public class LangFilterRemoveLocaleFromPathTest {

    private static LangFilter langFilter = new LangFilter();
    @Parameterized.Parameter(0)
    public String input;

    @Parameterized.Parameter(1)
    public String expectedValue;

    @Parameterized.Parameters
    public static Iterable<Object[]> data() {
        return Arrays.asList(new Object[][]{{"/", "/"}, {"", ""}, {"/en/some/file", "/some/file"}, {"/en/", "/"},
                {"/en", ""}});
    }

    @BeforeClass
    public static void setUpClass() throws Exception {
        FilterConfig filterConfig = mock(FilterConfig.class);
        Hashtable<String, String> languages = new Hashtable<>();
        languages.put("en", "en");
        languages.put("ru", "ru");
        when(filterConfig.getInitParameterNames()).thenReturn(languages.elements());
        langFilter.init(filterConfig);
    }

    @Test
    public void testGetLocaleFromPath() {
        Assert.assertEquals(expectedValue, langFilter.removeLangDir(input));
    }

}