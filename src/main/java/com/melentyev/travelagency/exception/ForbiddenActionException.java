package com.melentyev.travelagency.exception;

public class ForbiddenActionException extends DaoBusinessException {

    public ForbiddenActionException(String message) {
        super(message);
    }

    public ForbiddenActionException(String message, Throwable cause) {
        super(message, cause);
    }

}
