package com.melentyev.travelagency.auth;

import com.melentyev.travelagency.entity.User;
import com.melentyev.travelagency.exception.DaoSystemException;
import com.melentyev.travelagency.service.UserService;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

/**
 * Util class for checking user auth privileges
 */
public class Authenticator {

    protected static UserService userService = new UserService();

    public static void authorization(HttpServletRequest request, User user) {
        HttpSession session = request.getSession();
        session.setAttribute("User", user);
    }

    public static boolean isUserValid(User originUser) throws DaoSystemException {
        if (originUser == null)
            return false;
        User user = userService.getUser(originUser.getId());
        return user != null && !user.isBlocked();
    }

    public static User getUserFromSession(HttpServletRequest request) {
        HttpSession session = request.getSession(false);
        return session != null ? (User) session.getAttribute("User") : null;
    }

}
