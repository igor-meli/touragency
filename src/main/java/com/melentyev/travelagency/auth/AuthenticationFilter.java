package com.melentyev.travelagency.auth;

import com.melentyev.travelagency.entity.User;
import com.melentyev.travelagency.exception.DaoSystemException;
import com.melentyev.travelagency.filter.BaseFilter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.DispatcherType;
import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebFilter(urlPatterns = "/*", dispatcherTypes = {DispatcherType.FORWARD, DispatcherType.REQUEST})
public class AuthenticationFilter extends BaseFilter {

    private static final Logger logger = LoggerFactory.getLogger(AuthenticationFilter.class);

    @Override
    public void doFilter(HttpServletRequest request, HttpServletResponse response, FilterChain chain) throws IOException, ServletException {
        String url = request.getRequestURI();
        User user = Authenticator.getUserFromSession(request);

        if (!isUserAuthorized(user)) {
            if (isForbiddenUrl(url)) {
                logger.debug("Unauthorized access request");
                response.sendRedirect("/login");
            } else {
                chain.doFilter(request, response);
            }
        } else {
            if (isUserBlocked(user)) {
                logger.debug("User account #" + user.getId() + " is blocked");
                request.getSession().invalidate();
                request.setAttribute("errorMsg", "userBlocked");
                request.getRequestDispatcher("/error.jsp").forward(request, response);
            } else if (url.startsWith("/login") || url.startsWith("/signup")) {
                response.sendRedirect("/tours");
            } else {
                chain.doFilter(request, response);
            }
        }
    }

    private boolean isUserBlocked(User user) {
        try {
            return !Authenticator.isUserValid(user);
        } catch (DaoSystemException e) {
            logger.warn("ERROR: ", e);
            return true;
        }
    }

    private boolean isUserAuthorized(User user) {
        return user != null;
    }

    private boolean isForbiddenUrl(String uri) {
        return uri.startsWith("/admin") || uri.startsWith("/tours/book");
    }

}
