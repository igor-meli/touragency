package com.melentyev.travelagency.auth;

/**
 *  Set of String constants to be used for checking User Rights
 */
public class RightsAction {

    public static final String CHANGE_HOTNESS = "cngHot";
    public static final String CHANGE_STATUS = "cngStatus";
    public static final String CHANGE_DISCOUNT = "cngDiscount";
    public static final String BAN = "banUser";
    public static final String ADD_TOUR = "addTour";

}
