package com.melentyev.travelagency.db.dao.discountprogram;

import com.melentyev.travelagency.db.dao.Dao;
import com.melentyev.travelagency.entity.DiscountProgram;
import com.melentyev.travelagency.exception.DaoSystemException;

public interface DiscountProgramDao extends Dao<DiscountProgram> {

    DiscountProgram getByOrderId(int id) throws DaoSystemException;

    DiscountProgram getByName(String name) throws DaoSystemException;

}
