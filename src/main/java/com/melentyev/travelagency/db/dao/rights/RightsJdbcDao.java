package com.melentyev.travelagency.db.dao.rights;

import com.melentyev.travelagency.db.dao.AbstractJdbcDao;
import com.melentyev.travelagency.db.enricher.RightsEnricher;
import com.melentyev.travelagency.db.extractor.rights.RightsExtractor;
import com.melentyev.travelagency.entity.Rights;
import com.melentyev.travelagency.exception.DaoSystemException;
import com.melentyev.travelagency.util.SQLStorage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.List;

public class RightsJdbcDao extends AbstractJdbcDao<Rights> implements RightsDao {

    private static final Logger logger = LoggerFactory.getLogger(RightsJdbcDao.class);

    @Override
    public Rights getById(int id) throws DaoSystemException {
        throw new UnsupportedOperationException();
    }

    @Override
    public List<Rights> getAll() throws DaoSystemException {
        throw new UnsupportedOperationException();
    }

    @Override
    public void add(Rights entity) throws DaoSystemException {
        throw new UnsupportedOperationException();
    }

    @Override
    public void deleteById(int id) {
        throw new UnsupportedOperationException();
    }

    @Override
    public void update(Rights entity) throws DaoSystemException {
        throw new UnsupportedOperationException();
    }

    @Override
    public List<Rights> getByRoleId(int id) throws DaoSystemException {
        String sql = SQLStorage.getSql("rights.select.by.role.id");
        try (PreparedStatement ps = getConnection().prepareStatement(sql)) {
            ps.setInt(1, id);
            return executeQuery(ps, new RightsExtractor(), new RightsEnricher());
        } catch (SQLException e) {
            logger.warn("Can't handle sql ['" + sql + "']; Message: " + e);
            throw new DaoSystemException("Can't handle sql ['" + sql + "']", e);
        }
    }
}
