package com.melentyev.travelagency.db.dao.action;

import com.melentyev.travelagency.db.dao.Dao;
import com.melentyev.travelagency.entity.Action;
import com.melentyev.travelagency.exception.DaoSystemException;

public interface ActionDao extends Dao<Action> {

    Action getActionByRightsId(int id) throws DaoSystemException;

}
