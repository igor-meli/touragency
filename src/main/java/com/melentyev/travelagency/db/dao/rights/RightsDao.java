package com.melentyev.travelagency.db.dao.rights;

import com.melentyev.travelagency.db.dao.Dao;
import com.melentyev.travelagency.entity.Rights;
import com.melentyev.travelagency.exception.DaoSystemException;

import java.util.List;

public interface RightsDao extends Dao<Rights> {

    List<Rights> getByRoleId(int id) throws DaoSystemException;

}
