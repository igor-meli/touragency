package com.melentyev.travelagency.db.dao.tour;

import com.melentyev.travelagency.db.dao.Dao;
import com.melentyev.travelagency.entity.Tour;
import com.melentyev.travelagency.exception.DaoSystemException;

import java.util.List;

public interface TourDao extends Dao<Tour> {

    List<Tour> getAll(int offset, int rows) throws DaoSystemException;

    int getToursCount() throws DaoSystemException;

    List<Tour> searchTours(String sql, List<Integer> params) throws DaoSystemException;

    Tour getToursByOfferId(int id) throws DaoSystemException;

    Tour getByName(String name) throws DaoSystemException;

}
