package com.melentyev.travelagency.db.dao.country;

import com.melentyev.travelagency.db.dao.AbstractJdbcDao;
import com.melentyev.travelagency.db.extractor.CountryJdbcExtractor;
import com.melentyev.travelagency.entity.Country;
import com.melentyev.travelagency.exception.DaoSystemException;
import com.melentyev.travelagency.util.SQLStorage;

import java.util.List;

public class CountryJdbcDao extends AbstractJdbcDao<Country> implements CountryDao {

    @Override
    public Country getById(int id) throws DaoSystemException {
        return getById(id, SQLStorage.getSql("country.select.by.id"), new CountryJdbcExtractor());
    }

    @Override
    public List<Country> getAll() throws DaoSystemException {
        return getAll(SQLStorage.getSql("country.select.all"), new CountryJdbcExtractor());
    }

    @Override
    public void add(Country entity) throws DaoSystemException {
        throw new UnsupportedOperationException();
    }

    @Override
    public void deleteById(int id) throws DaoSystemException {
        throw new UnsupportedOperationException();
    }

    @Override
    public void update(Country entity) throws DaoSystemException {
        throw new UnsupportedOperationException();
    }

    @Override
    public Country getByCityId(int id) throws DaoSystemException {
        return getById(id, SQLStorage.getSql("country.select.by.city.id"), new CountryJdbcExtractor());
    }
}
