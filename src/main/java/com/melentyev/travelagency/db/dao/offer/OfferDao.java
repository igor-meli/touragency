package com.melentyev.travelagency.db.dao.offer;

import com.melentyev.travelagency.db.dao.Dao;
import com.melentyev.travelagency.entity.Offer;
import com.melentyev.travelagency.exception.DaoSystemException;

import java.util.List;

public interface OfferDao extends Dao<Offer> {

    List<Offer> getOffersByTourId(int tourId) throws DaoSystemException;

    Offer getOfferByOrderId(int id) throws DaoSystemException;


    Offer getByHotelIdTourIdAndRoomId(int hotelId, int tourId, int roomId) throws DaoSystemException;
}
