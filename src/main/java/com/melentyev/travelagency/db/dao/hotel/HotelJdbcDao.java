package com.melentyev.travelagency.db.dao.hotel;

import com.melentyev.travelagency.db.dao.AbstractJdbcDao;
import com.melentyev.travelagency.db.enricher.HotelEnricher;
import com.melentyev.travelagency.db.extractor.hotel.HotelJdbcExtractor;
import com.melentyev.travelagency.entity.Hotel;
import com.melentyev.travelagency.exception.DaoSystemException;
import com.melentyev.travelagency.util.SQLStorage;

import java.util.List;

public class HotelJdbcDao extends AbstractJdbcDao<Hotel> implements HotelDao {

    @Override
    public Hotel getById(int id) throws DaoSystemException {
        return getById(id, SQLStorage.getSql("hotel.select.by.id"), new HotelJdbcExtractor(), new HotelEnricher());
    }

    @Override
    public List<Hotel> getAll() throws DaoSystemException {
        return getAll(SQLStorage.getSql("hotel.select.all"), new HotelJdbcExtractor(), new HotelEnricher());
    }

    @Override
    public void add(Hotel entity) throws DaoSystemException {
        throw new UnsupportedOperationException();
    }

    @Override
    public void deleteById(int id) throws DaoSystemException {
        throw new UnsupportedOperationException();
    }

    @Override
    public void update(Hotel entity) throws DaoSystemException {
        throw new UnsupportedOperationException();
    }

    @Override
    public Hotel getByRoomTypeId(int id) throws DaoSystemException {
        return getById(id, SQLStorage.getSql("hotel.select.by.roomtype.id"), new HotelJdbcExtractor(), new HotelEnricher());
    }
}
