package com.melentyev.travelagency.db.dao.tour;

import com.melentyev.travelagency.db.dao.AbstractJdbcDao;
import com.melentyev.travelagency.db.extractor.tour.TourByIdJdbcExtractor;
import com.melentyev.travelagency.db.extractor.tour.TourJdbcExtractor;
import com.melentyev.travelagency.entity.Tour;
import com.melentyev.travelagency.exception.DaoSystemException;
import com.melentyev.travelagency.util.SQLStorage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;

public class TourJdbcDao extends AbstractJdbcDao<Tour> implements TourDao {

    private static final Logger logger = LoggerFactory.getLogger(TourJdbcDao.class);

    @Override
    public Tour getById(int id) throws DaoSystemException {
        return getById(id, SQLStorage.getSql("tour.select.by.id"), new TourByIdJdbcExtractor());
    }

    @Override
    public List<Tour> getAll() throws DaoSystemException {
        return getAll(SQLStorage.getSql("tour.shortdesk.select.all"), new TourJdbcExtractor());
    }

    @Override
    public void add(Tour tour) throws DaoSystemException {
        String sql = SQLStorage.getSql("tour.insert");
        try (PreparedStatement ps = getConnection().prepareStatement(sql)) {
            ps.setString(1, tour.getName());
            ps.setString(2, tour.getDescription());
            ps.setBoolean(3, tour.isHot());
            ps.setInt(4, tour.getType().getId());
            ps.executeUpdate();
        } catch (SQLException e) {
            logger.warn("Can't handle sql ['" + sql + "']; Message: " + e);
            throw new DaoSystemException("Can't handle sql ['" + sql + "']", e);
        }
    }

    @Override
    public void deleteById(int id) throws DaoSystemException {
        deleteById(id, SQLStorage.getSql("tour.delete"));
    }

    @Override
    public void update(Tour tour) throws DaoSystemException {
        String sql = SQLStorage.getSql("tour.update");
        try (PreparedStatement ps = getConnection().prepareStatement(sql)) {
            ps.setString(1, tour.getName());
            ps.setString(2, tour.getDescription());
            ps.setBoolean(3, tour.isHot());
            ps.setInt(4, tour.getType().getId());
            ps.setInt(5, tour.getId());
            ps.executeUpdate();
        } catch (SQLException e) {
            logger.warn("Can't handle sql ['" + sql + "']; Message: " + e);
            throw new DaoSystemException("Can't handle sql ['" + sql + "']", e);
        }
    }

    @Override
    public List<Tour> getAll(int offset, int rows) throws DaoSystemException {
        String sql = SQLStorage.getSql("tour.shortdesk.select.all.limit");
        try (PreparedStatement ps = getConnection().prepareStatement(sql)) {
            ps.setInt(1, offset);
            ps.setInt(2, rows);
            return executeQuery(ps, new TourJdbcExtractor());
        } catch (SQLException e) {
            logger.warn("Can't handle sql ['" + sql + "']; Message: " + e);
            throw new DaoSystemException("Can't handle sql ['" + sql + "']", e);
        }
    }

    @Override
    public int getToursCount() throws DaoSystemException {
        String sql = SQLStorage.getSql("tour.row.count");
        try (Statement statement = getConnection().createStatement()) {
            ResultSet resultSet = statement.executeQuery(sql);
            resultSet.next();
            return resultSet.getInt("rowscount");
        } catch (SQLException e) {
            logger.warn("Can't handle sql ['" + sql + "']; Message: " + e);
            throw new DaoSystemException("Can't handle sql ['" + sql + "']", e);
        }
    }

    @Override
    public List<Tour> searchTours(String sql, List<Integer> params) throws DaoSystemException {
        try (PreparedStatement ps = getConnection().prepareStatement(sql)) {
            for (int i = 0; i < params.size(); i++) {
                ps.setInt(i + 1, params.get(i));
            }
            return executeQuery(ps, new TourJdbcExtractor());
        } catch (SQLException e) {
            logger.warn("Can't handle sql ['" + sql + "']; Message: " + e);
            throw new DaoSystemException("Can't handle sql ['" + sql + "']", e);
        }
    }

    @Override
    public Tour getToursByOfferId(int id) throws DaoSystemException {
        return getById(id, SQLStorage.getSql("tour.select.by.offer.id"), new TourByIdJdbcExtractor());
    }

    @Override
    public Tour getByName(String name) throws DaoSystemException {
        String sql = SQLStorage.getSql("tour.select.by.name");
        try (PreparedStatement ps = getConnection().prepareStatement(sql)) {
            ps.setString(1, name);
            List<Tour> tours = executeQuery(ps, new TourByIdJdbcExtractor());
            return tours.isEmpty() ? null : tours.get(0);
        } catch (SQLException e) {
            logger.warn("Can't handle sql ['" + sql + "']; Message: " + e);
            throw new DaoSystemException("Can't handle sql ['" + sql + "']", e);
        }
    }

}
