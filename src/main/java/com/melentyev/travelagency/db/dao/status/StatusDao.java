package com.melentyev.travelagency.db.dao.status;

import com.melentyev.travelagency.db.dao.Dao;
import com.melentyev.travelagency.entity.Status;
import com.melentyev.travelagency.exception.DaoSystemException;

public interface StatusDao extends Dao<Status> {

    Status getDefault() throws DaoSystemException;

    Status getCanceled() throws DaoSystemException;

    Status getPaid() throws DaoSystemException;
}
