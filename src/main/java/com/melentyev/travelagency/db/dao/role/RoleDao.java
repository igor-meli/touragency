package com.melentyev.travelagency.db.dao.role;

import com.melentyev.travelagency.db.dao.Dao;
import com.melentyev.travelagency.entity.Role;
import com.melentyev.travelagency.exception.DaoSystemException;

public interface RoleDao extends Dao<Role> {

    Role getCustomerRole() throws DaoSystemException;

    Role getRoleByUserId(int id) throws DaoSystemException;
}
