package com.melentyev.travelagency.db.dao.order;

import com.melentyev.travelagency.db.dao.AbstractJdbcDao;
import com.melentyev.travelagency.db.enricher.OrderEnricher;
import com.melentyev.travelagency.db.extractor.OrderJdbcExtractor;
import com.melentyev.travelagency.entity.Order;
import com.melentyev.travelagency.exception.DaoSystemException;
import com.melentyev.travelagency.util.SQLStorage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

public class OrderJdbcDao extends AbstractJdbcDao<Order> implements OrderDao {

    private static final Logger logger = LoggerFactory.getLogger(OrderJdbcDao.class);

    @Override
    public Order getById(int id) throws DaoSystemException {
        return getById(id, SQLStorage.getSql("orders.select.by.id"), new OrderJdbcExtractor(), new OrderEnricher());
    }

    @Override
    public List<Order> getAll() throws DaoSystemException {
        return getAll(SQLStorage.getSql("orders.select.all"), new OrderJdbcExtractor(), new OrderEnricher());
    }

    @Override
    public void add(Order order) throws DaoSystemException {
        String sql = SQLStorage.getSql("orders.insert");
        try (PreparedStatement ps = getConnection().prepareStatement(sql)) {
            ps.setInt(1, order.getStatus().getId());
            ps.setInt(2, order.getUser().getId());
            ps.setInt(3, order.getOffer().getId());
            if (order.getDiscountProgram() == null) {
                ps.setNull(4, java.sql.Types.INTEGER);
            } else {
                ps.setInt(4, order.getDiscountProgram().getId());
            }
            ps.executeUpdate();
        } catch (SQLException e) {
            logger.warn("Can't handle sql ['" + sql + "']; Message: " + e);
            throw new DaoSystemException("Can't handle sql ['" + sql + "']", e);
        }
    }

    @Override
    public void deleteById(int id) {
        throw new UnsupportedOperationException();
    }

    @Override
    public void update(Order order) throws DaoSystemException {
        String sql = SQLStorage.getSql("orders.update");
        try (PreparedStatement ps = getConnection().prepareStatement(sql)) {
            ps.setInt(1, order.getDiscount());
            ps.setInt(2, order.getStatus().getId());
            if (order.getDiscountProgram() == null) {
                ps.setNull(3, java.sql.Types.INTEGER);
            } else {
                ps.setInt(3, order.getDiscountProgram().getId());
            }

            if (order.getApprovedUser() == null) {
                ps.setNull(4, java.sql.Types.INTEGER);
            } else {
                ps.setInt(4, order.getApprovedUser().getId());
            }
            ps.setInt(5, order.getId());
            ps.executeUpdate();
        } catch (SQLException e) {
            logger.warn("Can't handle sql ['" + sql + "']; Message: " + e);
            throw new DaoSystemException("Can't handle sql ['" + sql + "']", e);
        }
    }

    @Override
    public int getUserOrdersCount(int id) throws DaoSystemException {
        String sql = SQLStorage.getSql("orders.user.row.count");
        try (PreparedStatement ps = getConnection().prepareStatement(sql)) {
            ps.setInt(1, id);
            ResultSet resultSet = ps.executeQuery();
            resultSet.next();
            return resultSet.getInt("rowscount");
        } catch (SQLException e) {
            logger.warn("Can't handle sql ['" + sql + "']; Message: " + e);
            throw new DaoSystemException("Can't handle sql ['" + sql + "']", e);
        }
    }

    @Override
    public List<Order> getAllUserOrders(int id, int offset, int rows) throws DaoSystemException {
        String sql = SQLStorage.getSql("orders.select.all.user.limit");
        try (PreparedStatement ps = getConnection().prepareStatement(sql)) {
            ps.setInt(1, id);
            ps.setInt(2, offset);
            ps.setInt(3, rows);
            return executeQuery(ps, new OrderJdbcExtractor(), new OrderEnricher());
        } catch (SQLException e) {
            logger.warn("Can't handle sql ['" + sql + "']; Message: " + e);
            throw new DaoSystemException("Can't handle sql ['" + sql + "']", e);
        }
    }

    @Override
    public int getOrdersCount() throws DaoSystemException {
        String sql = SQLStorage.getSql("orders.row.count");
        try (PreparedStatement ps = getConnection().prepareStatement(sql)) {
            ResultSet resultSet = ps.executeQuery();
            resultSet.next();
            return resultSet.getInt("rowscount");
        } catch (SQLException e) {
            logger.warn("Can't handle sql ['" + sql + "']");
            throw new DaoSystemException("Can't handle sql ['" + sql + "']", e);
        }
    }

    @Override
    public List<Order> getAll(int offset, int rows) throws DaoSystemException {
        String sql = SQLStorage.getSql("orders.select.all.limit");
        try (PreparedStatement ps = getConnection().prepareStatement(sql)) {
            ps.setInt(1, offset);
            ps.setInt(2, rows);
            return executeQuery(ps, new OrderJdbcExtractor(), new OrderEnricher());
        } catch (SQLException e) {
            logger.warn("Can't handle sql ['" + sql + "']");
            throw new DaoSystemException("Can't handle sql ['" + sql + "']", e);
        }
    }
}
