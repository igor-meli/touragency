package com.melentyev.travelagency.db.dao.roomtype;

import com.melentyev.travelagency.db.dao.Dao;
import com.melentyev.travelagency.entity.RoomType;
import com.melentyev.travelagency.exception.DaoSystemException;

import java.util.List;

public interface RoomTypeDao extends Dao<RoomType> {

    RoomType getByNameAndHotelId(String name, int id) throws DaoSystemException;

    List<RoomType> getAllHotelRooms(int id) throws DaoSystemException;

}
