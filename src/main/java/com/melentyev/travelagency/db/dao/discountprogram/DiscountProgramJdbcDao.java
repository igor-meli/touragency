package com.melentyev.travelagency.db.dao.discountprogram;

import com.melentyev.travelagency.db.dao.AbstractJdbcDao;
import com.melentyev.travelagency.db.extractor.discountprogram.DiscountProgramJdbcExtractor;
import com.melentyev.travelagency.entity.DiscountProgram;
import com.melentyev.travelagency.exception.DaoSystemException;
import com.melentyev.travelagency.util.SQLStorage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.List;

public class DiscountProgramJdbcDao extends AbstractJdbcDao<DiscountProgram> implements DiscountProgramDao {

    private static final Logger logger = LoggerFactory.getLogger(DiscountProgramJdbcDao.class);

    @Override
    public DiscountProgram getById(int id) throws DaoSystemException {
        return getById(id, SQLStorage.getSql("discountProgram.select.by.id"), new DiscountProgramJdbcExtractor());
    }

    @Override
    public List<DiscountProgram> getAll() throws DaoSystemException {
        return getAll(SQLStorage.getSql("discountProgram.select.all"), new DiscountProgramJdbcExtractor());
    }

    @Override
    public void add(DiscountProgram discount) throws DaoSystemException {
        String sql = SQLStorage.getSql("discountProgram.insert");
        try (PreparedStatement ps = getConnection().prepareStatement(sql)) {
            ps.setString(1, discount.getName());
            ps.setInt(2, discount.getStep());
            ps.setInt(3, discount.getMax());
            ps.executeUpdate();
        } catch (SQLException e) {
            logger.warn("Can't handle sql ['" + sql + "']; Message: " + e);
            throw new DaoSystemException("Can't handle sql ['" + sql + "']", e);
        }
    }

    @Override
    public void deleteById(int id) throws DaoSystemException {
        deleteById(id, SQLStorage.getSql("discountProgram.delete"));
    }

    @Override
    public void update(DiscountProgram program) throws DaoSystemException {
        String sql = SQLStorage.getSql("discountProgram.update");
        try (PreparedStatement ps = getConnection().prepareStatement(sql)) {
            ps.setString(1, program.getName());
            ps.setInt(2, program.getStep());
            ps.setInt(3, program.getMax());
            ps.setInt(4, program.getId());
            ps.executeUpdate();
        } catch (SQLException e) {
            logger.warn("Can't handle sql ['" + sql + "']; Message: " + e);
            throw new DaoSystemException("Can't handle sql ['" + sql + "']", e);
        }
    }

    @Override
    public DiscountProgram getByOrderId(int id) throws DaoSystemException {
        return getById(id, SQLStorage.getSql("discountProgram.select.by.order.id"), new DiscountProgramJdbcExtractor());
    }

    @Override
    public DiscountProgram getByName(String name) throws DaoSystemException {
        String sql = SQLStorage.getSql("discountProgram.select.by.name");
        try (PreparedStatement ps = getConnection().prepareStatement(sql)) {
            ps.setString(1, name);
            List<DiscountProgram> records = executeQuery(ps, new DiscountProgramJdbcExtractor());
            return records.isEmpty() ? null : records.get(0);
        } catch (SQLException e) {
            logger.warn("Can't handle sql ['" + sql + "']; Message: " + e);
            throw new DaoSystemException("Can't handle sql ['" + sql + "']");
        }
    }
}
