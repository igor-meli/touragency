package com.melentyev.travelagency.db.dao.role;

import com.melentyev.travelagency.db.dao.AbstractJdbcDao;
import com.melentyev.travelagency.db.enricher.RoleEnricher;
import com.melentyev.travelagency.db.extractor.role.RoleJdbcExtractor;
import com.melentyev.travelagency.entity.Role;
import com.melentyev.travelagency.exception.DaoSystemException;
import com.melentyev.travelagency.util.SQLStorage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;

public class RoleJdbcDao extends AbstractJdbcDao<Role> implements RoleDao {

    private static final Logger logger = LoggerFactory.getLogger(RoleJdbcDao.class);

    @Override
    public Role getCustomerRole() throws DaoSystemException {
        return getById(1);
    }

    @Override
    public Role getRoleByUserId(int id) throws DaoSystemException {
        return getById(id, SQLStorage.getSql("role.select.by.user.id"), new RoleJdbcExtractor(), new RoleEnricher());
    }

    @Override
    public Role getById(int id) throws DaoSystemException {
        return getById(id, SQLStorage.getSql("role.select.by.id"), new RoleJdbcExtractor());
    }

    @Override
    public List<Role> getAll() throws DaoSystemException {
        return getAll(SQLStorage.getSql("role.select.all"), new RoleJdbcExtractor());
    }

    @Override
    public void add(Role entity) {
        throw new UnsupportedOperationException();
    }

    @Override
    public void deleteById(int id) {
        throw new UnsupportedOperationException();
    }

    @Override
    public void update(Role entity) throws DaoSystemException {
        throw new UnsupportedOperationException();
    }
}
