package com.melentyev.travelagency.db.dao.tourType;

import com.melentyev.travelagency.db.dao.Dao;
import com.melentyev.travelagency.entity.TourType;

public interface TourTypeDao extends Dao<TourType> {

}
