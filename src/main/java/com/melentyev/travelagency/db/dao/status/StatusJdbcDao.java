package com.melentyev.travelagency.db.dao.status;

import com.melentyev.travelagency.db.dao.AbstractJdbcDao;
import com.melentyev.travelagency.db.extractor.status.StatusJdbcExtractor;
import com.melentyev.travelagency.entity.Status;
import com.melentyev.travelagency.exception.DaoSystemException;
import com.melentyev.travelagency.util.SQLStorage;

import java.util.List;

/**
 * Status
 * 1 - Booked
 * 2 - Paid
 * 3 - Canceled
 */
public class StatusJdbcDao extends AbstractJdbcDao<Status> implements StatusDao {

    @Override
    public Status getDefault() throws DaoSystemException {
        return getById(1);
    }

    @Override
    public Status getCanceled() throws DaoSystemException {
        return getById(3);
    }

    @Override
    public Status getPaid() throws DaoSystemException {
        return getById(2);
    }

    @Override
    public Status getById(int id) throws DaoSystemException {
        return getById(id, SQLStorage.getSql("status.select.by.id"), new StatusJdbcExtractor());
    }

    @Override
    public List<Status> getAll() throws DaoSystemException {
        return getAll(SQLStorage.getSql("status.select.all"), new StatusJdbcExtractor());
    }

    @Override
    public void add(Status entity) throws DaoSystemException {
        throw new UnsupportedOperationException();
    }

    @Override
    public void deleteById(int id) {
        throw new UnsupportedOperationException();
    }

    @Override
    public void update(Status entity) throws DaoSystemException {
        throw new UnsupportedOperationException();
    }
}
