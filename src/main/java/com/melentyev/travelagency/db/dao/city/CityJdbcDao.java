package com.melentyev.travelagency.db.dao.city;

import com.melentyev.travelagency.db.dao.AbstractJdbcDao;
import com.melentyev.travelagency.db.enricher.CityEnricher;
import com.melentyev.travelagency.db.extractor.city.CityJdbcExtractor;
import com.melentyev.travelagency.entity.City;
import com.melentyev.travelagency.exception.DaoSystemException;
import com.melentyev.travelagency.util.SQLStorage;

import java.util.List;

public class CityJdbcDao extends AbstractJdbcDao<City> implements CityDao {

    @Override
    public City getById(int id) throws DaoSystemException {
        return getById(id, SQLStorage.getSql("city.select.by.id"), new CityJdbcExtractor(), new CityEnricher());
    }

    @Override
    public List<City> getAll() throws DaoSystemException {
        return getAll(SQLStorage.getSql("city.select.all"), new CityJdbcExtractor(), new CityEnricher());
    }

    @Override
    public void add(City entity) throws DaoSystemException {
        throw new UnsupportedOperationException();
    }

    @Override
    public void deleteById(int id) throws DaoSystemException {
        throw new UnsupportedOperationException();
    }

    @Override
    public void update(City entity) throws DaoSystemException {
        throw new UnsupportedOperationException();
    }

    @Override
    public City getByHotelId(int id) throws DaoSystemException {
        return getById(id, SQLStorage.getSql("city.select.by.hotel.id"), new CityJdbcExtractor(), new CityEnricher());
    }
}
