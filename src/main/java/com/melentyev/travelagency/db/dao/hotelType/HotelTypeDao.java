package com.melentyev.travelagency.db.dao.hotelType;

import com.melentyev.travelagency.db.dao.Dao;
import com.melentyev.travelagency.entity.HotelType;

public interface HotelTypeDao extends Dao<HotelType> {

}
