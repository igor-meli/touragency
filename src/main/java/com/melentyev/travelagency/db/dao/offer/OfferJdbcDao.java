package com.melentyev.travelagency.db.dao.offer;

import com.melentyev.travelagency.db.dao.AbstractJdbcDao;
import com.melentyev.travelagency.db.enricher.OfferEnricher;
import com.melentyev.travelagency.db.extractor.offer.OfferJdbcExtractor;
import com.melentyev.travelagency.entity.Offer;
import com.melentyev.travelagency.exception.DaoSystemException;
import com.melentyev.travelagency.util.SQLStorage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.List;

public class OfferJdbcDao extends AbstractJdbcDao<Offer> implements OfferDao {

    private static final Logger logger = LoggerFactory.getLogger(OfferJdbcDao.class);

    @Override
    public Offer getById(int id) throws DaoSystemException {
        return getById(id, SQLStorage.getSql("offer.select.by.id"), new OfferJdbcExtractor(), new OfferEnricher());
    }

    @Override
    public List<Offer> getAll() throws DaoSystemException {
        return getAll(SQLStorage.getSql("offer.select.all"), new OfferJdbcExtractor(), new OfferEnricher());
    }

    @Override
    public void add(Offer offer) throws DaoSystemException {
        String sql = SQLStorage.getSql("offer.insert");
        try (PreparedStatement ps = getConnection().prepareStatement(sql)) {
            ps.setInt(1, offer.getPrice());
            ps.setInt(2, offer.getHotel().getId());
            ps.setInt(3, offer.getTour().getId());
            ps.setInt(4, offer.getRoomType().getId());
            ps.executeUpdate();
        } catch (SQLException e) {
            logger.warn("Can't handle sql ['" + sql + "']; Message: " + e);
            throw new DaoSystemException("Can't handle sql ['" + sql + "']", e);
        }
    }

    @Override
    public void deleteById(int id) throws DaoSystemException {
        deleteById(id, SQLStorage.getSql("offer.delete"));
    }

    @Override
    public void update(Offer offer) throws DaoSystemException {
        String sql = SQLStorage.getSql("offer.update");
        try (PreparedStatement ps = getConnection().prepareStatement(sql)) {
            ps.setInt(1, offer.getPrice());
            ps.setInt(2, offer.getHotel().getId());
            ps.setInt(3, offer.getTour().getId());
            ps.setInt(4, offer.getRoomType().getId());
            ps.setInt(5, offer.getId());
            ps.executeUpdate();
        } catch (SQLException e) {
            logger.warn("Can't handle sql ['" + sql + "']; Message: " + e);
            throw new DaoSystemException("Can't handle sql ['" + sql + "']", e);
        }
    }

    @Override
    public List<Offer> getOffersByTourId(int tourId) throws DaoSystemException {
        String sql = SQLStorage.getSql("offer.select.by.tour.id");
        try (PreparedStatement ps = getConnection().prepareStatement(sql)) {
            ps.setInt(1, tourId);
            return executeQuery(ps, new OfferJdbcExtractor());
        } catch (SQLException e) {
            logger.warn("Can't handle sql ['" + sql + "']; Message: " + e);
            throw new DaoSystemException("Can't handle sql ['" + sql + "']", e);
        }
    }

    @Override
    public Offer getOfferByOrderId(int id) throws DaoSystemException {
        return getById(id, SQLStorage.getSql("offer.select.by.orders.id"), new OfferJdbcExtractor(), new OfferEnricher());
    }

    @Override
    public Offer getByHotelIdTourIdAndRoomId(int hotelId, int tourId, int roomId) throws DaoSystemException {
        String sql = SQLStorage.getSql("offer.select.by.hotel.id.and.tour.id");
        try (PreparedStatement ps = getConnection().prepareStatement(sql)) {
            ps.setInt(1, hotelId);
            ps.setInt(2, tourId);
            ps.setInt(3, roomId);
            List<Offer> offers = executeQuery(ps, new OfferJdbcExtractor());
            return offers.isEmpty() ? null : offers.get(0);
        } catch (SQLException e) {
            logger.warn("Can't handle sql ['" + sql + "']; Message: " + e);
            throw new DaoSystemException("Can't handle sql ['" + sql + "']", e);
        }
    }
}
