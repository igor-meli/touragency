package com.melentyev.travelagency.db.dao.country;

import com.melentyev.travelagency.db.dao.Dao;
import com.melentyev.travelagency.entity.Country;
import com.melentyev.travelagency.exception.DaoSystemException;

public interface CountryDao extends Dao<Country> {

    Country getByCityId(int id) throws DaoSystemException;

}
