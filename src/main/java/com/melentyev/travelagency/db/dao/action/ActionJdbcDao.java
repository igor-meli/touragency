package com.melentyev.travelagency.db.dao.action;

import com.melentyev.travelagency.db.dao.AbstractJdbcDao;
import com.melentyev.travelagency.db.extractor.action.ActionExtractor;
import com.melentyev.travelagency.entity.Action;
import com.melentyev.travelagency.exception.DaoSystemException;
import com.melentyev.travelagency.util.SQLStorage;

import java.util.List;

public class ActionJdbcDao extends AbstractJdbcDao<Action> implements ActionDao {

    @Override
    public Action getActionByRightsId(int id) throws DaoSystemException {
        return getById(id, SQLStorage.getSql("action.select.by.rights.id"), new ActionExtractor());
    }

    @Override
    public Action getById(int id) throws DaoSystemException {
        return getById(id, SQLStorage.getSql("action.select.by.id"), new ActionExtractor());
    }

    @Override
    public List<Action> getAll() throws DaoSystemException {
        return getAll(SQLStorage.getSql("action.select.all"), new ActionExtractor());
    }

    @Override
    public void add(Action entity) throws DaoSystemException {
        throw new UnsupportedOperationException();
    }

    @Override
    public void deleteById(int id) throws DaoSystemException {
        deleteById(id, SQLStorage.getSql("action.delete.by.id "));
    }

    @Override
    public void update(Action entity) throws DaoSystemException {
        throw new UnsupportedOperationException();
    }
}
