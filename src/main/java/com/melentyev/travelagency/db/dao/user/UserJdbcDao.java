package com.melentyev.travelagency.db.dao.user;

import com.melentyev.travelagency.db.dao.AbstractJdbcDao;
import com.melentyev.travelagency.db.enricher.UserEnricher;
import com.melentyev.travelagency.db.extractor.UserJdbcManagerExtractor;
import com.melentyev.travelagency.db.extractor.user.UserJdbcExtractor;
import com.melentyev.travelagency.entity.User;
import com.melentyev.travelagency.exception.DaoSystemException;
import com.melentyev.travelagency.util.SQLStorage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;

public class UserJdbcDao extends AbstractJdbcDao<User> implements UserDao {

    private static final Logger logger = LoggerFactory.getLogger(UserJdbcDao.class);

    @Override
    public User getById(int id) throws DaoSystemException {
        return getById(id, SQLStorage.getSql("user.select.by.id"), new UserJdbcExtractor(), new UserEnricher());
    }

    @Override
    public List<User> getAll() throws DaoSystemException {
        return getAll(SQLStorage.getSql("user.select.all"), new UserJdbcExtractor(), new UserEnricher());
    }

    @Override
    public void add(User user) throws DaoSystemException {
        String sql = SQLStorage.getSql("user.insert");
        try (PreparedStatement ps = getConnection().prepareStatement(sql)) {
            ps.setString(1, user.getLogin());
            ps.setString(2, user.getPassword());
            ps.setString(3, user.getFirstName());
            ps.setString(4, user.getSecondName());
            ps.setString(5, user.getEmail());
            ps.setBoolean(6, user.isBlocked());
            ps.setInt(7, user.getRole().getId());
            ps.executeUpdate();
        } catch (SQLException e) {
            logger.warn("Can't handle sql ['" + sql + "']; Message: " + e);
            throw new DaoSystemException("Can't handle sql ['" + sql + "']", e);
        }
    }

    @Override
    public void deleteById(int id) {
        throw new UnsupportedOperationException();
    }

    @Override
    public void update(User user) throws DaoSystemException {
        String sql = SQLStorage.getSql("user.update");
        try (PreparedStatement ps = getConnection().prepareStatement(sql)) {
            ps.setString(1, user.getPassword());
            ps.setString(2, user.getFirstName());
            ps.setString(3, user.getSecondName());
            ps.setString(4, user.getEmail());
            ps.setBoolean(5, user.isBlocked());
            ps.setInt(6, user.getId());
            ps.executeUpdate();
        } catch (SQLException e) {
            logger.warn("Can't handle sql ['" + sql + "']; Message: " + e);
            throw new DaoSystemException("Can't handle sql ['" + sql + "']", e);
        }
    }

    @Override
    public User getUserByLogin(String login) throws DaoSystemException {
        String sql = SQLStorage.getSql("user.select.by.login");
        try (PreparedStatement ps = getConnection().prepareStatement(sql)) {
            ps.setString(1, login);
            List<User> users = executeQuery(ps, new UserJdbcExtractor(), new UserEnricher());
            return users.isEmpty() ? null : users.get(0);
        } catch (SQLException e) {
            logger.warn("Can't handle sql ['" + sql + "']; Message: " + e);
            throw new DaoSystemException("Can't handle sql ['" + sql + "']", e);
        }
    }

    @Override
    public User getUserByLoginAndPassword(String login, String password) throws DaoSystemException {
        String sql = SQLStorage.getSql("user.select.by.login.and.password");
        try (PreparedStatement ps = getConnection().prepareStatement(sql)) {
            ps.setString(1, login);
            ps.setString(2, password);
            List<User> users = executeQuery(ps, new UserJdbcExtractor(), new UserEnricher());
            return users.isEmpty() ? null : users.get(0);
        } catch (SQLException e) {
            logger.warn("Can't handle sql ['" + sql + "']; Message: " + e);
            throw new DaoSystemException("Can't handle sql ['" + sql + "']", e);
        }
    }

    @Override
    public User getUserByOrderId(int id) throws DaoSystemException {
        return getById(id, SQLStorage.getSql("user.select.by.order.id"), new UserJdbcExtractor(), new UserEnricher());
    }

    @Override
    public int getRowsCount() throws DaoSystemException {
        String sql = SQLStorage.getSql("user.row.count");
        try (Statement statement = getConnection().createStatement()) {
            ResultSet resultSet = statement.executeQuery(sql);
            resultSet.next();
            return resultSet.getInt("rowscount");
        } catch (SQLException e) {
            logger.warn("Can't handle sql ['" + sql + "']; Message: " + e);
            throw new DaoSystemException("Can't handle sql ['" + sql + "']", e);
        }
    }

    @Override
    public List<User> getAll(int offset, int rows) throws DaoSystemException {
        String sql = SQLStorage.getSql("user.select.all.limit");
        try (PreparedStatement ps = getConnection().prepareStatement(sql)) {
            ps.setInt(1, offset);
            ps.setInt(2, rows);
            return executeQuery(ps, new UserJdbcExtractor(), new UserEnricher());
        } catch (SQLException e) {
            logger.warn("Can't handle sql ['" + sql + "']; Message: " + e);
            throw new DaoSystemException("Can't handle sql ['" + sql + "']", e);
        }
    }

    @Override
    public List<User> getAllManagers() throws DaoSystemException {
        return getAll(SQLStorage.getSql("user.select.by.order.approved"), new UserJdbcManagerExtractor());
    }

}
