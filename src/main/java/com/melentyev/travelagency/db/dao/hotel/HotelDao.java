package com.melentyev.travelagency.db.dao.hotel;

import com.melentyev.travelagency.db.dao.Dao;
import com.melentyev.travelagency.entity.Hotel;
import com.melentyev.travelagency.exception.DaoSystemException;

public interface HotelDao extends Dao<Hotel> {

    Hotel getByRoomTypeId(int id) throws DaoSystemException;

}
