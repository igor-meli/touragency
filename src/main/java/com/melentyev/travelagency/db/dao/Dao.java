package com.melentyev.travelagency.db.dao;

import com.melentyev.travelagency.exception.DaoSystemException;

import java.util.List;

public interface Dao<T> {

    T getById(int id) throws DaoSystemException;

    List<T> getAll() throws DaoSystemException;

    void add(T entity) throws DaoSystemException;

    void deleteById(int id) throws DaoSystemException;

    void update(T entity) throws DaoSystemException;

}
