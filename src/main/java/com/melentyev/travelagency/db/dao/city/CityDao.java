package com.melentyev.travelagency.db.dao.city;

import com.melentyev.travelagency.db.dao.Dao;
import com.melentyev.travelagency.entity.City;
import com.melentyev.travelagency.exception.DaoSystemException;

public interface CityDao extends Dao<City> {

    City getByHotelId(int id) throws DaoSystemException;

}
