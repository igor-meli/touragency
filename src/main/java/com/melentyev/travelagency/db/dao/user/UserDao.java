package com.melentyev.travelagency.db.dao.user;

import com.melentyev.travelagency.db.dao.Dao;
import com.melentyev.travelagency.entity.User;
import com.melentyev.travelagency.exception.DaoSystemException;

import java.util.List;

public interface UserDao extends Dao<User> {

    User getUserByLogin(String login) throws DaoSystemException;

    User getUserByLoginAndPassword(String login, String password) throws DaoSystemException;

    User getUserByOrderId(int id) throws DaoSystemException;

    int getRowsCount() throws DaoSystemException;

    List<User> getAll(int offset, int numbersOfRows) throws DaoSystemException;

    List<User> getAllManagers() throws DaoSystemException;

}
