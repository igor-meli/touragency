package com.melentyev.travelagency.db.dao;

import com.melentyev.travelagency.db.enricher.Enricher;
import com.melentyev.travelagency.db.extractor.Extractor;
import com.melentyev.travelagency.db.tx.TransactionManagerImpl;
import com.melentyev.travelagency.exception.DaoSystemException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public abstract class AbstractJdbcDao<T> implements Dao<T> {

    private static final Logger logger = LoggerFactory.getLogger(AbstractJdbcDao.class);
    private DataSource dataSource = TransactionManagerImpl.getTx();

    @SuppressWarnings("unchecked")
    protected T getById(int id, String sql, Extractor<T> extractor) throws DaoSystemException {
        return getById(id, sql, extractor, Enricher.NONE);
    }

    protected T getById(int id, String sql, Extractor<T> extractor, Enricher<T> enricher) throws DaoSystemException {
        try (PreparedStatement ps = getConnection().prepareStatement(sql)) {
            ps.setInt(1, id);
            List<T> records = executeQuery(ps, extractor, enricher);
            return records.isEmpty() ? null : records.get(0);
        } catch (SQLException e) {
            logger.warn("Can't handle sql ['" + sql + "']; Message: " + e);
            throw new DaoSystemException("Can't handle sql ['" + sql + "']");
        }
    }

    protected Connection getConnection() {
        try {
            return dataSource.getConnection();
        } catch (SQLException e) {
            throw new IllegalStateException("Connection wasn't set");
        }
    }

    protected List<T> executeQuery(PreparedStatement ps, Extractor<T> extractor, Enricher<T> enricher) throws SQLException, DaoSystemException {
        ResultSet resultSet = ps.executeQuery();
        List<T> result = new ArrayList<>();
        while (resultSet.next()) {
            T record = extractor.extractOne(resultSet);
            enricher.enrich(record);
            result.add(record);
        }
        return result;
    }

    @SuppressWarnings("unchecked")
    protected List<T> getAll(String sql, Extractor<T> extractor) throws DaoSystemException {
        return getAll(sql, extractor, Enricher.NONE);
    }

    protected List<T> getAll(String sql, Extractor<T> extractor, Enricher<T> enricher) throws DaoSystemException {
        try (PreparedStatement ps = getConnection().prepareStatement(sql)) {
            return executeQuery(ps, extractor, enricher);
        } catch (SQLException e) {
            logger.warn("Can't handle sql ['" + sql + "']; Message: " + e);
            throw new DaoSystemException("Can't handle sql ['" + sql + "']", e);
        }
    }

    @SuppressWarnings("unchecked")
    protected List<T> executeQuery(PreparedStatement ps, Extractor<T> extractor) throws SQLException, DaoSystemException {
        return executeQuery(ps, extractor, Enricher.NONE);
    }

    protected void deleteById(int id, String sql) throws DaoSystemException {
        try (PreparedStatement ps = getConnection().prepareStatement(sql)) {
            ps.setInt(1, id);
            ps.executeUpdate();
        } catch (SQLException e) {
            logger.warn("Can't handle sql ['" + sql + "']; Message: " + e);
            throw new DaoSystemException("Can't handle sql ['" + sql + "']", e);
        }
    }
}
