package com.melentyev.travelagency.db.dao.tourType;

import com.melentyev.travelagency.db.dao.AbstractJdbcDao;
import com.melentyev.travelagency.db.extractor.tourtype.TourTypeJdbcExtractor;
import com.melentyev.travelagency.entity.TourType;
import com.melentyev.travelagency.exception.DaoSystemException;
import com.melentyev.travelagency.util.SQLStorage;

import java.util.List;

public class TourTypeJdbcDao extends AbstractJdbcDao<TourType> implements TourTypeDao {

    @Override
    public TourType getById(int id) throws DaoSystemException {
        return getById(id, SQLStorage.getSql("tourType.select.by.id"), new TourTypeJdbcExtractor());
    }

    @Override
    public List<TourType> getAll() throws DaoSystemException {
        return getAll(SQLStorage.getSql("tourType.select.all"), new TourTypeJdbcExtractor());
    }

    @Override
    public void add(TourType entity) {
        throw new UnsupportedOperationException();
    }

    @Override
    public void deleteById(int id) {
        throw new UnsupportedOperationException();
    }

    @Override
    public void update(TourType entity) throws DaoSystemException {
        throw new UnsupportedOperationException();
    }
}
