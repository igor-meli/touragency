package com.melentyev.travelagency.db.dao.hotelType;

import com.melentyev.travelagency.db.dao.AbstractJdbcDao;
import com.melentyev.travelagency.db.extractor.hoteltype.HotelTypeExtractor;
import com.melentyev.travelagency.entity.HotelType;
import com.melentyev.travelagency.exception.DaoSystemException;
import com.melentyev.travelagency.util.SQLStorage;

import java.util.List;

public class HotelTypeJdbcDao extends AbstractJdbcDao<HotelType> implements HotelTypeDao {

    @Override
    public HotelType getById(int id) throws DaoSystemException {
        return getById(id, SQLStorage.getSql("hotelType.select.by.id"), new HotelTypeExtractor());
    }

    @Override
    public List<HotelType> getAll() throws DaoSystemException {
        return getAll(SQLStorage.getSql("hotelType.select.all"), new HotelTypeExtractor());
    }

    @Override
    public void add(HotelType entity) {
        throw new UnsupportedOperationException();
    }

    @Override
    public void deleteById(int id) {
        throw new UnsupportedOperationException();
    }

    @Override
    public void update(HotelType entity) throws DaoSystemException {
        throw new UnsupportedOperationException();
    }
}
