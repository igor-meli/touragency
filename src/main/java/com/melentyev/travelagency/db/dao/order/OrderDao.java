package com.melentyev.travelagency.db.dao.order;

import com.melentyev.travelagency.db.dao.Dao;
import com.melentyev.travelagency.entity.Order;
import com.melentyev.travelagency.exception.DaoSystemException;

import java.util.List;

public interface OrderDao extends Dao<Order> {

    int getUserOrdersCount(int id) throws DaoSystemException;

    List<Order> getAllUserOrders(int id, int offset, int rows) throws DaoSystemException;

    int getOrdersCount() throws DaoSystemException;

    List<Order> getAll(int offset, int rows) throws DaoSystemException;
}
