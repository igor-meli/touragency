package com.melentyev.travelagency.db.dao.roomtype;

import com.melentyev.travelagency.db.dao.AbstractJdbcDao;
import com.melentyev.travelagency.db.enricher.RoomTypeEnricher;
import com.melentyev.travelagency.db.extractor.roomtype.RoomTypeJdbcExtractor;
import com.melentyev.travelagency.entity.RoomType;
import com.melentyev.travelagency.exception.DaoSystemException;
import com.melentyev.travelagency.util.SQLStorage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.List;

public class RoomTypeJdbcDao extends AbstractJdbcDao<RoomType> implements RoomTypeDao {

    private static final Logger logger = LoggerFactory.getLogger(RoomTypeJdbcDao.class);

    @Override
    public RoomType getById(int id) throws DaoSystemException {
        return getById(id, SQLStorage.getSql("roomtype.select.by.id"), new RoomTypeJdbcExtractor(), new RoomTypeEnricher());
    }

    @Override
    public List<RoomType> getAll() throws DaoSystemException {
        return getAll(SQLStorage.getSql("roomtype.select.all"), new RoomTypeJdbcExtractor(), new RoomTypeEnricher());
    }

    @Override
    public void add(RoomType roomType) throws DaoSystemException {
        String sql = SQLStorage.getSql("roomType.insert");
        try (PreparedStatement ps = getConnection().prepareStatement(sql)) {
            ps.setString(1, roomType.getName());
            ps.setInt(2, roomType.getMaxAdults());
            ps.setInt(3, roomType.getMaxChildren());
            ps.setInt(4, roomType.getHotel().getId());
            ps.executeUpdate();
        } catch (SQLException e) {
            logger.warn("Can't handle sql ['" + sql + "']; Message: " + e);
            throw new DaoSystemException("Can't handle sql ['" + sql + "']", e);
        }
    }

    @Override
    public void deleteById(int id) throws DaoSystemException {
        deleteById(id,SQLStorage.getSql("roomType.delete"));
    }

    @Override
    public void update(RoomType roomType) throws DaoSystemException {
        String sql = SQLStorage.getSql("roomType.update");
        try (PreparedStatement ps = getConnection().prepareStatement(sql)) {
            ps.setInt(1, roomType.getMaxAdults());
            ps.setInt(2, roomType.getMaxChildren());
            ps.setString(3, roomType.getName());
            ps.setInt(4, roomType.getHotel().getId());
            ps.setInt(5, roomType.getId());
            ps.executeUpdate();
        } catch (SQLException e) {
            logger.warn("Can't handle sql ['" + sql + "']; Message: " + e);
            throw new DaoSystemException("Can't handle sql ['" + sql + "']", e);
        }
    }

    @Override
    public RoomType getByNameAndHotelId(String name, int id) throws DaoSystemException {
        String sql = SQLStorage.getSql("roomtype.select.by.name.and.hotel.id");
        try (PreparedStatement ps = getConnection().prepareStatement(sql)) {
            ps.setString(1, name);
            ps.setInt(2, id);
            List<RoomType> records = executeQuery(ps, new RoomTypeJdbcExtractor(), new RoomTypeEnricher());
            return records.isEmpty() ? null : records.get(0);
        } catch (SQLException e) {
            logger.warn("Can't handle sql ['" + sql + "']; Message: " + e);
            throw new DaoSystemException("Can't handle sql ['" + sql + "']");
        }
    }

    @Override
    public List<RoomType> getAllHotelRooms(int id) throws DaoSystemException {
        String sql = SQLStorage.getSql("roomtype.select.by.hotel.id");
        try (PreparedStatement ps = getConnection().prepareStatement(sql)) {
            ps.setInt(1, id);
            return executeQuery(ps, new RoomTypeJdbcExtractor());
        } catch (SQLException e) {
            logger.warn("Can't handle sql ['" + sql + "']; Message: " + e);
            throw new DaoSystemException("Can't handle sql ['" + sql + "']");
        }
    }
}
