package com.melentyev.travelagency.db.extractor.city;

import com.melentyev.travelagency.db.extractor.Extractor;
import com.melentyev.travelagency.entity.City;

import java.sql.ResultSet;
import java.sql.SQLException;

public class CityJdbcExtractor implements Extractor<City> {

    @Override
    public City extractOne(ResultSet resultSet) throws SQLException {
        int id = resultSet.getInt("id");
        String city = resultSet.getString("city");
        return new City(id, city);
    }
}
