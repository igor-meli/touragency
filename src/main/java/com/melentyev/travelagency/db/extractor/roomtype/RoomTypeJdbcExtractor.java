package com.melentyev.travelagency.db.extractor.roomtype;

import com.melentyev.travelagency.db.extractor.Extractor;
import com.melentyev.travelagency.entity.RoomType;

import java.sql.ResultSet;
import java.sql.SQLException;

public class RoomTypeJdbcExtractor implements Extractor<RoomType> {

    @Override
    public RoomType extractOne(ResultSet resultSet) throws SQLException {
        int id = resultSet.getInt("id");
        int maxAdults = resultSet.getInt("max_adults");
        int maxChildren = resultSet.getInt("max_children");
        String name = resultSet.getString("name");
        return new RoomType(id,name, maxChildren,maxAdults);
    }
}
