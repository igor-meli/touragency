package com.melentyev.travelagency.db.extractor;

import java.sql.ResultSet;
import java.sql.SQLException;

public interface Extractor<T> {

    T extractOne(ResultSet resultSet) throws SQLException;

}

