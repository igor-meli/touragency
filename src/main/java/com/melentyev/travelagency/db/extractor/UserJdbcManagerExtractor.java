package com.melentyev.travelagency.db.extractor;

import com.melentyev.travelagency.entity.User;

import java.sql.ResultSet;
import java.sql.SQLException;

public class UserJdbcManagerExtractor implements Extractor<User> {

    @Override
    public User extractOne(ResultSet resultSet) throws SQLException {
        int id = resultSet.getInt("id");
        String login = resultSet.getString("login");
        String password = resultSet.getString("password");
        String firstName = resultSet.getString("first_name");
        String secondName = resultSet.getString("second_name");
        String email = resultSet.getString("email");
        boolean isBlocked = resultSet.getBoolean("isBlocked");
        int approvedCount = resultSet.getInt("approvedUserId");


        return new User(id, login,password, firstName, secondName, email, isBlocked, approvedCount);
    }
}
