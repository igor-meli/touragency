package com.melentyev.travelagency.db.extractor.offer;

import com.melentyev.travelagency.db.extractor.Extractor;
import com.melentyev.travelagency.entity.*;

import java.sql.ResultSet;
import java.sql.SQLException;

public class OfferJdbcExtractor implements Extractor<Offer> {

    @Override
    public Offer extractOne(ResultSet resultSet) throws SQLException {
        int offerId = resultSet.getInt("offer_id");
        int price = resultSet.getInt("price");

        int hotelId = resultSet.getInt("hotel_id");
        String hotelName = resultSet.getString("hotelName");
        int hotelTypeId = resultSet.getInt("hotel_type_id");
        String hotelTypeName = resultSet.getString("hotel_type");
        int roomId = resultSet.getInt("room_type_id");
        String room = resultSet.getString("room_type_name");
        int maxChildren = resultSet.getInt("max_children");
        int maxAdults = resultSet.getInt("max_adults");
        int cityId = resultSet.getInt("city_id");
        String cityName = resultSet.getString("city");
        int countryId = resultSet.getInt("country_id");
        String countryName = resultSet.getString("country");

        Country country = new Country(countryId, countryName);
        City city = new City(cityId, cityName, country);
        HotelType hotelType = new HotelType(hotelTypeId, hotelTypeName);
        Hotel hotel = new Hotel(hotelId, hotelName, city, hotelType);
        RoomType roomType = new RoomType(roomId, room, maxChildren, maxAdults, hotel);

        return new Offer(offerId, price, hotel, roomType);
    }
}
