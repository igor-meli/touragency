package com.melentyev.travelagency.db.extractor.role;

import com.melentyev.travelagency.db.extractor.Extractor;
import com.melentyev.travelagency.entity.Role;

import java.sql.ResultSet;
import java.sql.SQLException;

public class RoleJdbcExtractor implements Extractor<Role> {

    @Override
    public Role extractOne(ResultSet resultSet) throws SQLException {
        int id = resultSet.getInt("id");
        String name = resultSet.getString("role_name");
        return new Role(id, name);
    }
}
