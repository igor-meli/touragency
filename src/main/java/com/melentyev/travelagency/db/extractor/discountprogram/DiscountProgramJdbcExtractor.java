package com.melentyev.travelagency.db.extractor.discountprogram;

import com.melentyev.travelagency.db.extractor.Extractor;
import com.melentyev.travelagency.entity.DiscountProgram;

import java.sql.ResultSet;
import java.sql.SQLException;

public class DiscountProgramJdbcExtractor implements Extractor<DiscountProgram> {

    @Override
    public DiscountProgram extractOne(ResultSet resultSet) throws SQLException {
        int id = resultSet.getInt("id");
        String name = resultSet.getString("name");
        int step = resultSet.getInt("step");
        int max = resultSet.getInt("max");
        return new DiscountProgram(id, name, step, max);
    }

}
