package com.melentyev.travelagency.db.extractor.rights;

import com.melentyev.travelagency.db.extractor.Extractor;
import com.melentyev.travelagency.entity.Rights;

import java.sql.ResultSet;
import java.sql.SQLException;

public class RightsExtractor implements Extractor<Rights> {

    @Override
    public Rights extractOne(ResultSet resultSet) throws SQLException {
        int id = resultSet.getInt("id");
        return new Rights(id);
    }
}
