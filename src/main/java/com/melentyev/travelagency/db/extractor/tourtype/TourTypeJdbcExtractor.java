package com.melentyev.travelagency.db.extractor.tourtype;

import com.melentyev.travelagency.db.extractor.Extractor;
import com.melentyev.travelagency.entity.TourType;

import java.sql.ResultSet;
import java.sql.SQLException;

public class TourTypeJdbcExtractor implements Extractor<TourType> {

    @Override
    public TourType extractOne(ResultSet resultSet) throws SQLException {
        int id = resultSet.getInt("id");
        String name = resultSet.getString("name");
        return new TourType(id, name);
    }
}
