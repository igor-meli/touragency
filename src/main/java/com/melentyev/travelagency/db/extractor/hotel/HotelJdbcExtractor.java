package com.melentyev.travelagency.db.extractor.hotel;

import com.melentyev.travelagency.db.extractor.Extractor;
import com.melentyev.travelagency.entity.Hotel;

import java.sql.ResultSet;
import java.sql.SQLException;

public class HotelJdbcExtractor implements Extractor<Hotel> {

    @Override
    public Hotel extractOne(ResultSet resultSet) throws SQLException {
        int id = resultSet.getInt("id");
        String name = resultSet.getString("name");
        return new Hotel(id,name);
    }
}
