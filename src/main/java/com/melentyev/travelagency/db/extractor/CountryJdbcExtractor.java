package com.melentyev.travelagency.db.extractor;

import com.melentyev.travelagency.db.extractor.Extractor;
import com.melentyev.travelagency.entity.Country;

import java.sql.ResultSet;
import java.sql.SQLException;

public class CountryJdbcExtractor implements Extractor<Country> {

    @Override
    public Country extractOne(ResultSet resultSet) throws SQLException {
        int id = resultSet.getInt("id");
        String country = resultSet.getString("country");
        return new Country(id, country);
    }
}
