package com.melentyev.travelagency.db.extractor.status;

import com.melentyev.travelagency.db.extractor.Extractor;
import com.melentyev.travelagency.entity.Status;

import java.sql.ResultSet;
import java.sql.SQLException;

public class StatusJdbcExtractor implements Extractor<Status> {

    @Override
    public Status extractOne(ResultSet resultSet) throws SQLException {
        int id = resultSet.getInt("id");
        String name = resultSet.getString("name");
        return new Status(id, name);
    }

}
