package com.melentyev.travelagency.db.extractor;

import com.melentyev.travelagency.entity.Order;
import com.melentyev.travelagency.entity.Status;

import java.sql.ResultSet;
import java.sql.SQLException;

public class OrderJdbcExtractor implements Extractor<Order> {

    @Override
    public Order extractOne(ResultSet resultSet) throws SQLException {
        int id = resultSet.getInt("id");
        int discount = resultSet.getInt("discount");
        int statusId = resultSet.getInt("status_id");
        String statusName = resultSet.getString("status_name");
        Status status = new Status(statusId, statusName);
        Order order = new Order();
        order.setId(id);
        order.setDiscount(discount);
        order.setStatus(status);
        return order;
    }

}
