package com.melentyev.travelagency.db.extractor.hoteltype;

import com.melentyev.travelagency.db.extractor.Extractor;
import com.melentyev.travelagency.entity.HotelType;

import java.sql.ResultSet;
import java.sql.SQLException;

public class HotelTypeExtractor implements Extractor<HotelType> {

    @Override
    public HotelType extractOne(ResultSet resultSet) throws SQLException {
        int id = resultSet.getInt("id");
        String name = resultSet.getString("name");
        return new HotelType(id, name);
    }
}
