package com.melentyev.travelagency.db.extractor.tour;

import com.melentyev.travelagency.db.extractor.Extractor;
import com.melentyev.travelagency.entity.Tour;
import com.melentyev.travelagency.entity.TourType;

import java.sql.ResultSet;
import java.sql.SQLException;

public class TourJdbcExtractor implements Extractor<Tour> {

    @Override
    public Tour extractOne(ResultSet resultSet) throws SQLException {
        int tourId = resultSet.getInt("id");
        String tourName = resultSet.getString("name");
        String description = resultSet.getString("description");
        boolean isHot = resultSet.getBoolean("isHot");
        int minPrice = resultSet.getInt("minPrice");

        int typeId = resultSet.getInt("type_id");
        String typeName = resultSet.getString("type_name");
        TourType type = new TourType(typeId, typeName);

        return new Tour(tourId, tourName, description, isHot, type, minPrice);
    }
}
