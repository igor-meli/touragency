package com.melentyev.travelagency.db.extractor.action;

import com.melentyev.travelagency.db.extractor.Extractor;
import com.melentyev.travelagency.entity.Action;

import java.sql.ResultSet;
import java.sql.SQLException;

public class ActionExtractor implements Extractor<Action> {

    @Override
    public Action extractOne(ResultSet resultSet) throws SQLException {
        int id = resultSet.getInt("id");
        String action = resultSet.getString("action");
        return new Action(id, action);
    }
}
