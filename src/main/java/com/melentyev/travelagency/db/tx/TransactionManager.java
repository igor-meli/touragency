package com.melentyev.travelagency.db.tx;

import com.melentyev.travelagency.exception.DaoSystemException;

import javax.sql.DataSource;
import java.util.concurrent.Callable;

public interface TransactionManager extends DataSource {

    public <T> T doInTransaction(Callable<T> unitOfWork) throws DaoSystemException;

}
