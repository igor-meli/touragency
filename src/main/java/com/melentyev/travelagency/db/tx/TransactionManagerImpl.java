package com.melentyev.travelagency.db.tx;

import com.melentyev.travelagency.db.factory.BoneCPConnectionFactory;
import com.melentyev.travelagency.db.factory.ConnectionFactory;
import com.melentyev.travelagency.exception.DaoSystemException;
import com.melentyev.travelagency.util.JdbcUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.Connection;
import java.util.concurrent.Callable;

public class TransactionManagerImpl extends BaseDataSource implements TransactionManager {

    private static final TransactionManager TX = new TransactionManagerImpl();
    private static final Logger logger = LoggerFactory.getLogger(TransactionManagerImpl.class);
    private static final ThreadLocal<Connection> connectionHolder = new ThreadLocal<>();
    private static final ConnectionFactory connectionFactory = new BoneCPConnectionFactory();

    private TransactionManagerImpl() {
    }

    public static TransactionManager getTx() {
        return TX;
    }

    @Override
    public Connection getConnection() {
        return connectionHolder.get();
    }

    @Override
    public <T> T doInTransaction(Callable<T> unitOfWork) throws DaoSystemException {
        Connection connection = connectionFactory.getConnection();
        connectionHolder.set(connection);
        try {
            T result = unitOfWork.call();
            connection.commit();
            return result;
        } catch (Exception e) {
            logger.warn("Couldn't commit, exception: " + e.getMessage());
            JdbcUtil.rollback(connection);
            throw new DaoSystemException("Couldn't commit ", e);
        } finally {
            JdbcUtil.closeConnection(connection);
            connectionHolder.remove();
        }
    }
}
