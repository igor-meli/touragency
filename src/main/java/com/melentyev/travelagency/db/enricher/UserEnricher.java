package com.melentyev.travelagency.db.enricher;

import com.melentyev.travelagency.db.dao.role.RoleDao;
import com.melentyev.travelagency.db.dao.role.RoleJdbcDao;
import com.melentyev.travelagency.entity.Role;
import com.melentyev.travelagency.entity.User;
import com.melentyev.travelagency.exception.DaoSystemException;

public class UserEnricher implements Enricher<User> {

    private RoleDao roleDao = new RoleJdbcDao();

    @Override
    public void enrich(User record) throws DaoSystemException {
        Role role = roleDao.getRoleByUserId(record.getId());
        record.setRole(role);
    }
}
