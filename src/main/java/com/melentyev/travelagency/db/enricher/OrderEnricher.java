package com.melentyev.travelagency.db.enricher;

import com.melentyev.travelagency.db.dao.discountprogram.DiscountProgramDao;
import com.melentyev.travelagency.db.dao.discountprogram.DiscountProgramJdbcDao;
import com.melentyev.travelagency.db.dao.offer.OfferDao;
import com.melentyev.travelagency.db.dao.offer.OfferJdbcDao;
import com.melentyev.travelagency.db.dao.user.UserDao;
import com.melentyev.travelagency.db.dao.user.UserJdbcDao;
import com.melentyev.travelagency.entity.DiscountProgram;
import com.melentyev.travelagency.entity.Offer;
import com.melentyev.travelagency.entity.Order;
import com.melentyev.travelagency.entity.User;
import com.melentyev.travelagency.exception.DaoSystemException;

public class OrderEnricher implements Enricher<Order> {

    private UserDao userDao = new UserJdbcDao();
    private OfferDao offerDao = new OfferJdbcDao();
    private DiscountProgramDao discountProgramDao = new DiscountProgramJdbcDao();

    @Override
    public void enrich(Order order) throws DaoSystemException {
        User user = userDao.getUserByOrderId(order.getId());
        order.setUser(user);
        Offer offer = offerDao.getOfferByOrderId(order.getId());
        order.setOffer(offer);
        DiscountProgram program = discountProgramDao.getByOrderId(order.getId());
        order.setDiscountProgram(program);
    }

}
