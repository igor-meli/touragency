package com.melentyev.travelagency.db.enricher;

import com.melentyev.travelagency.db.dao.rights.RightsDao;
import com.melentyev.travelagency.db.dao.rights.RightsJdbcDao;
import com.melentyev.travelagency.entity.Rights;
import com.melentyev.travelagency.entity.Role;
import com.melentyev.travelagency.exception.DaoSystemException;

import java.util.List;

public class RoleEnricher implements Enricher<Role> {

    private RightsDao rightsDao = new RightsJdbcDao();

    @Override
    public void enrich(Role role) throws DaoSystemException {
        List<Rights> rights = rightsDao.getByRoleId(role.getId());
        role.setRights(rights);
    }
}
