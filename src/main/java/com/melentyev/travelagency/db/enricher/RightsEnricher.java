package com.melentyev.travelagency.db.enricher;

import com.melentyev.travelagency.db.dao.action.ActionDao;
import com.melentyev.travelagency.db.dao.action.ActionJdbcDao;
import com.melentyev.travelagency.entity.Action;
import com.melentyev.travelagency.entity.Rights;
import com.melentyev.travelagency.exception.DaoSystemException;

public class RightsEnricher implements Enricher<Rights> {

    private ActionDao actionDao = new ActionJdbcDao();

    @Override
    public void enrich(Rights rights) throws DaoSystemException {
        Action action = actionDao.getActionByRightsId(rights.getId());
        rights.setAction(action);
    }

}
