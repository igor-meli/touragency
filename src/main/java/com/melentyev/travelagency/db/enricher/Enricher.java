package com.melentyev.travelagency.db.enricher;

import com.melentyev.travelagency.exception.DaoSystemException;

public interface Enricher<T> {

    Enricher NONE = new Enricher() {
        @Override
        public void enrich(Object record) throws DaoSystemException {

        }
    };

    void enrich(T record) throws DaoSystemException;

}
