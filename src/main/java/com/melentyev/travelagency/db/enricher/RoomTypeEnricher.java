package com.melentyev.travelagency.db.enricher;

import com.melentyev.travelagency.db.dao.hotel.HotelDao;
import com.melentyev.travelagency.db.dao.hotel.HotelJdbcDao;
import com.melentyev.travelagency.entity.Hotel;
import com.melentyev.travelagency.entity.RoomType;
import com.melentyev.travelagency.exception.DaoSystemException;

public class RoomTypeEnricher implements Enricher<RoomType> {

    private HotelDao hotelDao = new HotelJdbcDao();

    @Override
    public void enrich(RoomType roomType) throws DaoSystemException {
        Hotel hotel = hotelDao.getByRoomTypeId(roomType.getId());
        roomType.setHotel(hotel);

    }
}
