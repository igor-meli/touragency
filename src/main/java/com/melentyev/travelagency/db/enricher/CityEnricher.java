package com.melentyev.travelagency.db.enricher;

import com.melentyev.travelagency.db.dao.country.CountryDao;
import com.melentyev.travelagency.db.dao.country.CountryJdbcDao;
import com.melentyev.travelagency.entity.City;
import com.melentyev.travelagency.entity.Country;
import com.melentyev.travelagency.exception.DaoSystemException;

public class CityEnricher implements Enricher<City> {

    private CountryDao countryDao = new CountryJdbcDao();

    @Override
    public void enrich(City city) throws DaoSystemException {
        Country country = countryDao.getByCityId(city.getId());
        city.setCountry(country);
    }
}
