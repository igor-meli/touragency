package com.melentyev.travelagency.db.enricher;

import com.melentyev.travelagency.db.dao.tour.TourDao;
import com.melentyev.travelagency.db.dao.tour.TourJdbcDao;
import com.melentyev.travelagency.entity.Offer;
import com.melentyev.travelagency.entity.Tour;
import com.melentyev.travelagency.exception.DaoSystemException;

public class OfferEnricher implements Enricher<Offer> {

    private TourDao tourDao = new TourJdbcDao();


    @Override
    public void enrich(Offer offer) throws DaoSystemException {
        Tour tour = tourDao.getToursByOfferId(offer.getId());
        offer.setTour(tour);
    }
}
