package com.melentyev.travelagency.db.enricher;

import com.melentyev.travelagency.db.dao.city.CityDao;
import com.melentyev.travelagency.db.dao.city.CityJdbcDao;
import com.melentyev.travelagency.db.dao.roomtype.RoomTypeDao;
import com.melentyev.travelagency.db.dao.roomtype.RoomTypeJdbcDao;
import com.melentyev.travelagency.entity.City;
import com.melentyev.travelagency.entity.Hotel;
import com.melentyev.travelagency.entity.RoomType;
import com.melentyev.travelagency.exception.DaoSystemException;

import java.util.List;

public class HotelEnricher implements Enricher<Hotel> {

    private CityDao cityDao = new CityJdbcDao();
    private RoomTypeDao roomTypeDao = new RoomTypeJdbcDao();

    @Override
    public void enrich(Hotel hotel) throws DaoSystemException {
        City city = cityDao.getByHotelId(hotel.getId());
        hotel.setCity(city);
        List<RoomType> allHotelRooms = roomTypeDao.getAllHotelRooms(hotel.getId());
        hotel.setRoomTypes(allHotelRooms);
    }
}
