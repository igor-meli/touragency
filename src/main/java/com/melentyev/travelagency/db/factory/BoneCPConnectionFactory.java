package com.melentyev.travelagency.db.factory;

import com.jolbox.bonecp.BoneCPDataSource;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.Connection;
import java.sql.SQLException;

import static com.melentyev.travelagency.util.DatabaseConfigUtil.getConfig;

public class BoneCPConnectionFactory implements ConnectionFactory {

    private static final Logger logger = LoggerFactory.getLogger(BoneCPConnectionFactory.class);
    private BoneCPDataSource dataSource;

    public BoneCPConnectionFactory() {
        dataSource = new BoneCPDataSource();
        dataSource.setDriverClass(getConfig("driver"));
        dataSource.setJdbcUrl(getConfig("jdbc-Url"));
        dataSource.setUsername(getConfig("username"));
        dataSource.setPassword(getConfig("password"));
        dataSource.setDefaultTransactionIsolation(getConfig("transactionIsolation"));
        dataSource.setDefaultAutoCommit(false);
        dataSource.setAcquireIncrement(Integer.parseInt(getConfig("acquireIncrement")));
        dataSource.setPartitionCount(Integer.parseInt(getConfig("partitionCount")));
        dataSource.setMaxConnectionsPerPartition(Integer.parseInt(getConfig("maxConnectionsPerPartition")));
        dataSource.setMinConnectionsPerPartition(Integer.parseInt(getConfig("minConnectionsPerPartition")));
    }

    @Override
    public Connection getConnection() {
        try {
            return dataSource.getConnection();
        } catch (SQLException e) {
            logger.error("Connection wasn't set");
            throw new IllegalStateException("Connection wasn't set");
        }
    }
}
