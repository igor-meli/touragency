package com.melentyev.travelagency.db.factory;

import java.sql.Connection;

public interface ConnectionFactory {

    Connection getConnection();

}
