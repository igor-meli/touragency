package com.melentyev.travelagency.util;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.Properties;

public class SQLStorage {

    private static final Logger logger = LoggerFactory.getLogger(SQLStorage.class);
    private static final Properties properties = new Properties();

    static {
        try {
            properties.load(SQLStorage.class.getResourceAsStream("/sql.properties"));
        } catch (IOException e) {
            logger.error("Can't load database config file: 'sql.properties'");
        }
    }

    public static String getSql(String key) {
        return properties.getProperty(key);
    }
}
