package com.melentyev.travelagency.util;

import java.util.ArrayList;
import java.util.List;

public class SelectBuilder {

    private List<String> columns = new ArrayList<>();
    private List<String> tables = new ArrayList<>();
    private List<String> joins = new ArrayList<>();
    private List<String> leftJoins = new ArrayList<>();
    private List<String> wheres = new ArrayList<>();
    private List<String> orderBys = new ArrayList<>();
    private List<String> groupBys = new ArrayList<>();
    private List<String> havings = new ArrayList<>();

    public SelectBuilder() {
    }

    public SelectBuilder(String table) {
        tables.add(table);
    }

    public SelectBuilder column(String name) {
        columns.add(name);
        return this;
    }

    public SelectBuilder column(String name, boolean groupBy) {
        columns.add(name);
        if (groupBy) {
            groupBys.add(name);
        }
        return this;
    }

    public SelectBuilder from(String table) {
        tables.add(table);
        return this;
    }

    public SelectBuilder groupBy(String expr) {
        groupBys.add(expr);
        return this;
    }

    public SelectBuilder having(String expr) {
        havings.add(expr);
        return this;
    }

    public SelectBuilder join(String join) {
        joins.add(join);
        return this;
    }

    public SelectBuilder leftJoin(String join) {
        leftJoins.add(join);
        return this;
    }

    public SelectBuilder orderBy(String name) {
        orderBys.add(name);
        return this;
    }

    @Override
    public String toString() {

        StringBuilder sql = new StringBuilder("SELECT ");

        if (columns.size() == 0) {
            sql.append("*");
        } else {
            appendList(sql, columns, "", ", ");
        }

        appendList(sql, tables, " FROM ", ", ");
        appendList(sql, joins, " INNER JOIN ", " INNER JOIN ");
        appendList(sql, leftJoins, " LEFT JOIN ", " LEFT JOIN ");
        appendList(sql, wheres, " WHERE ", " AND ");
        appendList(sql, groupBys, " GROUP BY ", ", ");
        appendList(sql, havings, " HAVING ", " AND ");
        appendList(sql, orderBys, " ORDER BY ", ", ");

        return sql.toString();
    }

    private void appendList(StringBuilder sql, List<String> list, String init, String sep) {
        boolean first = true;
        for (String s : list) {
            if (first) {
                sql.append(init);
            } else {
                sql.append(sep);
            }
            sql.append(s);
            first = false;
        }
    }

    public SelectBuilder where(String expr) {
        wheres.add(expr);
        return this;
    }

}