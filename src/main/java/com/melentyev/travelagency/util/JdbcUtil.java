package com.melentyev.travelagency.util;

import com.melentyev.travelagency.exception.DaoSystemException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.Connection;
import java.sql.SQLException;

public class JdbcUtil {

    private static final Logger logger = LoggerFactory.getLogger(JdbcUtil.class);

    public static void closeConnection(Connection connection) throws DaoSystemException {
        try {
            connection.close();
        } catch (SQLException e) {
            logger.error("Couldn't close connection, exception: " + e.getMessage());
            throw new DaoSystemException("Couldn't close connection", e);
        }
    }

    public static void rollback(Connection connection) throws DaoSystemException {
        try {
            connection.rollback();
        } catch (SQLException e) {
            logger.error("Couldn't rollback, exception: " + e.getMessage());
            throw new DaoSystemException("Couldn't rollback ", e);
        }
    }

}
