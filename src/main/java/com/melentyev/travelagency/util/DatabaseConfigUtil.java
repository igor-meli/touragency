package com.melentyev.travelagency.util;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.Properties;

public class DatabaseConfigUtil {

    private static final Logger logger = LoggerFactory.getLogger(DatabaseConfigUtil.class);
    private static final Properties properties = new Properties();
    static {
        try {
            properties.load(DatabaseConfigUtil.class.getResourceAsStream("/database.properties"));
        } catch (IOException e) {
            logger.error("Can't load database config file: 'database.properties'");
        }
    }

    public static String getConfig(String key) {
        return properties.getProperty(key);
    }
}
