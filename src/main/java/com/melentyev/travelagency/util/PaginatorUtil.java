package com.melentyev.travelagency.util;

import javax.servlet.http.HttpServletRequest;

public class PaginatorUtil {

    public static void initPaginatorToRequest(int currPage, int rowsCount, int numberOfRows, HttpServletRequest request) {
        int totalPages = (int) Math.ceil((double) rowsCount / (double) numberOfRows);
        request.setAttribute("totalPages", totalPages);
        request.setAttribute("currPage", currPage + 1);
    }

}
