package com.melentyev.travelagency.service;

import com.melentyev.travelagency.db.dao.hotelType.HotelTypeDao;
import com.melentyev.travelagency.db.dao.hotelType.HotelTypeJdbcDao;
import com.melentyev.travelagency.db.tx.TransactionManager;
import com.melentyev.travelagency.db.tx.TransactionManagerImpl;
import com.melentyev.travelagency.entity.HotelType;
import com.melentyev.travelagency.exception.DaoSystemException;

import java.util.List;
import java.util.concurrent.Callable;

public class HotelTypeService {

    private static final TransactionManager tx = TransactionManagerImpl.getTx();
    private HotelTypeDao hotelTypeDao = new HotelTypeJdbcDao();

    public List<HotelType> getAllHotelTypes() throws DaoSystemException {
        return tx.doInTransaction(new Callable<List<HotelType>>() {
            @Override
            public List<HotelType> call() throws Exception {
                return hotelTypeDao.getAll();
            }
        });
    }
}
