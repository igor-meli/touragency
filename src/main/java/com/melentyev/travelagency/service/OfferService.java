package com.melentyev.travelagency.service;

import com.melentyev.travelagency.db.dao.offer.OfferDao;
import com.melentyev.travelagency.db.dao.offer.OfferJdbcDao;
import com.melentyev.travelagency.db.tx.TransactionManager;
import com.melentyev.travelagency.db.tx.TransactionManagerImpl;
import com.melentyev.travelagency.entity.Hotel;
import com.melentyev.travelagency.entity.Offer;
import com.melentyev.travelagency.entity.RoomType;
import com.melentyev.travelagency.entity.Tour;
import com.melentyev.travelagency.exception.DaoSystemException;

import java.util.List;
import java.util.concurrent.Callable;

public class OfferService {

    private static final TransactionManager tx = TransactionManagerImpl.getTx();
    private OfferDao offerDao = new OfferJdbcDao();

    public List<Offer> getAllOffers() throws DaoSystemException {
        return tx.doInTransaction(new Callable<List<Offer>>() {
            @Override
            public List<Offer> call() throws Exception {
                return offerDao.getAll();
            }
        });
    }

    public Offer getOfferById(final int id) throws DaoSystemException {
        return tx.doInTransaction(new Callable<Offer>() {
            @Override
            public Offer call() throws Exception {
                return offerDao.getById(id);
            }
        });
    }

    public void updateOffer(final Offer offer) throws DaoSystemException {
        tx.doInTransaction(new Callable<Void>() {
            @Override
            public Void call() throws Exception {
                offerDao.update(offer);
                return null;
            }
        });
    }

    public void addOffer(final Offer offer) throws DaoSystemException {
        tx.doInTransaction(new Callable<Void>() {
            @Override
            public Void call() throws Exception {
                offerDao.add(offer);
                return null;
            }
        });
    }

    public boolean isOfferExist(final Offer offer) throws DaoSystemException {
        if (offer == null)
            return false;
        Offer comparedOffer = tx.doInTransaction(new Callable<Offer>() {
            @Override
            public Offer call() throws Exception {
                Hotel hotel = offer.getHotel();
                Tour tour = offer.getTour();
                RoomType roomType = offer.getRoomType();
                if (hotel == null || tour == null) {
                    return null;
                }
                return offerDao.getByHotelIdTourIdAndRoomId(hotel.getId(), tour.getId(), roomType.getId());
            }
        });
        return comparedOffer != null;
    }

    public void deleteById(final int id) throws DaoSystemException {
        tx.doInTransaction(new Callable<Void>() {
            @Override
            public Void call() throws Exception {
                offerDao.deleteById(id);
                return null;
            }
        });
    }
}
