package com.melentyev.travelagency.service;

import com.melentyev.travelagency.db.dao.tourType.TourTypeDao;
import com.melentyev.travelagency.db.dao.tourType.TourTypeJdbcDao;
import com.melentyev.travelagency.db.tx.TransactionManager;
import com.melentyev.travelagency.db.tx.TransactionManagerImpl;
import com.melentyev.travelagency.entity.TourType;
import com.melentyev.travelagency.exception.DaoSystemException;

import java.util.List;
import java.util.concurrent.Callable;

public class TourTypeService {

    private static final TransactionManager tx = TransactionManagerImpl.getTx();
    private TourTypeDao tourTypeDao = new TourTypeJdbcDao();

    public TourType getTourTypeById(final int id) throws DaoSystemException {
        return tx.doInTransaction(new Callable<TourType>() {
            @Override
            public TourType call() throws Exception {
                return tourTypeDao.getById(id);
            }
        });
    }

    public List<TourType> getAllTourTypes() throws DaoSystemException {
        return tx.doInTransaction(new Callable<List<TourType>>() {
            @Override
            public List<TourType> call() throws Exception {
                return tourTypeDao.getAll();
            }
        });
    }
}
