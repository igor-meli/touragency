package com.melentyev.travelagency.service;

import com.melentyev.travelagency.db.dao.offer.OfferDao;
import com.melentyev.travelagency.db.dao.offer.OfferJdbcDao;
import com.melentyev.travelagency.db.dao.tour.TourDao;
import com.melentyev.travelagency.db.dao.tour.TourJdbcDao;
import com.melentyev.travelagency.db.tx.TransactionManager;
import com.melentyev.travelagency.db.tx.TransactionManagerImpl;
import com.melentyev.travelagency.entity.Offer;
import com.melentyev.travelagency.entity.Tour;
import com.melentyev.travelagency.exception.DaoSystemException;

import java.util.List;
import java.util.concurrent.Callable;

public class TourService {

    private static final TransactionManager tx = TransactionManagerImpl.getTx();
    private TourDao tourDao = new TourJdbcDao();
    private OfferDao offerDao = new OfferJdbcDao();

    public List<Tour> getAllToursLimit(final int offset, final int rows) throws DaoSystemException {
        return tx.doInTransaction(new Callable<List<Tour>>() {
            @Override
            public List<Tour> call() throws Exception {
                return tourDao.getAll(offset, rows);
            }
        });
    }

    public int getRowsCount() throws DaoSystemException {
        return tx.doInTransaction(new Callable<Integer>() {
            @Override
            public Integer call() throws Exception {
                return tourDao.getToursCount();
            }
        });
    }

    public Tour getTourById(final int id) throws DaoSystemException {
        return tx.doInTransaction(new Callable<Tour>() {
            @Override
            public Tour call() throws Exception {
                Tour tour = tourDao.getById(id);
                List<Offer> offers = offerDao.getOffersByTourId(tour.getId());
                tour.setOfferList(offers);
                return tour;
            }
        });
    }

    public List<Tour> searchTours(final String sql, final List<Integer> params) throws DaoSystemException {
        return tx.doInTransaction(new Callable<List<Tour>>() {
            @Override
            public List<Tour> call() throws Exception {
                return tourDao.searchTours(sql, params);
            }
        });
    }

    public List<Tour> getAllTours() throws DaoSystemException {
        return tx.doInTransaction(new Callable<List<Tour>>() {
            @Override
            public List<Tour> call() throws Exception {
                return tourDao.getAll();
            }
        });
    }

    public boolean isTourExists(final Tour tour) throws DaoSystemException {
        if (tour == null) {
            return false;
        }
        Tour compareTour = tx.doInTransaction(new Callable<Tour>() {
            @Override
            public Tour call() throws Exception {
                return tourDao.getByName(tour.getName());
            }
        });
        return compareTour != null && compareTour.getId() != tour.getId();
    }

    public void addTour(final Tour tour) throws DaoSystemException {
        tx.doInTransaction(new Callable<Void>() {
            @Override
            public Void call() throws Exception {
                tourDao.add(tour);
                return null;
            }
        });
    }

    public void deleteById(final int id) throws DaoSystemException {
        tx.doInTransaction(new Callable<Void>() {
            @Override
            public Void call() throws Exception {
                tourDao.deleteById(id);
                return null;
            }
        });
    }

    public void update(final Tour tour) throws DaoSystemException {
        if (tour != null) {
            tx.doInTransaction(new Callable<Void>() {
                @Override
                public Void call() throws Exception {
                    tourDao.update(tour);
                    return null;
                }
            });
        }
    }

    public void changeTourHotness(final int id) throws DaoSystemException {
        tx.doInTransaction(new Callable<Void>() {
            @Override
            public Void call() throws DaoSystemException {
                Tour tour = tourDao.getById(id);
                tour.setHot(!tour.isHot());
                tourDao.update(tour);
                return null;
            }
        });
    }
}
