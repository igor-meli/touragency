package com.melentyev.travelagency.service;

import com.melentyev.travelagency.db.dao.roomtype.RoomTypeDao;
import com.melentyev.travelagency.db.dao.roomtype.RoomTypeJdbcDao;
import com.melentyev.travelagency.db.tx.TransactionManager;
import com.melentyev.travelagency.db.tx.TransactionManagerImpl;
import com.melentyev.travelagency.entity.RoomType;
import com.melentyev.travelagency.exception.DaoSystemException;

import java.util.List;
import java.util.concurrent.Callable;

public class RoomTypeService {

    private static final TransactionManager tx = TransactionManagerImpl.getTx();
    private RoomTypeDao roomTypeDao = new RoomTypeJdbcDao();

    public boolean isRoomTypeExists(final RoomType roomType) throws DaoSystemException {
        if (roomType == null) {
            return false;
        }
        RoomType compareRoomType = tx.doInTransaction(new Callable<RoomType>() {
            @Override
            public RoomType call() throws Exception {
                return roomTypeDao.getByNameAndHotelId(roomType.getName(), roomType.getHotel().getId());
            }
        });
        return compareRoomType != null && compareRoomType.getId() != roomType.getId();
    }

    public List<RoomType> getAllRoomTypes() throws DaoSystemException {
        return tx.doInTransaction(new Callable<List<RoomType>>() {
            @Override
            public List<RoomType> call() throws Exception {
                return roomTypeDao.getAll();
            }
        });
    }

    public void addRoomType(final RoomType roomType) throws DaoSystemException {
        tx.doInTransaction(new Callable<Void>() {
            @Override
            public Void call() throws Exception {
                roomTypeDao.add(roomType);
                return null;
            }
        });
    }

    public void deleteById(final int roomTypeId) throws DaoSystemException {
        tx.doInTransaction(new Callable<Void>() {
            @Override
            public Void call() throws Exception {
                roomTypeDao.deleteById(roomTypeId);
                return null;
            }
        });
    }

    public RoomType getRoomTypeById(final int id) throws DaoSystemException {
        return tx.doInTransaction(new Callable<RoomType>() {
            @Override
            public RoomType call() throws Exception {
                return roomTypeDao.getById(id);
            }
        });
    }

    public void updateRoomType(final RoomType roomType) throws DaoSystemException {
        tx.doInTransaction(new Callable<Void>() {
            @Override
            public Void call() throws Exception {
                roomTypeDao.update(roomType);
                return null;
            }
        });

    }

    public List<RoomType> getRoomTypeByHotelId(final int id) throws DaoSystemException {
        return tx.doInTransaction(new Callable<List<RoomType>>() {
            @Override
            public List<RoomType> call() throws Exception {
                return roomTypeDao.getAllHotelRooms(id);
            }
        });
    }
}
