package com.melentyev.travelagency.service;

import com.melentyev.travelagency.db.dao.discountprogram.DiscountProgramDao;
import com.melentyev.travelagency.db.dao.discountprogram.DiscountProgramJdbcDao;
import com.melentyev.travelagency.db.dao.offer.OfferDao;
import com.melentyev.travelagency.db.dao.offer.OfferJdbcDao;
import com.melentyev.travelagency.db.dao.order.OrderDao;
import com.melentyev.travelagency.db.dao.order.OrderJdbcDao;
import com.melentyev.travelagency.db.dao.status.StatusDao;
import com.melentyev.travelagency.db.dao.status.StatusJdbcDao;
import com.melentyev.travelagency.db.tx.TransactionManager;
import com.melentyev.travelagency.db.tx.TransactionManagerImpl;
import com.melentyev.travelagency.entity.*;
import com.melentyev.travelagency.exception.DaoException;
import com.melentyev.travelagency.exception.DaoSystemException;
import com.melentyev.travelagency.exception.ForbiddenActionException;
import com.melentyev.travelagency.exception.NoSuchEntityException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;
import java.util.concurrent.Callable;

public class OrderService {

    private static final Logger logger = LoggerFactory.getLogger(OrderService.class);
    private static final TransactionManager tx = TransactionManagerImpl.getTx();
    private OrderDao orderDao = new OrderJdbcDao();
    private DiscountProgramDao discountDao = new DiscountProgramJdbcDao();
    private OfferDao offerDao = new OfferJdbcDao();
    private StatusDao statusDao = new StatusJdbcDao();

    public Order getOrderById(final int id) throws DaoSystemException {
        return tx.doInTransaction(new Callable<Order>() {
            @Override
            public Order call() throws Exception {
                return orderDao.getById(id);
            }
        });
    }

    public void updateOrderDiscount(final Order order, int discount) throws DaoSystemException {
        int maxDiscount = getMaxOrderDiscount(order.getId());
        if (discount >= 0 && discount <= maxDiscount) {
            order.setDiscount(discount);
            tx.doInTransaction(new Callable<Void>() {
                @Override
                public Void call() throws Exception {
                    orderDao.update(order);
                    return null;
                }
            });
        }

    }

    private int getMaxOrderDiscount(final int orderId) throws DaoSystemException {
        return tx.doInTransaction(new Callable<Integer>() {
            @Override
            public Integer call() throws Exception {
                DiscountProgram program = discountDao.getByOrderId(orderId);
                return program != null ? program.getMax() : 0;
            }
        });
    }

    public void updateOrderDiscountProgram(final Order order, final int discountId) throws DaoSystemException {
        tx.doInTransaction(new Callable<Void>() {
            @Override
            public Void call() throws Exception {
                DiscountProgram discountProgram = discountDao.getById(discountId);
                order.setDiscountProgram(discountProgram);
                order.setDiscount(0);
                orderDao.update(order);
                return null;
            }
        });
    }

    public void bookOfferByUser(final int id, final User user) throws Exception {
        tx.doInTransaction(new Callable<Object>() {
            @Override
            public Object call() throws Exception {
                Offer offer = offerDao.getById(id);
                if (offer == null) {
                    logger.info("Offer with id = " + id + " not found");
                    throw new NoSuchEntityException("Offer with id = " + id + " not found");
                }
                Status status = statusDao.getDefault();
                Order order = new Order();
                order.setStatus(status);
                order.setOffer(offer);
                order.setUser(user);
                orderDao.add(order);
                return null;
            }
        });
    }

    public void cancelOrder(final int id, final User user) throws DaoSystemException {
        tx.doInTransaction(new Callable<Void>() {
            @Override
            public Void call() throws DaoException {
                Order order = orderDao.getById(id);
                if (order == null) {
                    logger.info("Order with id = " + id + " not found");
                    throw new NoSuchEntityException("Order with id = " + id + " not found");
                }
                if (!order.getUser().equals(user)) {
                    logger.info("User with id = " + user.getId() + " cannot cancel order with id " + id);
                    throw new ForbiddenActionException("User id = " + user.getId() + " cannot cancel order with id " + id);
                }
                Status status = statusDao.getCanceled();
                if (status == null) {
                    logger.info("Canceled Status not found");
                    throw new NoSuchEntityException("Canceled Status not found");
                }
                order.setStatus(status);
                order.setApprovedUser(null);
                orderDao.update(order);
                return null;
            }
        });
    }

    public void changeStatus(final int orderId, final int changedStatusId, final User user) throws DaoSystemException {
        tx.doInTransaction(new Callable<Void>() {
            @Override
            public Void call() throws DaoSystemException, NoSuchEntityException {
                Status status = statusDao.getById(changedStatusId);
                if (status == null) {
                    throw new NoSuchEntityException("Status with id " + changedStatusId + " not found");
                }
                Order order = orderDao.getById(orderId);
                if (order == null) {
                    throw new NoSuchEntityException("Order with id " + orderId + " not found");
                }
                order.setStatus(status);
                Status approvedStatus = statusDao.getPaid();
                if (status.equals(approvedStatus)) {
                    if (user != null) {
                        order.setApprovedUser(user);
                    }
                } else {
                    order.setApprovedUser(null);
                }
                orderDao.update(order);

                return null;
            }
        });
    }

    public int getOrdersCount() throws DaoSystemException {
        return tx.doInTransaction(new Callable<Integer>() {
            @Override
            public Integer call() throws Exception {
                return orderDao.getOrdersCount();
            }
        });
    }

    public List<Order> getAllOrders(final int offset, final int rows) throws DaoSystemException {
        return tx.doInTransaction(new Callable<List<Order>>() {
            @Override
            public List<Order> call() throws Exception {
                return orderDao.getAll(offset, rows);
            }
        });
    }

    public int getUserOrdersCount(final int userId) throws DaoSystemException {
        return tx.doInTransaction(new Callable<Integer>() {
            @Override
            public Integer call() throws Exception {
                return orderDao.getUserOrdersCount(userId);
            }
        });
    }

    public List<Order> getAllUserService(final int id, final int offset, final int rows) throws DaoSystemException {
        return tx.doInTransaction(new Callable<List<Order>>() {
            @Override
            public List<Order> call() throws Exception {
                return orderDao.getAllUserOrders(id, offset, rows);
            }
        });
    }
}
