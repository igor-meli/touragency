package com.melentyev.travelagency.service;

import com.melentyev.travelagency.db.dao.role.RoleDao;
import com.melentyev.travelagency.db.dao.role.RoleJdbcDao;
import com.melentyev.travelagency.db.dao.user.UserDao;
import com.melentyev.travelagency.db.dao.user.UserJdbcDao;
import com.melentyev.travelagency.db.tx.TransactionManager;
import com.melentyev.travelagency.db.tx.TransactionManagerImpl;
import com.melentyev.travelagency.entity.Role;
import com.melentyev.travelagency.entity.User;
import com.melentyev.travelagency.exception.DaoSystemException;
import com.melentyev.travelagency.exception.NoSuchEntityException;

import java.util.List;
import java.util.concurrent.Callable;

public class UserService {

    private static final TransactionManager tx = TransactionManagerImpl.getTx();
    private UserDao userDao = new UserJdbcDao();
    private RoleDao roleDao = new RoleJdbcDao();


    public void blockUser(final int id) throws DaoSystemException {
        tx.doInTransaction(new Callable<Void>() {
            @Override
            public Void call() throws Exception {
                User user = userDao.getById(id);
                if (user == null) {
                    throw new NoSuchEntityException("User with id " + id + " doesn't exist");
                }
                user.setBlocked(!user.isBlocked());
                userDao.update(user);
                return null;
            }
        });
    }

    public int getRowsCount() throws DaoSystemException {
        return tx.doInTransaction(new Callable<Integer>() {
            @Override
            public Integer call() throws Exception {
                return userDao.getRowsCount();
            }
        });
    }

    public List<User> getAllUsersLimit(final int offset, final int numbersOfRows) throws DaoSystemException {
        return tx.doInTransaction(new Callable<List<User>>() {
            @Override
            public List<User> call() throws Exception {
                return userDao.getAll(offset, numbersOfRows);
            }
        });
    }

    public User getUser(final String login, final String password) throws Exception {
        return tx.doInTransaction(new Callable<User>() {
            @Override
            public User call() throws Exception {
                return userDao.getUserByLoginAndPassword(login, password);
            }
        });
    }

    public User getUser(final int id) throws DaoSystemException {
        return tx.doInTransaction(new Callable<User>() {
            @Override
            public User call() throws Exception {
                return userDao.getById(id);
            }
        });
    }

    public void updateUser(final User user) throws DaoSystemException {
        tx.doInTransaction(new Callable<Void>() {
            @Override
            public Void call() throws DaoSystemException {
                userDao.update(user);
                return null;
            }
        });
    }

    public User saveUser(final User user) throws Exception {
        return tx.doInTransaction(new Callable<User>() {
            @Override
            public User call() throws Exception {
                Role role = roleDao.getCustomerRole();
                user.setRole(role);
                userDao.add(user);
                return userDao.getUserByLogin(user.getLogin());
            }
        });
    }

    public boolean isLoginExists(final String login) throws Exception {
        User existedUser = tx.doInTransaction(new Callable<User>() {
            @Override
            public User call() throws Exception {
                return userDao.getUserByLogin(login);
            }
        });
        return existedUser != null;
    }

    public List<User> getAllManagers() throws DaoSystemException {
        return tx.doInTransaction(new Callable<List<User>>() {
            @Override
            public List<User> call() throws Exception {
                return userDao.getAllManagers();
            }
        });
    }
}
