package com.melentyev.travelagency.service;

import com.melentyev.travelagency.db.dao.status.StatusDao;
import com.melentyev.travelagency.db.dao.status.StatusJdbcDao;
import com.melentyev.travelagency.db.tx.TransactionManager;
import com.melentyev.travelagency.db.tx.TransactionManagerImpl;
import com.melentyev.travelagency.entity.Status;
import com.melentyev.travelagency.exception.DaoSystemException;

import java.util.List;
import java.util.concurrent.Callable;

public class StatusService {

    private static final TransactionManager tx = TransactionManagerImpl.getTx();
    private StatusDao statusDao = new StatusJdbcDao();

    public List<Status> getAllStatus() throws DaoSystemException {
        return tx.doInTransaction(new Callable<List<Status>>() {
            @Override
            public List<Status> call() throws Exception {
                return statusDao.getAll();
            }
        });
    }
}
