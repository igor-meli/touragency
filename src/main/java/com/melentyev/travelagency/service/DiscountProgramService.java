package com.melentyev.travelagency.service;

import com.melentyev.travelagency.db.dao.discountprogram.DiscountProgramDao;
import com.melentyev.travelagency.db.dao.discountprogram.DiscountProgramJdbcDao;
import com.melentyev.travelagency.db.tx.TransactionManager;
import com.melentyev.travelagency.db.tx.TransactionManagerImpl;
import com.melentyev.travelagency.entity.DiscountProgram;
import com.melentyev.travelagency.exception.DaoSystemException;

import java.util.List;
import java.util.concurrent.Callable;

public class DiscountProgramService {

    private static final TransactionManager tx = TransactionManagerImpl.getTx();
    private DiscountProgramDao discountDao = new DiscountProgramJdbcDao();

    public List<DiscountProgram> getAllDiscountPrograms() throws DaoSystemException {
        return tx.doInTransaction(new Callable<List<DiscountProgram>>() {
            @Override
            public List<DiscountProgram> call() throws Exception {
                return discountDao.getAll();
            }
        });
    }

    public void addDiscountProgram(final DiscountProgram discountProgram) throws DaoSystemException {
        tx.doInTransaction(new Callable<Void>() {
            @Override
            public Void call() throws Exception {
                discountDao.add(discountProgram);
                return null;
            }
        });
    }

    public boolean isDiscountExists(final DiscountProgram discount) throws DaoSystemException {
        if (discount == null) {
            return false;
        }
        DiscountProgram compareDiscount = tx.doInTransaction(new Callable<DiscountProgram>() {
            @Override
            public DiscountProgram call() throws Exception {
                return discountDao.getByName(discount.getName());
            }
        });
        return compareDiscount != null && compareDiscount.getId() != discount.getId();
    }

    public void deleteById(final int discountId) throws DaoSystemException {
        tx.doInTransaction(new Callable<Void>() {
            @Override
            public Void call() throws Exception {
                discountDao.deleteById(discountId);
                return null;
            }
        });
    }

    public void updateDiscountProgram(final DiscountProgram discountProgram) throws DaoSystemException {
        tx.doInTransaction(new Callable<Void>() {
            @Override
            public Void call() throws Exception {
                discountDao.update(discountProgram);
                return null;
            }
        });
    }

    public DiscountProgram getDiscountById(final int id) throws DaoSystemException {

        return tx.doInTransaction(new Callable<DiscountProgram>() {
            @Override
            public DiscountProgram call() throws Exception {
                return discountDao.getById(id);
            }
        });
    }
}
