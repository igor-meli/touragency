package com.melentyev.travelagency.service;

import com.melentyev.travelagency.db.dao.hotel.HotelDao;
import com.melentyev.travelagency.db.dao.hotel.HotelJdbcDao;
import com.melentyev.travelagency.db.tx.TransactionManager;
import com.melentyev.travelagency.db.tx.TransactionManagerImpl;
import com.melentyev.travelagency.entity.Hotel;
import com.melentyev.travelagency.exception.DaoSystemException;

import java.util.List;
import java.util.concurrent.Callable;

public class HotelService {

    private static final TransactionManager tx = TransactionManagerImpl.getTx();
    private HotelDao hotelDao = new HotelJdbcDao();

    public Hotel getHotelById(final int id) throws DaoSystemException {
        return tx.doInTransaction(new Callable<Hotel>() {
            @Override
            public Hotel call() throws Exception {
                return hotelDao.getById(id);
            }
        });
    }

    public List<Hotel> getAllHotels() throws DaoSystemException {
        return tx.doInTransaction(new Callable<List<Hotel>>() {
            @Override
            public List<Hotel> call() throws Exception {
                return hotelDao.getAll();
            }
        });
    }

}
