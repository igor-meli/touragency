package com.melentyev.travelagency.validator;

import com.melentyev.travelagency.entity.Tour;

import java.util.HashMap;
import java.util.Map;

public class TourValidator implements Validator<Tour> {

    private static final int minNameLength = 4;
    private static final int maxNameLength = 100;
    private static final int minDescLength = 2;
    private static final int maxDescLength = 500;

    @Override
    public Map<String, String> validate(Tour tour) {
        Map<String, String> errorMap = new HashMap<>();
        validateName(tour.getName(), errorMap);
        validateDesc(tour.getDescription(), errorMap);
        return errorMap;
    }

    private void validateName(String name, Map<String, String> errorMap) {
        if (name == null || name.trim().isEmpty()) {
            errorMap.put("tourName", "empty");
            return;
        }
        if (name.length() < minNameLength) {
            errorMap.put("tourName", "tooShort");
            return;
        }
        if (name.length() > maxNameLength) {
            errorMap.put("tourName", "tooLong");
        }
    }

    private void validateDesc(String description, Map<String, String> errorMap) {
        if (description == null || description.trim().isEmpty()) {
            errorMap.put("description", "empty");
            return;
        }
        if (description.length() < minDescLength) {
            errorMap.put("description", "tooShort");
            return;
        }
        if (description.length() > maxDescLength) {
            errorMap.put("description", "tooLong");
        }
    }

}
