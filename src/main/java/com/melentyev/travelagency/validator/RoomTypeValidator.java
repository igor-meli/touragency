package com.melentyev.travelagency.validator;

import com.melentyev.travelagency.entity.RoomType;

import java.util.HashMap;
import java.util.Map;

public class RoomTypeValidator implements Validator<RoomType> {

    private static final int minNameLength = 4;
    private static final int maxNameLength = 30;
    private static final int minAdults = 1;
    private static final int maxAdults = 10;
    private static final int minChildren = 0;
    private static final int maxChildren = 10;

    @Override
    public Map<String, String> validate(RoomType entity) {
        Map<String, String> errorMap = new HashMap<>();
        validateName(entity.getName(), errorMap);
        validateAdults(entity.getMaxAdults(), errorMap);
        validateChildren(entity.getMaxChildren(), errorMap);
        return errorMap;
    }

    private void validateName(String name, Map<String, String> errorMap) {
        if (name == null || name.trim().isEmpty()) {
            errorMap.put("roomtypeName", "empty");
            return;
        }
        if (name.length() < minNameLength) {
            errorMap.put("roomtypeName", "tooShort");
            return;
        }
        if (name.length() > maxNameLength) {
            errorMap.put("roomtypeName", "tooLong");
        }
    }

    private void validateAdults(int adults, Map<String, String> errorMap) {
        if (adults < minAdults) {
            errorMap.put("maxAdults", "tooShort");
            return;
        }
        if (adults > maxAdults) {
            errorMap.put("maxAdults", "tooLong");
        }
    }

    private void validateChildren(int children, Map<String, String> errorMap) {
        if (children < minChildren) {
            errorMap.put("maxChildren", "tooShort");
            return;
        }
        if (children > maxChildren) {
            errorMap.put("maxChildren", "tooLong");
        }
    }

}
