package com.melentyev.travelagency.validator;

import com.melentyev.travelagency.entity.DiscountProgram;

import java.util.HashMap;
import java.util.Map;

public class DiscountValidator implements Validator<DiscountProgram> {

    private final static int minDiscountName = 4;
    private final static int maxDiscountName = 30;
    private final static int minStep = 1;
    private final static int maxStep = 100;
    private final static int minMax = 0;
    private final static int maxMax = 100;

    @Override
    public Map<String, String> validate(DiscountProgram entity) {
        Map<String, String> errorMap = new HashMap<>();
        validateName(entity.getName(), errorMap);
        validateStep(entity.getStep(), errorMap);
        validateMax(entity.getMax(), errorMap);
        if (entity.getStep() > entity.getMax()) {
            errorMap.put("step", "stepGreaterMax");
        }
        return errorMap;
    }

    private void validateName(String discountName, Map<String, String> errorMap) {
        if (discountName == null || discountName.trim().isEmpty()) {
            errorMap.put("discountName", "empty");
            return;
        }
        if (discountName.length() < minDiscountName) {
            errorMap.put("discountName", "tooShort");
            return;
        }
        if (discountName.length() > maxDiscountName) {
            errorMap.put("discountName", "tooLong");
        }

    }

    private void validateStep(int step, Map<String, String> errorMap) {
        if (step < minStep) {
            errorMap.put("step", "tooShort");
            return;
        }
        if (step > maxStep) {
            errorMap.put("step", "tooLong");
        }
    }

    private void validateMax(int max, Map<String, String> errorMap) {
        if (max < minMax) {
            errorMap.put("max", "tooShort");
            return;
        }
        if (max > maxMax) {
            errorMap.put("max", "tooLong");
        }
    }
}
