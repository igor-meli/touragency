package com.melentyev.travelagency.validator;

import com.melentyev.travelagency.entity.User;

import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class UserValidator implements Validator<User> {

    private final static String emailPattern = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
    private final static int minLoginName = 4;
    private final static int maxLoginName = 16;
    private final static int minPassName = 6;
    private final static int maxPassName = 32;
    private final static int minSecondName = 2;
    private final static int maxSecondName = 30;
    private final static int minFirstName = 2;
    private final static int maxFirstName = 30;

    @Override
    public Map<String, String> validate(User user) {
        Map<String, String> errorMap = new HashMap<>();
        validateLogin(user.getLogin(), errorMap);
        validatePassword(user.getPassword(), errorMap);
        validateEmail(user.getEmail(), errorMap);
        validateFirstName(user.getFirstName(), errorMap);
        validateSecondName(user.getSecondName(), errorMap);
        return errorMap;
    }

    private void validateLogin(String login, Map<String, String> errorMap) {
        if (login == null || login.trim().isEmpty()) {
            errorMap.put("login", "empty");
            return;
        }
        if (login.length() < minLoginName) {
            errorMap.put("login", "tooShort");
            return;
        }
        if (login.length() > maxLoginName) {
            errorMap.put("login", "tooLong");
        }
    }

    private void validatePassword(String password, Map<String, String> errorMap) {
        if (password == null || password.trim().isEmpty()) {
            errorMap.put("password", "empty");
            return;
        }
        if (password.length() < minPassName) {
            errorMap.put("password", "tooShort");
            return;
        }
        if (password.length() > maxPassName) {
            errorMap.put("password", "tooLong");
        }
    }

    private void validateEmail(final String email, Map<String, String> errorMap) {
        if (email==null || email.trim().isEmpty()){
            errorMap.put("email", "empty");
            return;
        }
        Pattern pattern = Pattern.compile(emailPattern);
        Matcher matcher = pattern.matcher(email);
        if (!matcher.matches()) {
            errorMap.put("email", "invalid");
        }
    }

    private void validateFirstName(String firstName, Map<String, String> errorMap) {
        if (firstName == null || firstName.trim().isEmpty()) {
            errorMap.put("firstName", "empty");
            return;
        }
        if (firstName.length() < minFirstName) {
            errorMap.put("firstName", "tooShort");
            return;
        }
        if (firstName.length() > maxFirstName) {
            errorMap.put("firstName", "tooLong");
        }
    }

    private void validateSecondName(String secondName, Map<String, String> errorMap) {
        if (secondName == null || secondName.trim().isEmpty()) {
            errorMap.put("secondName", "empty");
            return;
        }
        if (secondName.length() < minSecondName) {
            errorMap.put("secondName", "tooShort");
            return;
        }
        if (secondName.length() > maxSecondName) {
            errorMap.put("secondName", "tooLong");
        }
    }

}
