package com.melentyev.travelagency.controller.order;

import com.melentyev.travelagency.auth.Authenticator;
import com.melentyev.travelagency.entity.Order;
import com.melentyev.travelagency.entity.User;
import com.melentyev.travelagency.exception.NoSuchEntityException;
import com.melentyev.travelagency.service.OrderService;
import com.melentyev.travelagency.util.PaginatorUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

@WebServlet(urlPatterns = "/admin/orders", loadOnStartup = 6)
public class UserOrdersController extends HttpServlet {

    private static final Logger logger = LoggerFactory.getLogger(UserOrdersController.class);
    private static final int NUMBERS_OF_ROWS = 4;
    private OrderService orderService = new OrderService();

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        User user = Authenticator.getUserFromSession(request);
        try {
            String pageNumStr = request.getParameter("page");
            int page = 0;
            if (pageNumStr != null)
                page = Integer.parseInt(pageNumStr) - 1;

            PaginatorUtil.initPaginatorToRequest(page, orderService.getUserOrdersCount(user.getId()), NUMBERS_OF_ROWS, request);

            List<Order> orders = orderService.getAllUserService(user.getId(), page * NUMBERS_OF_ROWS, NUMBERS_OF_ROWS);
            if (orders.isEmpty()) {
                throw new NoSuchEntityException("Orders not found");
            }
            request.setAttribute("orders", orders);

            request.getRequestDispatcher("/admin/myOrders.jsp").forward(request, response);
            return;
        } catch (NoSuchEntityException у) {
            request.setAttribute("errorMsg", "notFoundOrders");
        } catch (NumberFormatException e) {
            request.setAttribute("errorMsg", "notFoundError");
        } catch (Exception e) {
            logger.warn("PAGE ERROR", e);
            request.setAttribute("errorMsg", "systemCrash");
        }
        request.getRequestDispatcher("/error.jsp").forward(request, response);
    }

}
