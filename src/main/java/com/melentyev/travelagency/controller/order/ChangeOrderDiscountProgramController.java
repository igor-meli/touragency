package com.melentyev.travelagency.controller.order;

import com.melentyev.travelagency.auth.Authenticator;
import com.melentyev.travelagency.entity.Order;
import com.melentyev.travelagency.entity.User;
import com.melentyev.travelagency.exception.NoSuchEntityException;
import com.melentyev.travelagency.service.OrderService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

import static com.melentyev.travelagency.auth.RightsAction.CHANGE_DISCOUNT;

@WebServlet(urlPatterns = "/admin/order/discountProgram", loadOnStartup = 13)
public class ChangeOrderDiscountProgramController extends HttpServlet {

    private static final Logger logger = LoggerFactory.getLogger(ChangeOrderDiscountProgramController.class);
    private OrderService orderService = new OrderService();

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        User user = Authenticator.getUserFromSession(request);
        try {
            if (user.hasRights(CHANGE_DISCOUNT)) {
                String orderIdStr = request.getParameter("orderId");
                String discountProgramIdStr = request.getParameter("discountProgramId");
                int orderId = Integer.parseInt(orderIdStr);
                int discountProgramId = Integer.parseInt(discountProgramIdStr);
                Order order = orderService.getOrderById(orderId);
                if (order == null) {
                    throw new NoSuchEntityException("Orders with id " + orderId + " not found");
                }
                orderService.updateOrderDiscountProgram(order, discountProgramId);
            }
        } catch (Exception e) {
            logger.warn("PAGE ERROR", e);
        }
    }
}
