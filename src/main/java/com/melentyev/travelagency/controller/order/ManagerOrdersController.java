package com.melentyev.travelagency.controller.order;

import com.melentyev.travelagency.auth.Authenticator;
import com.melentyev.travelagency.entity.Order;
import com.melentyev.travelagency.entity.User;
import com.melentyev.travelagency.exception.ForbiddenActionException;
import com.melentyev.travelagency.exception.NoSuchEntityException;
import com.melentyev.travelagency.service.DiscountProgramService;
import com.melentyev.travelagency.service.OrderService;
import com.melentyev.travelagency.service.StatusService;
import com.melentyev.travelagency.util.PaginatorUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

import static com.melentyev.travelagency.auth.RightsAction.CHANGE_STATUS;

@WebServlet(urlPatterns = "/admin/change/status", loadOnStartup = 13)
public class ManagerOrdersController extends HttpServlet {

    private static final Logger logger = LoggerFactory.getLogger(ManagerOrdersController.class);
    private static final int NUMBERS_OF_ROWS = 4;
    private OrderService orderService = new OrderService();
    private StatusService statusService = new StatusService();
    private DiscountProgramService discountProgramService = new DiscountProgramService();

    protected void doGet(final HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        User user = Authenticator.getUserFromSession(request);
        try {
            if (!user.hasRights(CHANGE_STATUS)) {
                logger.info("User with id = {} doesn't have access rights to this page", user.getId());
                throw new ForbiddenActionException("User with id = " + user.getId() + " doesn't have access rights to this page");
            }
            String pageNumStr = request.getParameter("page");
            int page = 0;
            if (pageNumStr != null)
                page = Integer.parseInt(pageNumStr) - 1;

            PaginatorUtil.initPaginatorToRequest(page, orderService.getOrdersCount(), NUMBERS_OF_ROWS, request);

            request.setAttribute("status", statusService.getAllStatus());
            request.setAttribute("discounts", discountProgramService.getAllDiscountPrograms());
            List<Order> orders = orderService.getAllOrders(page * NUMBERS_OF_ROWS, NUMBERS_OF_ROWS);
            if (orders.isEmpty()) {
                throw new NoSuchEntityException("Orders not found");
            }
            request.setAttribute("orders", orders);
            request.getRequestDispatcher("/admin/allOrders.jsp").forward(request, response);
            return;
        } catch (ForbiddenActionException e) {
            logger.warn("NO ACCESS", e);
            request.setAttribute("errorMsg", "forbidden");
        } catch (Exception e) {
            logger.warn("PAGE ERROR", e);
            request.setAttribute("errorMsg", "systemCrash");
        }
        request.getRequestDispatcher("/error.jsp").forward(request, response);
    }

    /**
     * Change orders status (Used by JQuery)
     *
     * @param request  - HttpServletRequest
     * @param response - HttpServletResponse
     * @throws ServletException
     * @throws IOException
     */
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        User user = Authenticator.getUserFromSession(request);
        try {
            if (user.hasRights(CHANGE_STATUS)) {
                String orderIdStr = request.getParameter("orderId");
                String changedStatusIdStr = request.getParameter("changedStatusId");

                int orderId = Integer.parseInt(orderIdStr);
                int changedStatusId = Integer.parseInt(changedStatusIdStr);
                orderService.changeStatus(orderId, changedStatusId, user);
            }
        } catch (Exception e) {
            logger.warn("PAGE ERROR", e);
        }
    }

}
