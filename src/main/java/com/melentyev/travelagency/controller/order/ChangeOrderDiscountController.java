package com.melentyev.travelagency.controller.order;

import com.melentyev.travelagency.auth.Authenticator;
import com.melentyev.travelagency.entity.Order;
import com.melentyev.travelagency.entity.User;
import com.melentyev.travelagency.service.OrderService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.Writer;

import static com.melentyev.travelagency.auth.RightsAction.CHANGE_DISCOUNT;

@WebServlet(urlPatterns = "/admin/order/discount", loadOnStartup = 12)
public class ChangeOrderDiscountController extends HttpServlet {

    private static final Logger logger = LoggerFactory.getLogger(ChangeOrderDiscountController.class);
    private OrderService orderService = new OrderService();

    /**
     * API for /admin/allOrders.jsp
     * Returns total price
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        User user = Authenticator.getUserFromSession(request);
        try {
            if (user.hasRights(CHANGE_DISCOUNT)) {
                String orderIdStr = request.getParameter("orderId");
                String discountStr = request.getParameter("discount");

                int orderId = Integer.parseInt(orderIdStr);
                int discount = Integer.parseInt(discountStr);
                Order order = orderService.getOrderById(orderId);
                orderService.updateOrderDiscount(order, discount);
                Writer writer = response.getWriter();
                writer.write(String.valueOf(order.getTotalPrice()));
            }
        } catch (Exception e) {
            logger.warn("PAGE ERROR", e);
        }

    }

}
