package com.melentyev.travelagency.controller.roomtype;

import com.melentyev.travelagency.auth.Authenticator;
import com.melentyev.travelagency.entity.User;
import com.melentyev.travelagency.exception.ForbiddenActionException;
import com.melentyev.travelagency.service.HotelService;
import com.melentyev.travelagency.service.RoomTypeService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

import static com.melentyev.travelagency.auth.RightsAction.ADD_TOUR;

@WebServlet(urlPatterns = "/admin/roomtypes",  loadOnStartup = 24)
public class RoomTypesController extends HttpServlet {

    private static final Logger logger = LoggerFactory.getLogger(RoomTypesController.class);
    private RoomTypeService roomTypeService = new RoomTypeService();
    private HotelService hotelService = new HotelService();

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        User user = Authenticator.getUserFromSession(request);
        try {
            if (!user.hasRights(ADD_TOUR)) {
                logger.info("User with id = {} doesn't have access rights to this page", user.getId());
                throw new ForbiddenActionException("User with id = " + user.getId() + " doesn't have access rights to this page");
            }
            request.setAttribute("roomtypes", roomTypeService.getAllRoomTypes());
            request.setAttribute("hotels", hotelService.getAllHotels());
            request.getRequestDispatcher("/admin/room/roomtype.jsp").forward(request, response);
            return;
        } catch (ForbiddenActionException e) {
            logger.warn("NO ACCESS", e);
            request.setAttribute("errorMsg", "forbidden");
        } catch (Exception e) {
            logger.warn("PAGE ERROR", e);
            request.setAttribute("errorMsg", "systemCrash");
        }
        request.getRequestDispatcher("/error.jsp").forward(request, response);
    }

}
