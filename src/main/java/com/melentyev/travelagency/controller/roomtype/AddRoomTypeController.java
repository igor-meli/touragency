package com.melentyev.travelagency.controller.roomtype;

import com.melentyev.travelagency.auth.Authenticator;
import com.melentyev.travelagency.entity.Hotel;
import com.melentyev.travelagency.entity.RoomType;
import com.melentyev.travelagency.entity.User;
import com.melentyev.travelagency.exception.ForbiddenActionException;
import com.melentyev.travelagency.service.HotelService;
import com.melentyev.travelagency.service.RoomTypeService;
import com.melentyev.travelagency.validator.RoomTypeValidator;
import com.melentyev.travelagency.validator.Validator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Map;

import static com.melentyev.travelagency.auth.RightsAction.ADD_TOUR;

@WebServlet(urlPatterns = "/admin/addRoomType", loadOnStartup = 22)
public class AddRoomTypeController extends HttpServlet {

    private static final Logger logger = LoggerFactory.getLogger(AddRoomTypeController.class);
    private RoomTypeService roomTypeService = new RoomTypeService();
    private HotelService hotelService = new HotelService();
    private Validator<RoomType> roomTypeValidator = new RoomTypeValidator();

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        User user = Authenticator.getUserFromSession(request);
        try {
            if (!user.hasRights(ADD_TOUR)) {
                logger.info("User with id = {} doesn't have access rights to this page", user.getId());
                throw new ForbiddenActionException("User with id = " + user.getId() + " doesn't have access rights to this page");
            }
            String roomTypeName = request.getParameter("roomtypeName");
            String maxAdultsStr = request.getParameter("maxAdults");
            String maxChildrenStr = request.getParameter("maxChildren");
            String hotelIdStr = request.getParameter("hotelId");
            int maxAdults = Integer.parseInt(maxAdultsStr);
            int maxChildren = Integer.parseInt(maxChildrenStr);
            int hotelId = Integer.parseInt(hotelIdStr);
            Hotel hotel = hotelService.getHotelById(hotelId);

            RoomType roomType = new RoomType(roomTypeName, maxChildren, maxAdults, hotel);


            Map<String, String> errors = roomTypeValidator.validate(roomType);

            if (roomTypeService.isRoomTypeExists(roomType)) {
                errors.put("roomtypeName", "roomtypeExists");
            }
            if (!errors.isEmpty()) {
                request.setAttribute("flashScope.roomtypeError", errors);
                saveEnteredDataToRequest(request, roomType);
                response.sendRedirect("/admin/roomtypes");
                return;
            }
            roomTypeService.addRoomType(roomType);
            response.sendRedirect("/admin/roomtypes");
            return;
        } catch (ForbiddenActionException e) {
            logger.warn("NO ACCESS", e);
            request.setAttribute("errorMsg", "forbidden");
        } catch (Exception e) {
            logger.warn("PAGE ERROR", e);
            request.setAttribute("errorMsg", "systemCrash");
        }
        request.getRequestDispatcher("/error.jsp").forward(request, response);
    }

    private void saveEnteredDataToRequest(HttpServletRequest request, RoomType roomType) {
        request.setAttribute("flashScope.roomtypeName", roomType.getName());
        request.setAttribute("flashScope.maxAdults", roomType.getMaxAdults());
        request.setAttribute("flashScope.maxChildren", roomType.getMaxChildren());
        request.setAttribute("flashScope.hotelId", roomType.getHotel().getId());
    }

}
