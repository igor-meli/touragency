package com.melentyev.travelagency.controller.roomtype;

import com.melentyev.travelagency.auth.Authenticator;
import com.melentyev.travelagency.entity.RoomType;
import com.melentyev.travelagency.entity.User;
import com.melentyev.travelagency.service.RoomTypeService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.Writer;
import java.util.List;

import static com.melentyev.travelagency.auth.RightsAction.ADD_TOUR;

/**
 * API class for addOffer.jsp (Get Hotel Rooms)
 * Response Example (<option value="${roomType.id}">${roomType.name}</option>):
 *  <option value="1">Standard Room</option>
 *  <option value="6">Double Room</option>
 * Parameters: hotelId
 */
@WebServlet(urlPatterns = "/api/hotel/rooms", loadOnStartup = 5)
public class AllHotelRoomsController extends HttpServlet {

    private static final Logger logger = LoggerFactory.getLogger(AllHotelRoomsController.class);
    private RoomTypeService roomTypeService = new RoomTypeService();

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        User user = Authenticator.getUserFromSession(request);
        if (user.hasRights(ADD_TOUR)) {
            response.setCharacterEncoding("UTF-8");
            String hotelIdStr = request.getParameter("hotelId");
            try (Writer writer = response.getWriter()) {
                int hotelId = Integer.parseInt(hotelIdStr);
                List<RoomType> roomTypes = roomTypeService.getRoomTypeByHotelId(hotelId);
                String roomTypeOptions = convertToSelectedOption(roomTypes);
                writer.write(roomTypeOptions);
            } catch (Exception e) {
                logger.warn("PAGE ERROR", e);
            }
        }
    }

    /**
     * Convert RoomTypes to html options <option value="${roomType.id}">${roomType.name}</option>.
     *
     * @param roomTypes - RoomTypes
     * @return - html options (<option value="${roomType.id}">${roomType.name}</option>)
     */
    private String convertToSelectedOption(List<RoomType> roomTypes) {
        StringBuilder options = new StringBuilder();
        for (RoomType roomType : roomTypes) {
            options.append("<option value=\"")
                    .append(roomType.getId())
                    .append("\">")
                    .append(roomType.getName())
                    .append("</option>");
        }
        return options.toString();
    }

}
