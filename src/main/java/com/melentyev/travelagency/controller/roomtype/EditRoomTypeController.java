package com.melentyev.travelagency.controller.roomtype;

import com.melentyev.travelagency.auth.Authenticator;
import com.melentyev.travelagency.entity.RoomType;
import com.melentyev.travelagency.entity.User;
import com.melentyev.travelagency.exception.ForbiddenActionException;
import com.melentyev.travelagency.exception.NoSuchEntityException;
import com.melentyev.travelagency.service.HotelService;
import com.melentyev.travelagency.service.RoomTypeService;
import com.melentyev.travelagency.validator.RoomTypeValidator;
import com.melentyev.travelagency.validator.Validator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import static com.melentyev.travelagency.auth.RightsAction.ADD_TOUR;
import static com.melentyev.travelagency.auth.RightsAction.CHANGE_DISCOUNT;

@WebServlet(urlPatterns = "/admin/editRoomType", loadOnStartup = 25)
public class EditRoomTypeController extends HttpServlet {

    private static final Logger logger = LoggerFactory.getLogger(EditRoomTypeController.class);
    private RoomTypeService roomTypeService = new RoomTypeService();
    private HotelService hotelService = new HotelService();
    private Validator<RoomType> roomTypeValidator = new RoomTypeValidator();

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        User user = Authenticator.getUserFromSession(request);
        try {
            if (!user.hasRights(CHANGE_DISCOUNT)) {
                logger.info("User with id = {} doesn't have access rights to this page", user.getId());
                throw new ForbiddenActionException("User with id = " + user.getId() + " doesn't have access rights to this page");
            }
            String idStr = request.getParameter("id");
            int id = Integer.parseInt(idStr);
            RoomType roomType = roomTypeService.getRoomTypeById(id);
            if (roomType == null) {
                throw new NoSuchEntityException("RoomType not found");
            }

            saveEnteredDataToRequestIfAbsent(request, roomType);
            request.setAttribute("hotels", hotelService.getAllHotels());
            request.getRequestDispatcher("/admin/room/editRoomType.jsp").forward(request, response);
            return;
        } catch (NoSuchEntityException e) {
            request.setAttribute("errorMsg", "notFoundError");
        } catch (ForbiddenActionException e) {
            request.setAttribute("errorMsg", "forbidden");
        } catch (Exception e) {
            request.setAttribute("errorMsg", "systemCrash");
            logger.info("Error Occurred " + e);
        }
        request.getRequestDispatcher("/error.jsp").forward(request, response);
    }

    private void saveEnteredDataToRequestIfAbsent(HttpServletRequest request, RoomType roomType) {
        if (request.getAttribute("editInfo") == null) {
            saveDataToRequest(request, roomType, "");
        }
    }

    private void saveDataToRequest(HttpServletRequest request, RoomType roomType, String prefix) {
        Map<String, Object> attributes = new HashMap<>();
        attributes.put("roomtypeName", roomType.getName());
        attributes.put("maxAdults", roomType.getMaxAdults());
        attributes.put("maxChildren", roomType.getMaxChildren());
        attributes.put("hotelId", roomType.getHotel().getId());
        attributes.put("roomTypeId", roomType.getId());
        request.setAttribute(prefix + "editInfo", attributes);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        User user = Authenticator.getUserFromSession(request);
        try {
            if (!user.hasRights(ADD_TOUR)) {
                logger.info("User with id = {} doesn't have access rights to this page", user.getId());
                throw new ForbiddenActionException("User with id = " + user.getId() + " doesn't have access rights to this page");
            }
            String roomTypeName = request.getParameter("roomtypeName");
            String maxAdultsStr = request.getParameter("maxAdults");
            String maxChildrenStr = request.getParameter("maxChildren");

            String roomTypeIdStr = request.getParameter("id");

            int maxAdults = Integer.parseInt(maxAdultsStr);
            int maxChildren = Integer.parseInt(maxChildrenStr);
            int roomTypeId = Integer.parseInt(roomTypeIdStr);

            RoomType roomType = roomTypeService.getRoomTypeById(roomTypeId);
            roomType.setName(roomTypeName);
            roomType.setMaxAdults(maxAdults);
            roomType.setMaxChildren(maxChildren);


            Map<String, String> errors = roomTypeValidator.validate(roomType);
            if (roomTypeService.isRoomTypeExists(roomType)) {
                errors.put("roomtypeName", "roomtypeExists");
            }
            if (!errors.isEmpty()) {
                request.setAttribute("flashScope.roomtypeError", errors);
                saveEnteredDataToFlashRequest(request, roomType);
                response.sendRedirect("/admin/editRoomType?id=" + roomTypeId);
                return;
            }
            roomTypeService.updateRoomType(roomType);
            response.sendRedirect("/admin/roomtypes");
            return;
        } catch (ForbiddenActionException e) {
            logger.warn("NO ACCESS", e);
            request.setAttribute("errorMsg", "forbidden");
        } catch (Exception e) {
            logger.warn("PAGE ERROR", e);
            request.setAttribute("errorMsg", "systemCrash");
        }
        request.getRequestDispatcher("/error.jsp").forward(request, response);
    }

    private void saveEnteredDataToFlashRequest(HttpServletRequest request, RoomType roomType) {
        saveDataToRequest(request, roomType, "flashScope.");
    }
}
