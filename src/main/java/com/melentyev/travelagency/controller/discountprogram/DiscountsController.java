package com.melentyev.travelagency.controller.discountprogram;

import com.melentyev.travelagency.auth.Authenticator;
import com.melentyev.travelagency.entity.User;
import com.melentyev.travelagency.exception.ForbiddenActionException;
import com.melentyev.travelagency.service.DiscountProgramService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

import static com.melentyev.travelagency.auth.RightsAction.CHANGE_DISCOUNT;

@WebServlet(urlPatterns = "/admin/discounts", loadOnStartup = 19)
public class DiscountsController extends HttpServlet {

    private static final Logger logger = LoggerFactory.getLogger(DiscountsController.class);
    private DiscountProgramService dpService = new DiscountProgramService();

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        User user = Authenticator.getUserFromSession(request);
        try {
            if (!user.hasRights(CHANGE_DISCOUNT)) {
                logger.info("User with id = {} doesn't have access rights to this page", user.getId());
                throw new ForbiddenActionException("User with id = " + user.getId() + " doesn't have access rights to this page");
            }
            request.setAttribute("discounts", dpService.getAllDiscountPrograms());
            request.getRequestDispatcher("/admin/discount/discounts.jsp").forward(request, response);
            return;
        } catch (ForbiddenActionException e) {
            logger.warn("NO ACCESS", e);
            request.setAttribute("errorMsg", "forbidden");
        } catch (Exception e) {
            logger.warn("PAGE ERROR", e);
            request.setAttribute("errorMsg", "systemCrash");
        }
        request.getRequestDispatcher("/error.jsp").forward(request, response);
    }

}
