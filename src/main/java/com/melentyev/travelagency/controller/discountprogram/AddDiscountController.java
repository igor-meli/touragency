package com.melentyev.travelagency.controller.discountprogram;

import com.melentyev.travelagency.auth.Authenticator;
import com.melentyev.travelagency.entity.DiscountProgram;
import com.melentyev.travelagency.entity.User;
import com.melentyev.travelagency.exception.ForbiddenActionException;
import com.melentyev.travelagency.service.DiscountProgramService;
import com.melentyev.travelagency.validator.DiscountValidator;
import com.melentyev.travelagency.validator.Validator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Map;

import static com.melentyev.travelagency.auth.RightsAction.CHANGE_DISCOUNT;

@WebServlet(urlPatterns = "/admin/addDiscount", loadOnStartup = 17)
public class AddDiscountController extends HttpServlet {

    private static final Logger logger = LoggerFactory.getLogger(AddDiscountController.class);
    private DiscountProgramService dpService = new DiscountProgramService();
    private Validator<DiscountProgram> discountValidator = new DiscountValidator();

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        User user = Authenticator.getUserFromSession(request);
        try {
            if (!user.hasRights(CHANGE_DISCOUNT)) {
                logger.info("User with id = {} doesn't have access rights to this page", user.getId());
                throw new ForbiddenActionException("User with id = " + user.getId() + " doesn't have access rights to this page");
            }
            String discountName = request.getParameter("discountName");
            String stepStr = request.getParameter("step");
            String maxStr = request.getParameter("max");
            int step = Integer.parseInt(stepStr);
            int max = Integer.parseInt(maxStr);

            DiscountProgram discountProgram = new DiscountProgram(discountName, step, max);
            Map<String, String> errors = discountValidator.validate(discountProgram);

            if (dpService.isDiscountExists(discountProgram)) {
                errors.put("discountName", "discountExists");
            }
            if (!errors.isEmpty()) {
                request.setAttribute("flashScope.discountError", errors);
                saveEnteredDataToRequest(request, discountProgram);
                response.sendRedirect("/admin/discounts");
                return;
            }
            dpService.addDiscountProgram(discountProgram);
            logger.info("Discount Program added ({} step:{} max:{}) User: {}", discountProgram.getName(),
                    discountProgram.getStep(),  discountProgram.getMax(), user.getLogin());
            response.sendRedirect("/admin/discounts");
            return;
        } catch (ForbiddenActionException e) {
            logger.warn("NO ACCESS", e);
            request.setAttribute("errorMsg", "forbidden");
        } catch (Exception e) {
            logger.warn("PAGE ERROR", e);
            request.setAttribute("errorMsg", "systemCrash");
        }
        request.getRequestDispatcher("/error.jsp").forward(request, response);
    }

    private void saveEnteredDataToRequest(HttpServletRequest request, DiscountProgram discountProgram) {
        request.setAttribute("flashScope.discountName", discountProgram.getName());
        request.setAttribute("flashScope.step", discountProgram.getStep());
        request.setAttribute("flashScope.max", discountProgram.getMax());
    }

}
