package com.melentyev.travelagency.controller.discountprogram;

import com.melentyev.travelagency.auth.Authenticator;
import com.melentyev.travelagency.entity.User;
import com.melentyev.travelagency.exception.ForbiddenActionException;
import com.melentyev.travelagency.service.DiscountProgramService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

import static com.melentyev.travelagency.auth.RightsAction.CHANGE_DISCOUNT;

@WebServlet(urlPatterns = "/admin/deleteDiscount", loadOnStartup = 20)
public class DeleteDiscountController extends HttpServlet {

    private static final Logger logger = LoggerFactory.getLogger(DeleteDiscountController.class);
    private DiscountProgramService dpService = new DiscountProgramService();

    @Override
    protected void doDelete(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        User user = Authenticator.getUserFromSession(request);
        try {
            if (!user.hasRights(CHANGE_DISCOUNT)) {
                logger.info("User with id = {} doesn't have access rights to this page", user.getId());
                throw new ForbiddenActionException("User with id = " + user.getId() + " doesn't have access rights to this page");
            }
            String discountIdStr = request.getParameter("discountId");
            int discountId = Integer.parseInt(discountIdStr);
            dpService.deleteById(discountId);
            logger.info("Discount Program {} was deleted by user #{} login: {}", discountId, user.getId(), user.getLogin());
        } catch (ForbiddenActionException e) {
            logger.warn("NO ACCESS", e);
        } catch (Exception e) {
            logger.warn("PAGE ERROR", e);
        }
    }

}
