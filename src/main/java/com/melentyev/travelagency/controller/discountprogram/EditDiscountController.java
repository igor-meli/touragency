package com.melentyev.travelagency.controller.discountprogram;

import com.melentyev.travelagency.auth.Authenticator;
import com.melentyev.travelagency.entity.DiscountProgram;
import com.melentyev.travelagency.entity.User;
import com.melentyev.travelagency.exception.ForbiddenActionException;
import com.melentyev.travelagency.exception.NoSuchEntityException;
import com.melentyev.travelagency.service.DiscountProgramService;
import com.melentyev.travelagency.validator.DiscountValidator;
import com.melentyev.travelagency.validator.Validator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import static com.melentyev.travelagency.auth.RightsAction.CHANGE_DISCOUNT;

@WebServlet(urlPatterns = "/admin/editDiscount", loadOnStartup = 18)
public class EditDiscountController extends HttpServlet {

    private static final Logger logger = LoggerFactory.getLogger(EditDiscountController.class);
    private DiscountProgramService dpService = new DiscountProgramService();
    private Validator<DiscountProgram> discountValidator = new DiscountValidator();

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        User user = Authenticator.getUserFromSession(request);
        try {
            if (!user.hasRights(CHANGE_DISCOUNT)) {
                logger.info("User with id = {} doesn't have access rights to this page", user.getId());
                throw new ForbiddenActionException("User with id = " + user.getId() + " doesn't have access rights to this page");
            }
            String idStr = request.getParameter("id");
            int id = Integer.parseInt(idStr);
            DiscountProgram program = dpService.getDiscountById(id);
            if (program == null) {
                throw new NoSuchEntityException("Discount Programs not found");
            }
            saveEnteredDataToRequestIfAbsent(request, program);
            request.getRequestDispatcher("/admin/discount/editDiscount.jsp").forward(request, response);
            return;
        } catch (NoSuchEntityException e) {
            logger.warn("NOT FOUND", e);
            request.setAttribute("errorMsg", "notFoundError");
        } catch (ForbiddenActionException e) {
            logger.warn("NO ACCESS", e);
            request.setAttribute("errorMsg", "forbidden");
        } catch (Exception e) {
            logger.warn("PAGE ERROR", e);
            request.setAttribute("errorMsg", "systemCrash");
        }
        request.getRequestDispatcher("/error.jsp").forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        User user = Authenticator.getUserFromSession(request);
        try {
            if (!user.hasRights(CHANGE_DISCOUNT)) {
                logger.info("User with id = {} doesn't have access rights to this page", user.getId());
                throw new ForbiddenActionException("User with id = " + user.getId() + " doesn't have access rights to this page");
            }
            String discountName = request.getParameter("discountName");
            String stepStr = request.getParameter("step");
            String maxStr = request.getParameter("max");
            String discountIdStr = request.getParameter("id");
            int step = Integer.parseInt(stepStr);
            int max = Integer.parseInt(maxStr);
            int discountId = Integer.parseInt(discountIdStr);

            DiscountProgram discountProgram = dpService.getDiscountById(discountId);
            discountProgram.setName(discountName);
            discountProgram.setMax(max);
            discountProgram.setStep(step);

            Map<String, String> errors = discountValidator.validate(discountProgram);

            if (dpService.isDiscountExists(discountProgram)) {
                errors.put("discountName", "discountExists");
            }
            if (!errors.isEmpty()) {
                request.setAttribute("flashScope.discountError", errors);
                saveEnteredDataToFlashRequest(request, discountProgram);
                response.sendRedirect("/admin/editDiscount?id=" + discountId);
                return;
            }
            dpService.updateDiscountProgram(discountProgram);
            logger.info("Discount Program #{} was updated ({} step:{} max:{}) by user #{}, login {}", discountId,
                    discountProgram.getName(), discountProgram.getStep(), discountProgram.getMax(), user.getId(),
                    user.getLogin());
            response.sendRedirect("/admin/discounts");
            return;
        } catch (ForbiddenActionException e) {
            logger.warn("NO ACCESS", e);
            request.setAttribute("errorMsg", "forbidden");
        } catch (Exception e) {
            logger.warn("PAGE ERROR", e);
            request.setAttribute("errorMsg", "systemCrash");
        }
        request.getRequestDispatcher("/error.jsp").forward(request, response);
    }

    private void saveEnteredDataToFlashRequest(HttpServletRequest request, DiscountProgram program) {
        saveDataToRequest(request, program, "flashScope.");
    }

    private void saveEnteredDataToRequestIfAbsent(HttpServletRequest request, DiscountProgram program) {
        if (request.getAttribute("editInfo") == null) {
            saveDataToRequest(request, program, "");
        }
    }

    private void saveDataToRequest(HttpServletRequest request, DiscountProgram program, String prefix) {
        Map<String, Object> attributes = new HashMap<>();
        attributes.put("discountName", program.getName());
        attributes.put("step", program.getStep());
        attributes.put("max", program.getMax());
        attributes.put("discountId", program.getId());
        request.setAttribute(prefix + "editInfo", attributes);
    }

}
