package com.melentyev.travelagency.controller.tour;

import com.melentyev.travelagency.entity.Tour;
import com.melentyev.travelagency.exception.NoSuchEntityException;
import com.melentyev.travelagency.service.TourService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet(urlPatterns = "/tour", loadOnStartup = 26)
public class TourByIdController extends HttpServlet {

    private static final Logger logger = LoggerFactory.getLogger(TourByIdController.class);
    private TourService tourService = new TourService();

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        try {
            String idStr = request.getParameter("id");
            int id = Integer.parseInt(idStr);
            Tour tour = tourService.getTourById(id);
            if (tour == null)
                throw new NoSuchEntityException("Tour with id = " + id + " not found");

            request.setAttribute("tour", tour);
            request.getRequestDispatcher("/tour.jsp").forward(request, response);
            return;
        } catch (NoSuchEntityException | NumberFormatException e) {
            logger.warn("NOT FOUND", e);
            request.setAttribute("errorMsg", "notFoundError");
        } catch (Exception e) {
            logger.warn("PAGE ERROR", e);
            request.setAttribute("errorMsg", "systemCrash");
        }
        request.getRequestDispatcher("/error.jsp").forward(request, response);
    }
}
