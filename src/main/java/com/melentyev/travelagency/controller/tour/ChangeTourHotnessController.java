package com.melentyev.travelagency.controller.tour;

import com.melentyev.travelagency.auth.Authenticator;
import com.melentyev.travelagency.entity.User;
import com.melentyev.travelagency.service.TourService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

import static com.melentyev.travelagency.auth.RightsAction.CHANGE_HOTNESS;

/**
 * Change tour hotness (Used by JQuery)
 */
@WebServlet(urlPatterns = "/admin/change/hotness", loadOnStartup = 8)
public class ChangeTourHotnessController extends HttpServlet {

    private static final Logger logger = LoggerFactory.getLogger(ChangeTourHotnessController.class);
    private TourService tourService = new TourService();

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        User user = Authenticator.getUserFromSession(request);
        if (user.hasRights(CHANGE_HOTNESS)) {
            String idStr = request.getParameter("id");
            try {
                int id = Integer.parseInt(idStr);
                tourService.changeTourHotness(id);
            } catch (Exception e) {
                logger.warn("PAGE ERROR", e);
            }
        }
    }

}
