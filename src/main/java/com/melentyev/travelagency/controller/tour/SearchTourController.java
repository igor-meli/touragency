package com.melentyev.travelagency.controller.tour;

import com.melentyev.travelagency.entity.Tour;
import com.melentyev.travelagency.exception.NoSuchEntityException;
import com.melentyev.travelagency.service.TourService;
import com.melentyev.travelagency.util.SelectBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

@WebServlet(urlPatterns = "/search/tours", loadOnStartup = 27)
public class SearchTourController extends HttpServlet {

    private static final Logger logger = LoggerFactory.getLogger(SearchTourController.class);
    private TourService tourService = new TourService();
    private SelectBuilder selectBuilder;

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        selectBuilder = new SelectBuilder();
        initSelectBuilder();
        try {
            String tourTypeStr = request.getParameter("tourType");
            String priceStr = request.getParameter("price");
            String adultsStr = request.getParameter("adults");
            String childrenStr = request.getParameter("children");
            String hotelTypeStr = request.getParameter("hotelType");

            List<Integer> params = new ArrayList<>();
            if (isValidParameter(tourTypeStr)) {
                int tourType = Integer.parseInt(tourTypeStr);
                params.add(tourType);
                request.setAttribute("searchedTourType", tourType);
                selectBuilder.where("Tour.type_id = ?");
            }
            if (isValidParameter(priceStr)) {
                int price = Integer.parseInt(priceStr);
                params.add(price);
                selectBuilder.where("Offer.price >= ?");
                request.setAttribute("searchedPrice", price);
            }
            if (isValidParameter(adultsStr)) {
                int adults = Integer.parseInt(adultsStr);
                params.add(adults);
                selectBuilder.where("RoomType.max_adults >= ?");
                request.setAttribute("searchedAdults", adults);
            }
            if (isValidParameter(childrenStr)) {
                int children = Integer.parseInt(childrenStr);
                params.add(children);
                selectBuilder.where("RoomType.max_children >= ?");
                request.setAttribute("searchedChildren", children);
            }
            if (isValidParameter(hotelTypeStr)) {
                int hotelType = Integer.parseInt(hotelTypeStr);
                params.add(hotelType);
                selectBuilder.where("Hotel.hotel_type_id = ?");
                request.setAttribute("searchedHotelType", hotelType);
            }
            List<Tour> searchedTours = tourService.searchTours(selectBuilder.toString(), params);
            if (searchedTours.isEmpty())
                throw new NoSuchEntityException("Tours not found");
            request.setAttribute("tours", searchedTours);
            request.getRequestDispatcher("/allTours.jsp").forward(request, response);
            return;
        } catch (NoSuchEntityException | NumberFormatException e) {
            logger.warn("NOT FOUND", e);
            request.setAttribute("errorMsg", "notFoundError");
        } catch (Exception e) {
            logger.warn("PAGE ERROR", e);
            request.setAttribute("errorMsg", "systemCrash");
        }
        request.getRequestDispatcher("/error.jsp").forward(request, response);
    }

    private void initSelectBuilder() {
        selectBuilder.column("Tour.id").column("Tour.name")
                .column("Concat(substring(Tour.description,1,200),'...') as description")
                .column("Tour.isHot").column("Tour.type_id").column("TourType.name as type_name")
                .column("Min(Offer.price) as minPrice")
                .from("Tour")
                .join("TourType ON Tour.type_id = TourType.id")
                .join("Offer ON Tour.id = Offer.tour_id")
                .join("Hotel ON Offer.hotel_id = Hotel.id")
                .join("RoomType ON Offer.room_type_id = RoomType.id")
                .groupBy("Tour.id");
    }

    private boolean isValidParameter(String parameter) {
        return parameter != null && !parameter.isEmpty();
    }

    private void smth(String paramStr, String whereExpr, List<Integer> params) {
        int param = Integer.parseInt(paramStr);
        params.add(param);
        selectBuilder.where(whereExpr);
    }
}
