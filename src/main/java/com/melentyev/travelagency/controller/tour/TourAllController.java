package com.melentyev.travelagency.controller.tour;

import com.melentyev.travelagency.entity.Tour;
import com.melentyev.travelagency.exception.NoSuchEntityException;
import com.melentyev.travelagency.service.TourService;
import com.melentyev.travelagency.util.PaginatorUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

@WebServlet(urlPatterns = "/tours", loadOnStartup = 29)
public class TourAllController extends HttpServlet {

    private static final Logger logger = LoggerFactory.getLogger(TourAllController.class);
    private static final int NUMBERS_OF_ROWS = 4;
    private TourService tourService = new TourService();

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        try {
            String pageNumStr = request.getParameter("page");
            int page = 0;
            if (pageNumStr != null)
                page = Integer.parseInt(pageNumStr) - 1;

            PaginatorUtil.initPaginatorToRequest(page, tourService.getRowsCount(), NUMBERS_OF_ROWS, request);
            List<Tour> tours = tourService.getAllToursLimit(page * NUMBERS_OF_ROWS, NUMBERS_OF_ROWS);
            request.setAttribute("tours", tours);
            if (tours.isEmpty())
                throw new NoSuchEntityException("Tours not found");

            request.getRequestDispatcher("/allTours.jsp").forward(request, response);
            return;
        } catch (NoSuchEntityException | NumberFormatException e) {
            logger.warn("NOT FOUND", e);
            request.setAttribute("errorMsg", "notFoundError");
        } catch (Exception e) {
            logger.warn("PAGE ERROR", e);
            request.setAttribute("errorMsg", "systemCrash");
        }
        request.getRequestDispatcher("/error.jsp").forward(request, response);
    }
}
