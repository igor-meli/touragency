package com.melentyev.travelagency.controller.tour;

import com.melentyev.travelagency.auth.Authenticator;
import com.melentyev.travelagency.entity.User;
import com.melentyev.travelagency.exception.ForbiddenActionException;
import com.melentyev.travelagency.service.TourService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

import static com.melentyev.travelagency.auth.RightsAction.ADD_TOUR;

@WebServlet(urlPatterns = "/admin/deleteTour", loadOnStartup = 30)
public class DeleteTourController extends HttpServlet {

    private static final Logger logger = LoggerFactory.getLogger(DeleteTourController.class);
    private TourService tourService = new TourService();

    @Override
    protected void doDelete(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        User user = Authenticator.getUserFromSession(request);
        try {
            if (!user.hasRights(ADD_TOUR)) {
                logger.info("User with id = {} doesn't have access rights to this page", user.getId());
                throw new ForbiddenActionException("User with id = " + user.getId() + " doesn't have access rights to this page");
            }
            String tourIdStr = request.getParameter("tourId");
            int tourId = Integer.parseInt(tourIdStr);
            tourService.deleteById(tourId);
        } catch (ForbiddenActionException e) {
            logger.warn("NO ACCESS", e);
        } catch (Exception e) {
            logger.warn("PAGE ERROR", e);
        }
    }

}
