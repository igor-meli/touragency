package com.melentyev.travelagency.controller.tour;

import com.melentyev.travelagency.auth.Authenticator;
import com.melentyev.travelagency.entity.Tour;
import com.melentyev.travelagency.entity.TourType;
import com.melentyev.travelagency.entity.User;
import com.melentyev.travelagency.exception.DaoSystemException;
import com.melentyev.travelagency.exception.ForbiddenActionException;
import com.melentyev.travelagency.exception.NoSuchEntityException;
import com.melentyev.travelagency.service.TourService;
import com.melentyev.travelagency.service.TourTypeService;
import com.melentyev.travelagency.validator.TourValidator;
import com.melentyev.travelagency.validator.Validator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import static com.melentyev.travelagency.auth.RightsAction.ADD_TOUR;
import static com.melentyev.travelagency.auth.RightsAction.CHANGE_DISCOUNT;

@WebServlet(urlPatterns = "/admin/editTour", loadOnStartup = 33)
public class EditTourController extends HttpServlet {

    private static final Logger logger = LoggerFactory.getLogger(EditTourController.class);
    private TourService tourService = new TourService();
    private TourTypeService tourTypeService = new TourTypeService();
    private Validator<Tour> tourValidator = new TourValidator();

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        User user = Authenticator.getUserFromSession(request);
        try {
            if (!user.hasRights(CHANGE_DISCOUNT)) {
                logger.info("User with id = {} doesn't have access rights to this page", user.getId());
                throw new ForbiddenActionException("User with id = " + user.getId() + " doesn't have access rights to this page");
            }
            String idStr = request.getParameter("id");
            int id = Integer.parseInt(idStr);
            Tour tour = tourService.getTourById(id);

            if (tour == null) {
                throw new NoSuchEntityException("Tour not found");
            }
            saveEnteredDataToRequestIfAbsent(request, tour);
            request.getRequestDispatcher("/admin/tour/editTour.jsp").forward(request, response);
            return;
        } catch (NoSuchEntityException e) {
            logger.warn("NOT FOUND", e);
            request.setAttribute("errorMsg", "notFoundError");
        } catch (ForbiddenActionException e) {
            logger.warn("NO ACCESS", e);
            request.setAttribute("errorMsg", "forbidden");
        } catch (Exception e) {
            logger.warn("PAGE ERROR", e);
            request.setAttribute("errorMsg", "systemCrash");
        }
        request.getRequestDispatcher("/error.jsp").forward(request, response);
    }

    private void saveEnteredDataToRequestIfAbsent(HttpServletRequest request, Tour tour) throws DaoSystemException {
        if (request.getAttribute("editInfo") == null) {
            saveDataToRequest(request, tour, "");
        }
    }

    private void saveDataToRequest(HttpServletRequest request, Tour tour, String prefix) throws DaoSystemException {
        Map<String, Object> attributes = new HashMap<>();
        attributes.put("tourName", tour.getName());
        attributes.put("description", tour.getDescription());
        attributes.put("selectedTourType", tour.getType());
        attributes.put("tourId", tour.getId());
        attributes.put("tourTypes", tourTypeService.getAllTourTypes());
        request.setAttribute(prefix + "editInfo", attributes);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        User user = Authenticator.getUserFromSession(request);
        try {
            if (!user.hasRights(ADD_TOUR)) {
                logger.info("User with id = {} doesn't have access rights to this page", user.getId());
                throw new ForbiddenActionException("User with id = " + user.getId() + " doesn't have access rights to this page");
            }
            String tourName = request.getParameter("tourName");
            String description = request.getParameter("description");
            String tourTypeIdStr = request.getParameter("tourTypeId");
            String tourIdStr = request.getParameter("tourId");
            int tourTypeId = Integer.parseInt(tourTypeIdStr);
            int tourId = Integer.parseInt(tourIdStr);

            TourType tourType = tourTypeService.getTourTypeById(tourTypeId);
            Tour tour = tourService.getTourById(tourId);
            tour.setName(tourName);
            tour.setDescription(description);
            tour.setType(tourType);

            Map<String, String> errors = tourValidator.validate(tour);
            if (tourService.isTourExists(tour)) {
                errors.put("tourName", "tourExists");
            }
            if (!errors.isEmpty()) {
                request.setAttribute("flashScope.tourError", errors);
                saveEnteredDataToFlashRequest(request, tour);
                response.sendRedirect("/admin/editTour?id=" + tourId);
                return;
            }
            tourService.update(tour);
            response.sendRedirect("/admin/tours");
            return;
        } catch (ForbiddenActionException e) {
            request.setAttribute("errorMsg", "forbidden");
        } catch (Exception e) {
            request.setAttribute("errorMsg", "systemCrash");
            logger.info("Error Occurred " + e);
        }
        request.getRequestDispatcher("/error.jsp").forward(request, response);
    }

    private void saveEnteredDataToFlashRequest(HttpServletRequest request, Tour tour) throws DaoSystemException {
        saveDataToRequest(request, tour, "flashScope.");
    }
}
