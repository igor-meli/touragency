package com.melentyev.travelagency.controller.tour;

import com.melentyev.travelagency.auth.Authenticator;
import com.melentyev.travelagency.entity.Tour;
import com.melentyev.travelagency.entity.TourType;
import com.melentyev.travelagency.entity.User;
import com.melentyev.travelagency.exception.DaoSystemException;
import com.melentyev.travelagency.exception.ForbiddenActionException;
import com.melentyev.travelagency.service.TourService;
import com.melentyev.travelagency.service.TourTypeService;
import com.melentyev.travelagency.validator.TourValidator;
import com.melentyev.travelagency.validator.Validator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Map;

import static com.melentyev.travelagency.auth.RightsAction.ADD_TOUR;

@WebServlet(urlPatterns = "/admin/addTour", loadOnStartup = 31)
public class AddTourController extends HttpServlet {

    private static final Logger logger = LoggerFactory.getLogger(AddTourController.class);
    private TourService tourService = new TourService();
    private TourTypeService tourTypeService = new TourTypeService();
    private Validator<Tour> tourValidator = new TourValidator();

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        User user = Authenticator.getUserFromSession(request);
        try {
            if (!user.hasRights(ADD_TOUR)) {
                logger.info("User with id = {} doesn't have access rights to this page", user.getId());
                throw new ForbiddenActionException("User with id = " + user.getId() + " doesn't have access rights to this page");
            }
            String tourName = request.getParameter("tourName");
            String description = request.getParameter("description");
            String tourTypeIdStr = request.getParameter("tourTypeId");
            int tourTypeId = Integer.parseInt(tourTypeIdStr);

            TourType tourType = tourTypeService.getTourTypeById(tourTypeId);
            Tour tour = new Tour(tourName, description, tourType);

            Map<String, String> errors = tourValidator.validate(tour);
            if (tourService.isTourExists(tour)) {
                errors.put("tourName", "tourExists");
            }
            if (!errors.isEmpty()) {
                request.setAttribute("flashScope.tourError", errors);
                saveEnteredDataToRequest(request, tour);
                response.sendRedirect("/admin/tours");
                return;
            }
            tourService.addTour(tour);
            response.sendRedirect("/admin/tours");
            return;
        } catch (ForbiddenActionException e) {
            logger.warn("NO ACCESS", e);
            request.setAttribute("errorMsg", "forbidden");
        } catch (Exception e) {
            logger.warn("PAGE ERROR", e);
            request.setAttribute("errorMsg", "systemCrash");
        }
        request.getRequestDispatcher("/error.jsp").forward(request, response);
    }

    private void saveEnteredDataToRequest(HttpServletRequest request, Tour tour) throws DaoSystemException {
        request.setAttribute("flashScope.tourName", tour.getName());
        request.setAttribute("flashScope.description", tour.getDescription());
        request.setAttribute("flashScope.selectedTourType", tour.getType());
    }
}
