package com.melentyev.travelagency.controller.tour;

import com.melentyev.travelagency.auth.Authenticator;
import com.melentyev.travelagency.controller.user.LoginController;
import com.melentyev.travelagency.entity.User;
import com.melentyev.travelagency.exception.NoSuchEntityException;
import com.melentyev.travelagency.service.OrderService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet(urlPatterns = "/tours/book", loadOnStartup = 1)
public class BookTourController extends HttpServlet {

    private static final Logger logger = LoggerFactory.getLogger(LoginController.class);
    private OrderService orderService = new OrderService();

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        User user = Authenticator.getUserFromSession(request);
        try {
            String idStr = request.getParameter("id");
            int id = Integer.parseInt(idStr);
            orderService.bookOfferByUser(id, user);
            response.sendRedirect("/admin/orders");
            return;
        } catch (NoSuchEntityException | NumberFormatException e) {
            logger.warn("NOT FOUND ", e);
            request.setAttribute("errorMsg", "notFoundError");
        } catch (Exception e) {
            logger.warn("PAGE ERROR", e);
            request.setAttribute("errorMsg", "systemCrash");
        }
        request.getRequestDispatcher("/error.jsp").forward(request, response);
    }

}
