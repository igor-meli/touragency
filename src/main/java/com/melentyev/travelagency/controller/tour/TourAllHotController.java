package com.melentyev.travelagency.controller.tour;

import com.melentyev.travelagency.auth.Authenticator;
import com.melentyev.travelagency.entity.Tour;
import com.melentyev.travelagency.entity.User;
import com.melentyev.travelagency.exception.ForbiddenActionException;
import com.melentyev.travelagency.exception.NoSuchEntityException;
import com.melentyev.travelagency.service.TourService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

import static com.melentyev.travelagency.auth.RightsAction.CHANGE_HOTNESS;

@WebServlet(urlPatterns = "/admin/hotness", loadOnStartup = 7)
public class TourAllHotController extends HttpServlet {

    private static final Logger logger = LoggerFactory.getLogger(TourAllHotController.class);
    private TourService tourService = new TourService();

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        try {
            User user = Authenticator.getUserFromSession(request);
            if (!user.hasRights(CHANGE_HOTNESS)) {
                logger.info("User with id = {} doesn't have access rights to this page", user.getId());
                throw new ForbiddenActionException("User with id = " + user.getId() + " doesn't have access rights to this page");
            }
            List<Tour> tours = tourService.getAllTours();
            if (tours.isEmpty())
                throw new NoSuchEntityException("Tours not found");

            request.setAttribute("tours", tours);
            request.getRequestDispatcher("/admin/changeTourHotness.jsp").forward(request, response);
            return;
        } catch (ForbiddenActionException e) {
            logger.warn("NO ACCESS", e);
            request.setAttribute("errorMsg", "forbidden");
        } catch (NoSuchEntityException e) {
            logger.warn("NOT FOUND", e);
            request.setAttribute("errorMsg", "notFoundError");
        } catch (Exception e) {
            logger.warn("PAGE ERROR", e);
            request.setAttribute("errorMsg", "systemCrash");
        }
        request.getRequestDispatcher("/error.jsp").forward(request, response);
    }

}
