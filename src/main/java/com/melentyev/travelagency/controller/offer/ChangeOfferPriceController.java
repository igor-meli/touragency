package com.melentyev.travelagency.controller.offer;

import com.melentyev.travelagency.auth.Authenticator;
import com.melentyev.travelagency.entity.Offer;
import com.melentyev.travelagency.entity.User;
import com.melentyev.travelagency.service.OfferService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

import static com.melentyev.travelagency.auth.RightsAction.ADD_TOUR;

@WebServlet(urlPatterns = "/admin/change/offer", loadOnStartup = 15)
public class ChangeOfferPriceController extends HttpServlet {

    private static final Logger logger = LoggerFactory.getLogger(ChangeOfferPriceController.class);
    private OfferService offerService = new OfferService();

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        User user = Authenticator.getUserFromSession(request);
        try {
            if (user.hasRights(ADD_TOUR)) {
                String offerIdStr = request.getParameter("offerId");
                String priceStr = request.getParameter("price");

                int offerId = Integer.parseInt(offerIdStr);
                int price = Integer.parseInt(priceStr);
                Offer offer = offerService.getOfferById(offerId);
                offer.setPrice(price);
                offerService.updateOffer(offer);

            }
        } catch (Exception e) {
            logger.warn("PAGE ERROR", e);
        }

    }

}
