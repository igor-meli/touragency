package com.melentyev.travelagency.controller.offer;

import com.melentyev.travelagency.auth.Authenticator;
import com.melentyev.travelagency.entity.User;
import com.melentyev.travelagency.exception.ForbiddenActionException;
import com.melentyev.travelagency.service.OfferService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

import static com.melentyev.travelagency.auth.RightsAction.ADD_TOUR;

@WebServlet(urlPatterns = "/admin/deleteOffer", loadOnStartup = 16)
public class DeleteOfferController extends HttpServlet {

    private static final Logger logger = LoggerFactory.getLogger(DeleteOfferController.class);
    private OfferService offerService = new OfferService();

    @Override
    protected void doDelete(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        User user = Authenticator.getUserFromSession(request);
        try {
            if (!user.hasRights(ADD_TOUR)) {
                logger.info("User with id = {} doesn't have access rights to this page", user.getId());
                throw new ForbiddenActionException("User with id = " + user.getId() + " doesn't have access rights to this page");
            }
            String offerIdStr = request.getParameter("offerId");
            int offerId = Integer.parseInt(offerIdStr);
            offerService.deleteById(offerId);
        } catch (ForbiddenActionException e) {
            logger.warn("NO ACCESS", e);
        } catch (Exception e) {
            logger.warn("PAGE ERROR", e);
        }
    }

}
