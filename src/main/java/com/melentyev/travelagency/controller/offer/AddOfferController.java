package com.melentyev.travelagency.controller.offer;

import com.melentyev.travelagency.auth.Authenticator;
import com.melentyev.travelagency.entity.*;
import com.melentyev.travelagency.exception.DaoSystemException;
import com.melentyev.travelagency.exception.ForbiddenActionException;
import com.melentyev.travelagency.service.HotelService;
import com.melentyev.travelagency.service.OfferService;
import com.melentyev.travelagency.service.RoomTypeService;
import com.melentyev.travelagency.service.TourService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

import static com.melentyev.travelagency.auth.RightsAction.ADD_TOUR;

@WebServlet(urlPatterns = "/admin/addOffer", loadOnStartup = 16)
public class AddOfferController extends HttpServlet {

    private static final Logger logger = LoggerFactory.getLogger(AddOfferController.class);
    private OfferService offerService = new OfferService();
    private TourService tourService = new TourService();
    private HotelService hotelService = new HotelService();
    private RoomTypeService roomTypeService = new RoomTypeService();

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        User user = Authenticator.getUserFromSession(request);
        try {
            if (!user.hasRights(ADD_TOUR)) {
                logger.info("User with id = {} doesn't have access rights to this page", user.getId());
                throw new ForbiddenActionException("User with id = " + user.getId() + " doesn't have access rights to this page");
            }
            String priceStr = request.getParameter("price");
            String tourIdStr = request.getParameter("tourId");
            String hotelIdStr = request.getParameter("hotelId");
            String roomTypeIdStr = request.getParameter("roomTypeId");
            int price = Integer.parseInt(priceStr);
            int tourId = Integer.parseInt(tourIdStr);
            int hotelId = Integer.parseInt(hotelIdStr);
            int roomTypeId = Integer.parseInt(roomTypeIdStr);

            Tour tour = tourService.getTourById(tourId);
            Hotel hotel = hotelService.getHotelById(hotelId);
            RoomType roomType = roomTypeService.getRoomTypeById(roomTypeId);
            Offer offer = new Offer(price, hotel, roomType, tour);

            if (offerService.isOfferExist(offer)) {
                request.setAttribute("flashScope.offerError", "offerExists");
                saveEnteredDataToRequest(request, offer);
                //saveDataForAddOfferPanel(request);
                response.sendRedirect("/admin/offers");
                return;
            }
            offerService.addOffer(offer);
            response.sendRedirect("/admin/offers");
            return;
        } catch (ForbiddenActionException e) {
            logger.warn("NO ACCESS", e);
            request.setAttribute("errorMsg", "forbidden");
        } catch (Exception e) {
            logger.warn("PAGE ERROR", e);
            request.setAttribute("errorMsg", "systemCrash");
        }
        request.getRequestDispatcher("/error.jsp").forward(request, response);
    }

    private void saveEnteredDataToRequest(HttpServletRequest request, Offer offer) throws DaoSystemException {
        request.setAttribute("flashScope.selectedTour", offer.getTour().getId());
        request.setAttribute("flashScope.price", offer.getPrice());
        request.setAttribute("flashScope.selectedHotel", offer.getHotel().getId());
        request.setAttribute("flashScope.selectedRoomType", offer.getRoomType().getId());
        Hotel hotel = offer.getHotel();
        request.setAttribute("flashScope.roomTypes", hotel.getRoomTypes());
        request.setAttribute("flashScope.tours", tourService.getAllTours());
        List<Hotel> hotels = hotelService.getAllHotels();
        request.setAttribute("flashScope.hotels", hotels);
    }

}
