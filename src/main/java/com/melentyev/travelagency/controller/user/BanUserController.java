package com.melentyev.travelagency.controller.user;

import com.melentyev.travelagency.auth.Authenticator;
import com.melentyev.travelagency.entity.User;
import com.melentyev.travelagency.exception.ForbiddenActionException;
import com.melentyev.travelagency.service.UserService;
import com.melentyev.travelagency.util.PaginatorUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

import static com.melentyev.travelagency.auth.RightsAction.BAN;

@WebServlet(urlPatterns = "/admin/ban", loadOnStartup = 11)
public class BanUserController extends HttpServlet {

    private static final Logger logger = LoggerFactory.getLogger(BanUserController.class);
    private static final int NUMBERS_OF_ROWS = 10;
    private UserService userService = new UserService();

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        User user = Authenticator.getUserFromSession(request);
        try {
            if (!user.hasRights(BAN)) {
                logger.info("User with id = {} doesn't have access rights to this page", user.getId());
                throw new ForbiddenActionException("User with id = " + user.getId() + " doesn't have access rights to this page");
            }
            String pageNumStr = request.getParameter("page");
            int page = 0;
            if (pageNumStr != null)
                page = Integer.parseInt(pageNumStr) - 1;

            PaginatorUtil.initPaginatorToRequest(page, userService.getRowsCount() - 1, NUMBERS_OF_ROWS, request);

            List<User> users = userService.getAllUsersLimit(page * NUMBERS_OF_ROWS, NUMBERS_OF_ROWS);
            users.remove(user);
            request.setAttribute("users", users);

            request.getRequestDispatcher("/admin/banUsers.jsp").forward(request, response);
            return;
        } catch (ForbiddenActionException e) {
            logger.warn("NO ACCESS", e);
            request.setAttribute("errorMsg", "forbidden");
        } catch (Exception e) {
            logger.warn("PAGE ERROR", e);
            request.setAttribute("errorMsg", "systemCrash");
        }
        request.getRequestDispatcher("/error.jsp").forward(request, response);
    }

    /**
     * API for /admin/banUsers.jsp (JQuery)
     * Parameters: userId
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        User user = Authenticator.getUserFromSession(request);
        if (user.hasRights(BAN)) {
            try {
                String userIdStr = request.getParameter("userId");
                int userId = Integer.parseInt(userIdStr);
                userService.blockUser(userId);
            } catch (Exception e) {
                logger.warn("PAGE ERROR", e);
            }
        }
    }

}
