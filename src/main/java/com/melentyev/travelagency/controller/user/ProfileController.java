package com.melentyev.travelagency.controller.user;

import com.melentyev.travelagency.auth.Authenticator;
import com.melentyev.travelagency.entity.User;
import com.melentyev.travelagency.service.UserService;
import com.melentyev.travelagency.validator.UserValidator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

@WebServlet(urlPatterns = "/admin", loadOnStartup = 9)
public class ProfileController extends HttpServlet {

    private static final Logger logger = LoggerFactory.getLogger(ProfileController.class);
    private UserValidator userValidator = new UserValidator();
    private UserService userService = new UserService();

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        try {
            User sessionUser = Authenticator.getUserFromSession(request);
            User user = userService.getUser(sessionUser.getId());
            saveEnteredDataToRequestIfAbsent(request, user);
            request.getRequestDispatcher("/admin/profile.jsp").forward(request, response);
            return;
        } catch (Exception e) {
            logger.warn("PAGE ERROR", e);
            request.setAttribute("errorMsg", "systemCrash");
        }
        request.getRequestDispatcher("/error.jsp").forward(request, response);
    }

    private void saveEnteredDataToRequestIfAbsent(HttpServletRequest request, User user) {
        if (request.getAttribute("editInfo") == null) {
            saveDataToRequest(request, user, "");
        }
    }

    private void saveDataToRequest(HttpServletRequest request, User user, String prefix) {
        Map<String, Object> attributes = new HashMap<>();
        attributes.put("login", user.getLogin());
        attributes.put("firstName", user.getFirstName());
        attributes.put("secondName", user.getSecondName());
        attributes.put("email", user.getEmail());
        request.setAttribute(prefix + "editInfo", attributes);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        try {
            String password = request.getParameter("password");
            String firstName = request.getParameter("firstName");
            String secondName = request.getParameter("secondName");
            String email = request.getParameter("email");

            User sessionUser = Authenticator.getUserFromSession(request);
            User user = userService.getUser(sessionUser.getId());
            user.setFirstName(firstName);
            user.setSecondName(secondName);
            user.setEmail(email);
            if (password != null && !password.trim().isEmpty()) {
                user.setPassword(password);
            }

            Map<String, String> errors = userValidator.validate(user);
            if (!errors.isEmpty()) {
                request.setAttribute("flashScope.profileError", errors);
                saveEnteredDataToFlashRequest(request, user);
                response.sendRedirect("/admin");
                return;
            }
            userService.updateUser(user);
            Authenticator.authorization(request, user);
            response.sendRedirect("/admin");
            return;
        } catch (Exception e) {
            logger.warn("PAGE ERROR", e);
            request.setAttribute("errorMsg", "systemCrash");
        }
        request.getRequestDispatcher("/error.jsp").forward(request, response);
    }

    private void saveEnteredDataToFlashRequest(HttpServletRequest request, User user) {
        saveDataToRequest(request, user, "flashScope.");
    }

}
