package com.melentyev.travelagency.controller.user;

import com.melentyev.travelagency.auth.Authenticator;
import com.melentyev.travelagency.entity.User;
import com.melentyev.travelagency.service.UserService;
import com.melentyev.travelagency.validator.UserValidator;
import com.melentyev.travelagency.validator.Validator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Map;

@WebServlet(urlPatterns = "/registerUser", loadOnStartup = 4)
public class RegistrationController extends HttpServlet {

    private static final Logger logger = LoggerFactory.getLogger(RegistrationController.class);
    private Validator<User> userValidator = new UserValidator();
    private UserService userService = new UserService();

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        try {
            String login = request.getParameter("login");
            String password = request.getParameter("password");
            String firstName = request.getParameter("firstName");
            String secondName = request.getParameter("secondName");
            String email = request.getParameter("email");

            saveEnteredDataToRequest(request, login, password, firstName, secondName, email);

            User user = new User(login, password, firstName, secondName, email);
            Map<String, String> errors = userValidator.validate(user);

            if (userService.isLoginExists(login)) {
                errors.put("login", "loginExist");
            }

            if (!errors.isEmpty()) {
                request.setAttribute("flashScope.signupError", errors);
                response.sendRedirect("/signup");
                return;
            }
            User savedUser = userService.saveUser(user);
            Authenticator.authorization(request, savedUser);
            response.sendRedirect("/tours");
            return;
        } catch (Exception e) {
            logger.warn("PAGE ERROR", e);
            request.setAttribute("errorMsg", "systemCrash");
        }
        request.getRequestDispatcher("/error.jsp").forward(request, response);

    }

    private void saveEnteredDataToRequest(HttpServletRequest request, String login, String password, String firstName,
                                          String secondName, String email) {
        request.setAttribute("flashScope.login", login);
        request.setAttribute("flashScope.password", password);
        request.setAttribute("flashScope.firstName", firstName);
        request.setAttribute("flashScope.secondName", secondName);
        request.setAttribute("flashScope.email", email);
    }

}
