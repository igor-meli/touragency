package com.melentyev.travelagency.controller.user;

import com.melentyev.travelagency.auth.Authenticator;
import com.melentyev.travelagency.entity.User;
import com.melentyev.travelagency.service.UserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet(urlPatterns = "/loginUser", loadOnStartup = 3)
public class LoginController extends HttpServlet {

    private static final Logger logger = LoggerFactory.getLogger(LoginController.class);
    private UserService userService = new UserService();

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String login = request.getParameter("login");
        String password = request.getParameter("password");
        request.setAttribute("flashScope.login", login);
        try {
            User user = userService.getUser(login, password);
            if (user == null) {
                request.setAttribute("flashScope.signinError", "wrongSignIn");
                response.sendRedirect("/login");
                return;
            }
            if (user.isBlocked()) {
                request.setAttribute("flashScope.signinError", "userBlocked");
                response.sendRedirect("/login");
                return;
            }
            Authenticator.authorization(request, user);
            response.sendRedirect("/tours");
            return;
        } catch (Exception e) {
            logger.warn("PAGE ERROR", e);
            request.setAttribute("errorMsg", "systemCrash");
        }
        request.getRequestDispatcher("/error.jsp").forward(request, response);
    }
}
