package com.melentyev.travelagency.filter;

import javax.servlet.DispatcherType;
import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;

/**
 * Filter which helps to implement the redirect-after-post pattern in which an Servlet receives a POST,
 * does some processing and then redirects to a JSP to display the outcome.
 * FlashScopes make temporary use of session to store themselves briefly between two requests.
 * The flash scope should only be used to transport success/error messages on simple non-Ajax applications.
 * <p/>
 * Data are kept for only one request
 */
@WebFilter(urlPatterns = "/*", dispatcherTypes = {DispatcherType.REQUEST, DispatcherType.FORWARD})
public class FlashScopeFilter extends BaseFilter {

    private static final String FLASH_SESSION_KEY = "FLASH_SESSION_KEY";
    private static final String FLASH_SCOPE_REQUEST_ATTRIBUTE_PREFIX = "flashScope.";

    @Override
    public void doFilter(HttpServletRequest request, HttpServletResponse response, FilterChain chain) throws IOException, ServletException {
        saveSessionAttributeToRequest(request);
        chain.doFilter(request, response);
        saveRequestAttributeToSession(request);
    }

    @SuppressWarnings("unchecked")
    protected void saveSessionAttributeToRequest(HttpServletRequest request) {
        HttpSession session = request.getSession(false);
        if (session != null) {
            Map<String, Object> flashParams = (Map<String, Object>) session.getAttribute(FLASH_SESSION_KEY);
            if (flashParams != null) {
                setFlashParamsToRequestAttribute(request, flashParams);
                session.removeAttribute(FLASH_SESSION_KEY);
            }
        }
    }

    private void setFlashParamsToRequestAttribute(HttpServletRequest request, Map<String, Object> flashParams) {
        for (Map.Entry<String, Object> flashEntry : flashParams.entrySet()) {
            request.setAttribute(flashEntry.getKey(), flashEntry.getValue());
        }
    }

    protected void saveRequestAttributeToSession(HttpServletRequest request) {
        Map<String, Object> flashParams = new HashMap<>();
        Enumeration<String> e = request.getAttributeNames();
        while (e.hasMoreElements()) {
            String paramName = e.nextElement();
            if (paramName.startsWith(FLASH_SCOPE_REQUEST_ATTRIBUTE_PREFIX)) {
                Object value = request.getAttribute(paramName);
                paramName = getParamNameWithoutPrefix(paramName);
                flashParams.put(paramName, value);
            }
        }
        if (flashParams.size() > 0) {
            HttpSession session = request.getSession(false);
            session.setAttribute(FLASH_SESSION_KEY, flashParams);
        }
    }

    private String getParamNameWithoutPrefix(String paramName) {
        return paramName.substring(FLASH_SCOPE_REQUEST_ATTRIBUTE_PREFIX.length(), paramName.length());
    }
}
