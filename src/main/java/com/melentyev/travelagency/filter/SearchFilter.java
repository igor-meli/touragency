package com.melentyev.travelagency.filter;

import com.melentyev.travelagency.exception.DaoSystemException;
import com.melentyev.travelagency.service.HotelTypeService;
import com.melentyev.travelagency.service.TourTypeService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.DispatcherType;
import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;


@WebFilter(urlPatterns = "/*", dispatcherTypes = {DispatcherType.FORWARD, DispatcherType.REQUEST})
public class SearchFilter extends BaseFilter {

    private static final Logger logger = LoggerFactory.getLogger(SearchFilter.class);
    private TourTypeService tourTypeService = new TourTypeService();
    private HotelTypeService hotelTypeService = new HotelTypeService();

    @Override
    public void doFilter(HttpServletRequest request, HttpServletResponse response, FilterChain chain) throws IOException, ServletException {
        try {
            request.setAttribute("tourSearchTypes", tourTypeService.getAllTourTypes());
            request.setAttribute("hotelSearchTypes", hotelTypeService.getAllHotelTypes());
        } catch (DaoSystemException e) {
            logger.warn("SEARCH ERROR", e);
            request.setAttribute("errorMsg", "systemCrash");
        }
        chain.doFilter(request, response);
    }

}
