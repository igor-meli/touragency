package com.melentyev.travelagency.filter;

import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebFilter;
import javax.servlet.annotation.WebInitParam;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletRequestWrapper;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.Locale;

@WebFilter(urlPatterns = "/*", initParams = {@WebInitParam(name = "en", value = "en"), @WebInitParam(name = "ru", value = "ru")})
public class LangFilter extends BaseFilter {

    private static final String COOKIES_LOCALE = "language";
    private static final ArrayList<String> localList = new ArrayList<>();

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
        Enumeration<String> local = filterConfig.getInitParameterNames();
        while (local.hasMoreElements()) {
            localList.add(local.nextElement());
        }
    }

    @Override
    public void doFilter(HttpServletRequest request, HttpServletResponse response, FilterChain chain) throws IOException, ServletException {
        final String lang = getLocale(request);
        HttpServletRequest langRequest = new HttpServletRequestWrapper((HttpServletRequest) request) {
            @Override
            public Locale getLocale() {
                return new Locale(lang);
            }
        };
        langRequest.setAttribute("query", getRequestQuery(request));
        setLangToCookies(langRequest, response, lang);
        String newUrl = removeLangDir(request.getRequestURI());
        langRequest.setAttribute("url", newUrl);
        langRequest.getRequestDispatcher(newUrl).forward(langRequest, response);

    }

    private String getRequestQuery(HttpServletRequest request) {
        return request.getQueryString() != null ? "?" + request.getQueryString() : null;
    }

    /**
     * Set user language at cookie if it didn't exist or language was changed
     *
     * @param request  - HttpServletRequest
     * @param response - HttpServletResponse
     * @param lang     - user language
     */

    private void setLangToCookies(HttpServletRequest request, HttpServletResponse response, String lang) {
        Cookie cookie = getCookie(request, COOKIES_LOCALE);

        if (cookie != null && lang.equals(cookie.getValue()))
            return;

        if (cookie == null) {
            cookie = new Cookie(COOKIES_LOCALE, lang);
        } else if (!lang.equals(cookie.getValue())) {
            cookie.setValue(lang);
        }
        cookie.setPath("/");
        cookie.setHttpOnly(true);
        cookie.setMaxAge(Integer.MAX_VALUE);
        response.addCookie(cookie);
    }

    private String getLocale(HttpServletRequest request) {
        String lang = getLocaleFromPath(request.getRequestURI());
        if (lang != null)
            return lang;

        lang = getLocaleFromCookies(request);
        if (lang != null)
            return lang;

        lang = request.getLocale().getLanguage();
        if (localList.contains(lang))
            return lang;

        return localList.get(0);
    }

    private String getLocaleFromCookies(HttpServletRequest request) {
        Cookie cookie = getCookie(request, COOKIES_LOCALE);
        return cookie != null ? cookie.getValue() : null;
    }

    private Cookie getCookie(HttpServletRequest request, String name) {
        if (request.getCookies() != null) {
            for (Cookie cookie : request.getCookies()) {
                if (cookie.getName().equals(name)) {
                    return cookie;
                }
            }
        }
        return null;
    }

    protected String getLocaleFromPath(String path) {
        for (String locale : localList) {
            if (path.startsWith("/" + locale)) {
                return locale;
            }
        }
        return null;

    }

    /**
     * Remove language from path (example: /en/index.html => /index.html)
     *
     * @param url - request URI
     * @return path without language
     */
    protected String removeLangDir(String url) {
        for (String lang : localList) {
            if (url.startsWith("/" + lang))
                return url.replace("/" + lang, "");
        }
        return url;
    }

}
