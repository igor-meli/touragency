package com.melentyev.travelagency.entity;

import java.util.List;

public class Offer {

    private int id;
    private int price;
    private Hotel hotel;
    private RoomType roomType;
    private Tour tour;
    private List<Order> orders;

    public Offer(int id, int price, Hotel hotel, RoomType roomType) {
        this.id = id;
        this.price = price;
        this.hotel = hotel;
        this.roomType = roomType;
    }

    public Offer(int price, Hotel hotel, RoomType roomType, Tour tour) {
        this.price = price;
        this.hotel = hotel;
        this.roomType = roomType;
        this.tour = tour;
    }

    public List<Order> getOrders() {
        return orders;
    }

    public void setOrders(List<Order> orders) {
        this.orders = orders;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public Hotel getHotel() {
        return hotel;
    }

    public void setHotel(Hotel hotel) {
        this.hotel = hotel;
    }

    public RoomType getRoomType() {
        return roomType;
    }

    public void setRoomType(RoomType roomType) {
        this.roomType = roomType;
    }

    public Tour getTour() {
        return tour;
    }

    public void setTour(Tour tour) {
        this.tour = tour;
    }

}
