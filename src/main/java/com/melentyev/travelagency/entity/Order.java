package com.melentyev.travelagency.entity;

public class Order {

    private int id;
    private int discount;
    private Status status;
    private User user;
    private Offer offer;
    private DiscountProgram discountProgram;
    private User approvedUser;

    public Order(int id, int discount, Status status, User user, Offer offer) {
        this.id = id;
        this.discount = discount;
        this.status = status;
        this.user = user;
        this.offer = offer;
    }

    public Order() {
    }

    public DiscountProgram getDiscountProgram() {
        return discountProgram;
    }

    public void setDiscountProgram(DiscountProgram discountProgram) {
        this.discountProgram = discountProgram;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getDiscount() {
        return discount;
    }

    public void setDiscount(int discount) {
        this.discount = discount;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Offer getOffer() {
        return offer;
    }

    public void setOffer(Offer offer) {
        this.offer = offer;
    }

    public int getTotalPrice() {
        double percent = ((double) discount) / 100;
        return (int) Math.ceil(offer.getPrice() - offer.getPrice() * percent);
    }

    public User getApprovedUser() {
        return approvedUser;
    }

    public void setApprovedUser(User approvedUser) {
        this.approvedUser = approvedUser;
    }
}
