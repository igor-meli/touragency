package com.melentyev.travelagency.entity;

import java.util.List;

public class TourType {

    private int id;
    private String name;
    private List<Tour> tourList;

    public TourType(int id, String name) {
        this.id = id;
        this.name = name;
    }

    public List<Tour> getTourList() {
        return tourList;
    }

    public void setTourList(List<Tour> tourList) {
        this.tourList = tourList;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
