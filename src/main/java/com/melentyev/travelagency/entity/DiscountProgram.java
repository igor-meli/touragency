package com.melentyev.travelagency.entity;

import java.util.List;

public class DiscountProgram {

    private int id;
    private String name;
    private int step;
    private int max;
    private List<Offer> offers;

    public DiscountProgram(int id, String name, int step, int max) {
        this.id = id;
        this.name = name;
        this.step = step;
        this.max = max;
    }

    public DiscountProgram() {

    }

    public DiscountProgram(String name, int step, int max) {
        this.name = name;
        this.step = step;
        this.max = max;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Offer> getOffers() {
        return offers;
    }

    public void setOffers(List<Offer> offers) {
        this.offers = offers;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getStep() {
        return step;
    }

    public void setStep(int step) {
        this.step = step;
    }

    public int getMax() {
        return max;
    }

    public void setMax(int max) {
        this.max = max;
    }
}
