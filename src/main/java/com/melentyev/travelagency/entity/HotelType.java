package com.melentyev.travelagency.entity;

import java.util.List;

public class HotelType {

    private int id;
    private String name;
    private List<Hotel> hotels;

    public HotelType(int id, String name) {
        this.id = id;
        this.name = name;
    }

    public List<Hotel> getHotels() {
        return hotels;
    }

    public void setHotels(List<Hotel> hotels) {
        this.hotels = hotels;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
}
