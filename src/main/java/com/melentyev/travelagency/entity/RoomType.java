package com.melentyev.travelagency.entity;

import java.util.List;

public class RoomType {

    private int id;
    private String name;
    private int maxChildren;
    private int maxAdults;
    private Hotel hotel;
    private List<Offer> offers;

    public RoomType(int id, String name, int maxChildren, int maxAdults, Hotel hotel) {
        this.id = id;
        this.name = name;
        this.maxChildren = maxChildren;
        this.maxAdults = maxAdults;
        this.hotel = hotel;
    }

    public RoomType(String name, int maxChildren, int maxAdults, Hotel hotel) {
        this.name = name;
        this.maxChildren = maxChildren;
        this.maxAdults = maxAdults;
        this.hotel = hotel;
    }

    public RoomType(int id, String name, int maxChildren, int maxAdults) {
        this.id = id;
        this.name = name;
        this.maxChildren = maxChildren;
        this.maxAdults = maxAdults;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public List<Offer> getOffers() {
        return offers;
    }

    public void setOffers(List<Offer> offers) {
        this.offers = offers;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getMaxChildren() {
        return maxChildren;
    }

    public void setMaxChildren(int maxChildren) {
        this.maxChildren = maxChildren;
    }

    public int getMaxAdults() {
        return maxAdults;
    }

    public void setMaxAdults(int maxAdults) {
        this.maxAdults = maxAdults;
    }

    public Hotel getHotel() {
        return hotel;
    }

    public void setHotel(Hotel hotel) {
        this.hotel = hotel;
    }
}
