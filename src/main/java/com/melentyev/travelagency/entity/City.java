package com.melentyev.travelagency.entity;

import java.util.List;

public class City {

    private int id;
    private String city;
    private Country country;
    private List<Hotel> hotels;

    public City(int id, String city, Country country) {
        this.id = id;
        this.city = city;
        this.country = country;
    }

    public City(int id, String city) {
        this.id = id;
        this.city = city;
    }

    public List<Hotel> getHotels() {
        return hotels;
    }

    public void setHotels(List<Hotel> hotels) {
        this.hotels = hotels;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public Country getCountry() {
        return country;
    }

    public void setCountry(Country country) {
        this.country = country;
    }
}
