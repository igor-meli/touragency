package com.melentyev.travelagency.entity;

import java.util.List;

public class Tour {

    private int id;
    private String name;
    private String description;
    private boolean hot;
    private TourType type;
    private int minPrice;
    private List<Offer> offerList;

    public Tour(String name, String description, TourType type) {
        this.name = name;
        this.description = description;
        this.type = type;
    }

    public Tour(int id, String name, String description, boolean hot, TourType type, int minPrice) {
        this.id = id;
        this.name = name;
        this.description = description;
        this.hot = hot;
        this.type = type;
        this.minPrice = minPrice;
    }

    public Tour(int id, String name, String description, boolean hot, TourType type) {
        this.id = id;
        this.name = name;
        this.description = description;
        this.hot = hot;
        this.type = type;
    }

    public Tour(int id, String name, String description, boolean hot) {
        this.id = id;
        this.name = name;
        this.description = description;
        this.hot = hot;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public List<Offer> getOfferList() {
        return offerList;
    }

    public void setOfferList(List<Offer> offerList) {
        this.offerList = offerList;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public boolean isHot() {
        return hot;
    }

    public void setHot(boolean hot) {
        this.hot = hot;
    }

    public TourType getType() {
        return type;
    }

    public void setType(TourType type) {
        this.type = type;
    }

    public int getMinPrice() {
        return minPrice;
    }

    public void setMinPrice(int minPrice) {
        this.minPrice = minPrice;
    }
}
