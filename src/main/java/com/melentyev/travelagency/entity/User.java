package com.melentyev.travelagency.entity;

import java.util.List;

public class User {

    private int id;
    private String login;
    private String password;
    private String firstName;
    private String secondName;
    private String email;
    private boolean blocked;
    private Role role;
    private int countApproved;
    private List<Order> orderList;

    public User() {
    }

    public User(int id, String login, String password, String firstName, String secondName, String email,
                boolean blocked) {
        this.id = id;
        this.login = login;
        this.password = password;
        this.firstName = firstName;
        this.secondName = secondName;
        this.email = email;
        this.blocked = blocked;
    }

    public User(String login, String password, String firstName, String secondName, String email) {
        this.login = login;
        this.password = password;
        this.firstName = firstName;
        this.secondName = secondName;
        this.email = email;
    }

    public User(int id, String login, String password, String firstName, String secondName, String email, boolean blocked, int countApproved) {
        this.id = id;
        this.login = login;
        this.password = password;
        this.firstName = firstName;
        this.secondName = secondName;
        this.email = email;
        this.blocked = blocked;
        this.countApproved = countApproved;
    }

    public boolean hasRights(String action) {
        if (role != null && role.getRights() != null) {
            for (Rights rights : role.getRights()) {
                if (action.equalsIgnoreCase(rights.getAction().getName()))
                    return true;
            }
        }
        return false;
    }

    public boolean isInRole(String userRole) {
        return role != null && userRole != null && userRole.equalsIgnoreCase(role.getName());
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public List<Order> getOrderList() {
        return orderList;
    }

    public void setOrderList(List<Order> orderList) {
        this.orderList = orderList;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getSecondName() {
        return secondName;
    }

    public void setSecondName(String secondName) {
        this.secondName = secondName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public boolean isBlocked() {
        return blocked;
    }

    public void setBlocked(boolean isBlocked) {
        this.blocked = isBlocked;
    }

    public Role getRole() {
        return role;
    }

    public void setRole(Role role) {
        this.role = role;
    }

    @Override
    public int hashCode() {
        int result = id;
        result = 31 * result + login.hashCode();
        result = 31 * result + firstName.hashCode();
        result = 31 * result + secondName.hashCode();
        result = 31 * result + email.hashCode();
        result = 31 * result + (blocked ? 1 : 0);
        return result;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        User user = (User) o;

        if (id != user.id) return false;
        if (!firstName.equals(user.firstName)) return false;
        if (!login.equals(user.login)) return false;
        if (!secondName.equals(user.secondName)) return false;
        if (!email.equals(user.email)) return false;
        if (blocked != user.blocked) return false;
        return true;
    }

    public int getCountApproved() {
        return countApproved;
    }

    public void setCountApproved(int countApproved) {
        this.countApproved = countApproved;
    }
}
