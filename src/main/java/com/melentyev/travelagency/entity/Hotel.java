package com.melentyev.travelagency.entity;

import java.util.List;

public class Hotel {

    private int id;
    private String name;
    private City city;
    private HotelType hotelType;
    private List<RoomType> roomTypes;
    private List<Offer> offerList;

    public Hotel(int id, String name, City city, HotelType hotelType) {
        this.id = id;
        this.name = name;
        this.city = city;
        this.hotelType = hotelType;
    }

    public Hotel(int id, String name) {
        this.id = id;
        this.name = name;
    }

    public List<Offer> getOfferList() {
        return offerList;
    }

    public void setOfferList(List<Offer> offerList) {
        this.offerList = offerList;
    }

    public List<RoomType> getRoomTypes() {
        return roomTypes;
    }

    public void setRoomTypes(List<RoomType> roomTypes) {
        this.roomTypes = roomTypes;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public City getCity() {
        return city;
    }

    public void setCity(City city) {
        this.city = city;
    }

    public HotelType getHotelType() {
        return hotelType;
    }

    public void setHotelType(HotelType hotelType) {
        this.hotelType = hotelType;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Hotel hotel = (Hotel) o;

        if (id != hotel.id) return false;
        if (city != null ? !city.equals(hotel.city) : hotel.city != null) return false;
        if (hotelType != null ? !hotelType.equals(hotel.hotelType) : hotel.hotelType != null) return false;
        if (name != null ? !name.equals(hotel.name) : hotel.name != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = id;
        result = 31 * result + (name != null ? name.hashCode() : 0);
        result = 31 * result + (city != null ? city.hashCode() : 0);
        result = 31 * result + (hotelType != null ? hotelType.hashCode() : 0);
        return result;
    }
}
