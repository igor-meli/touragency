package com.melentyev.travelagency.entity;

public class Rights {

    private int id;
    private Role role;
    private Action action;

    public Rights() {
    }

    public Rights(int id) {
        this.id = id;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Action getAction() {
        return action;
    }

    public void setAction(Action action) {
        this.action = action;
    }

    public Role getRole() {
        return role;
    }

    public void setRole(Role role) {
        this.role = role;
    }
}
