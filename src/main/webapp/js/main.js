var MAX_SCROLL = 220;
$(document).ready(function () {

    $(window).scroll(function () {
        var menu = $("#left-menu");
        var scrollTop = document.documentElement.scrollTop || document.body.scrollTop;
        if (scrollTop > MAX_SCROLL && !menu.is(".fixed-top")) {
            menu.addClass("fixed-top");
            menu.removeClass("relative");
        }
        else if (scrollTop < MAX_SCROLL && menu.is(".fixed-top")) {
            menu.addClass("relative");
            menu.removeClass("fixed-top");
        }
    })
});

function validateSearch() {
    var isValid = true;
    var price = document.forms["search"]["price"].value;
    if (!isNumeric(price) || !isLengthMinMax(price, 0, 5)) {
        isValid = false;
        $("#crit2").addClass("has-error");
    }
    var adults = document.forms["search"]["adults"].value;
    if (!isNumeric(adults) || !isLengthMinMax(adults, 0, 2) || !isMinMaxValue(adults, 0, 10)) {
        isValid = false;
        $("#crit3").addClass("has-error");
    }
    var children = document.forms["search"]["children"].value;
    if (!isNumeric(children) || !isLengthMinMax(children, 0, 2) || !isMinMaxValue(children, 0, 10)) {
        isValid = false;
        $("#crit4").addClass("has-error");
    }
    return isValid;
}
function isNumeric(input) {
    var inputValue = input.trim();
    return (inputValue.search(/^[0-9]+$/) !== -1) || input == '';
}

function isLengthMinMax(input, minLength, maxLength) {
    var inputValue = input.trim();
    return (inputValue.length >= minLength) && (inputValue.length <= maxLength);
}

function isMinMaxValue(input, min, max) {
    var inputValue = input.trim();
    return (inputValue >= min) && (inputValue <= max);
}

function changeHot(id) {
    $.ajax({
        type: "POST",
        url: "/admin/change/hotness",
        data: "id=" + id
    });
}

function changeStatus(orderId) {
    var elem = document.getElementById("status" + orderId);
    var changedStatusId = elem.options[elem.selectedIndex].value;
    var cngClass = changedStatusId == 1 ? "panel-booked" : changedStatusId == 2 ? "panel-paid" : "panel-canceled";
    cngClass += " panel-body";
    document.getElementById("order" + orderId).className = cngClass;
    $.ajax({
        type: "POST",
        url: "/admin/change/status",
        data: "orderId=" + orderId + "&changedStatusId=" + changedStatusId
    });
}

function blockInput() {
    event.preventDefault();
}

function checkNumericInput() {
    if ((event.which < 48 || event.which > 57) && event.which != 8 && event.which != 46) {
        event.preventDefault();
    } else if (event.target.value.length > 4 && event.which != 8 && event.which != 46) {
        event.preventDefault();
    }

}

function changeOrderDiscount(orderId) {
    var discount = document.getElementById("intNumberStep" + orderId).value;
    $.ajax({
        type: "POST",
        url: "/admin/order/discount",
        data: "orderId=" + orderId + "&discount=" + discount,
        success: function (msg) {
            $("#totalPrice" + orderId).html(msg + "$");
        }
    });
}

function deleteDiscountProgram(discountId) {
    $.ajax({
        type: "DELETE",
        url: "/admin/deleteDiscount?discountId=" + discountId
    }).done(function () {
        location.reload();
    });
}

function changeOrderDiscountProgram(orderId) {
    var elem = document.getElementById("discount" + orderId);
    var selectedDiscountId = elem.options[elem.selectedIndex].value;
    $.ajax({
        type: "POST",
        url: "/admin/order/discountProgram",
        data: "orderId=" + orderId + "&discountProgramId=" + selectedDiscountId
    }).done(function () {
        location.reload();
    });

}

function changeOfferPrice(offerId) {
    var price = document.getElementById("price" + offerId).value;
    $.ajax({
        type: "POST",
        url: "/admin/change/offer",
        data: "offerId=" + offerId + "&price=" + price
    }).done(function () {
        location.reload();
    });
}

function banUser(userId) {
    var userBan = document.getElementById("banCheck" + userId).checked;
    document.getElementById("user" + userId).className = userBan ? "panel-canceled" : "panel-booked";
    $.ajax({
        type: "POST",
        url: "/admin/ban",
        data: "userId=" + userId
    });
}

function deleteRoomType(roomTypeId) {
    $.ajax({
        type: "DELETE",
        url: "/admin/deleteRoomType?roomTypeId=" + roomTypeId
    }).done(function () {
        location.reload();
    });
}

function deleteTour(tourId) {
    $.ajax({
        type: "DELETE",
        url: "/admin/deleteTour?tourId=" + tourId
    }).done(function () {
        location.reload();
    });
}
function deleteOffer(offerId) {
    $.ajax({
        type: "DELETE",
        url: "/admin/deleteOffer?offerId=" + offerId
    }).done(function () {
        location.reload();
    });
}
function changeRoom() {
    var elem = document.getElementById("hotel");
    var hotelId = elem.options[elem.selectedIndex].value;
    $.ajax({
        type: "GET",
        url: "/api/hotel/rooms",
        data: "hotelId=" + hotelId,
        success: function (data) {
            $("#roomType").empty().append(data);
        }
    });
}

