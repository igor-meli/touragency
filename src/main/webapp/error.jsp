<%@ include file="/jsp/jspheaders.jsp" %>
<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title><fmt:message key="title"/></title>
    <%@ include file="/jsp/cssincl.jsp" %>
</head>
<body>

<%@ include file="/jsp/header.jsp" %>
<div class="content-main">
    <div class="panel panel-primary content-panel">
        <div class="panel-heading">
            <h3 class="panel-title">
                <fmt:message key="error"/>
            </h3>
        </div>
        <div class="panel-body">
            <fmt:message key="${requestScope['errorMsg']}"/>
            <br>
        </div>
    </div>

    <%@ include file="/jsp/search.jsp" %>
</div>
<%@ include file="/jsp/jslibs.jsp" %>
</body>
</html>
