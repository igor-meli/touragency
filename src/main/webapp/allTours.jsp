<%@ taglib prefix="paginator" uri="/WEB-INF/tlds/paginator" %>
<%@ include file="/jsp/jspheaders.jsp" %>
<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title><fmt:message key="title"/></title>
    <%@ include file="/jsp/cssincl.jsp" %>
</head>
<body>

<%@ include file="/jsp/header.jsp" %>
<div class="content-main">
    <div class="panel panel-primary content-panel">
        <div class="panel-heading">
            <h3 class="panel-title"><fmt:message key="availableTours"/></h3>
        </div>
        <div class="panel-body">
            <c:forEach items="${requestScope.tours}" var="tour">
                <div style="color: red">
                    <c:if test="${tour.hot}">
                        <img src="<c:url value="/images/hot.gif"/>">
                    </c:if>
                    <b>${tour.name}</b>
                    <label class="price"><fmt:message key="startingAt"/> ${tour.minPrice}$</label><br>
                </div>
                <b><fmt:message key="type"/></b>: ${tour.type.name}<br>
                <b><fmt:message key="desc"/></b>:<br>
                ${tour.description}<br>
                <br>
                <a href="<c:url value="/tour?id=${tour.id}"/>" class="buttonInfo"><fmt:message key="info"/></a>
                <hr/>
            </c:forEach>

            <fmt:message key="next" var="next"/>
            <fmt:message key="previous" var="prev"/>
            <c:url var="searchUri" value="/tours?page=##"/>
            <paginator:display maxLinks="5" currPage="${currPage}" totalPages="${totalPages}" uri="${searchUri}"
                               next="${next}" previous="${prev}"/>
        </div>
    </div>

    <%@ include file="/jsp/search.jsp" %>
</div>
<%@ include file="/jsp/jslibs.jsp" %>
</body>
</html>
