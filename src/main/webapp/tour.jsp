<%--<jsp:include page="jsp/header.jsp" flush="true"/>--%>
<%@ include file="/jsp/jspheaders.jsp" %>
<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title><fmt:message key="title"/></title>
    <%@ include file="/jsp/cssincl.jsp" %>
</head>
<body>

<%@ include file="/jsp/header.jsp" %>
<jsp:useBean id="tour" scope="request" type="com.melentyev.travelagency.entity.Tour"/>
<div class="content-main">
    <div class="panel panel-primary content-panel">
        <div class="panel-heading">
            <h3 class="panel-title">
                ${tour.name}
            </h3>
        </div>
        <div class="panel-body">
            <b><fmt:message key="type"/></b>: ${tour.type.name}<br>
            <b><fmt:message key="desc"/></b>:<br>
            ${tour.description}<br>
            <br>
        </div>
        <c:forEach items="${tour.offerList}" var="offer">
            <div class="panel-footer panel-body panel-danger">
                <span style="text-decoration: underline;"><b>${offer.roomType.name}</b></span>
                <label class="price">${offer.price}$</label><br/>
                <b><fmt:message key="hotel"/></b>: ${offer.hotel.name} (${offer.hotel.hotelType.name})<br>
                <b><fmt:message key="city"/></b>: ${offer.hotel.city.city} (${offer.hotel.city.country.country})<br/>
                <b><fmt:message key="adults"/></b>: ${offer.roomType.maxAdults}<br/>
                <b><fmt:message key="children"/></b>: ${offer.roomType.maxChildren}
                <c:if test="${User!=null}">
                    <a href="/tours/book?id=${offer.id}" class="buyButt"><fmt:message key="book"/></a>
                </c:if>
            </div>
        </c:forEach>
    </div>

    <%@ include file="/jsp/search.jsp" %>
</div>
<%@ include file="/jsp/jslibs.jsp" %>
</body>
</html>
