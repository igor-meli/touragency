<%@ include file="/jsp/jspheaders.jsp" %>
<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title><fmt:message key="title"/></title>
    <%@ include file="/jsp/cssincl.jsp" %>
</head>
<body>

<%@ include file="/jsp/header.jsp" %>
<div id="alert-container"></div>
<div class="content-main">
    <div class="panel panel-primary content-panel">
        <div class="panel-heading">
            <h3 class="panel-title"><fmt:message key="offers"/></h3>
        </div>
        <div class="panel-body">
            <c:forEach items="${offers}" var="offer">
                <span style="text-decoration: underline;"><b>${offer.roomType.name}</b></span>
                <img class="cursorPointer" src="<c:url value="/images/delete.jpg"/>"
                     onclick="deleteOffer(${offer.id})"/>
                <label class="price">${offer.price}$</label><br/>
                <input id="price${offer.id}" type="number" min="0" max="99999" value="${offer.price}"
                       onkeydown="checkNumericInput()"/>
                <input type="submit" onclick="changeOfferPrice(${offer.id})"
                       value="<fmt:message key="save"/>"/> <br>
                <b><fmt:message key="tour"/></b>: ${offer.tour.name}<br>
                <b><fmt:message key="hotel"/></b>: ${offer.hotel.name} (${offer.hotel.hotelType.name})<br>
                <b><fmt:message key="city"/></b>: ${offer.hotel.city.city} (${offer.hotel.city.country.country})<br/>
                <b><fmt:message key="adults"/></b>: ${offer.roomType.maxAdults}<br/>
                <b><fmt:message key="children"/></b>: ${offer.roomType.maxChildren}
                <hr/>
            </c:forEach>
        </div>
    </div>

    <%@ include file="/admin/offer/addOffer.jsp" %>
</div>
<%@ include file="/jsp/jslibs.jsp" %>
</body>
</html>
