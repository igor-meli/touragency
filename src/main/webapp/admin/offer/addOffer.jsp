<div id="left-menu">
    <div class="panel panel-primary">
        <div class="panel-heading">
            <h3 class="panel-title"><fmt:message key="addOffer"/></h3>
        </div>
        <form method="POST" name="addOffer" action="<c:url value="/admin/addOffer"/>">
            <div class="panel-body">
                <div class="form-group">
                    <c:if test="${offerError!=null}">
                        <label class="col-lg-10 error"><fmt:message key="${offerError}"/></label>
                    </c:if>
                    <label for="price"><fmt:message key="price"/></label><br/>
                    <input id="price" name="price" type="number" min="0" max="99999" value="${price == null? 0:price}"
                           onkeydown="checkNumericInput()"/>
                </div>

                <div class="form-group">
                    <label for="tour"><fmt:message key="tour"/></label><br/>
                    <select class="form-control" id="tour" name="tourId">
                        <c:forEach items="${tours}" var="tour">
                            <c:choose>
                                <c:when test="${selectedTour == tour.id}">
                                    <option selected="selected" value="${tour.id}">${tour.name}</option>
                                </c:when>
                                <c:otherwise>
                                    <option value="${tour.id}">${tour.name}</option>
                                </c:otherwise>
                            </c:choose>
                        </c:forEach>
                    </select>
                </div>

                <div class="form-group">
                    <label for="hotel"><fmt:message key="hotel"/></label><br/>
                    <select onchange="changeRoom()" class="form-control" id="hotel" name="hotelId">
                        <c:forEach items="${hotels}" var="hotel">
                            <c:choose>
                                <c:when test="${selectedHotel == hotel.id}">
                                    <option selected="selected" value="${hotel.id}">${hotel.name}</option>
                                </c:when>
                                <c:otherwise>
                                    <option value="${hotel.id}">${hotel.name}</option>
                                </c:otherwise>
                            </c:choose>
                        </c:forEach>
                    </select>
                </div>

                <div class="form-group">
                    <label for="roomType"><fmt:message key="roomType"/></label><br/>
                    <select class="form-control" id="roomType" name="roomTypeId">
                        <c:forEach items="${roomTypes}" var="roomType">
                            <c:choose>
                                <c:when test="${selectedRoomType == roomType.id}">
                                    <option selected="selected" value="${roomType.id}">${roomType.name}</option>
                                </c:when>
                                <c:otherwise>
                                    <option value="${roomType.id}">${roomType.name}</option>
                                </c:otherwise>
                            </c:choose>
                        </c:forEach>
                    </select>
                </div>


                <div class="form-group search">
                    <button type="submit" class="btn btn-success search">
                        <fmt:message key="addOffer"/></button>
                </div>
            </div>
        </form>
    </div>
</div>