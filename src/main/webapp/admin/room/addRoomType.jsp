<div id="left-menu">
    <div class="panel panel-primary">
        <div class="panel-heading">
            <h3 class="panel-title"><fmt:message key="addRoomType"/></h3>
        </div>
        <form method="post" name="addRoomType" action="<c:url value="/admin/addRoomType"/>">
            <div class="panel-body">
                <div class="form-group">
                    <c:if test="${roomtypeError['roomtypeName']!=null}">
                        <label class="col-lg-10 error"><fmt:message key="${roomtypeError['roomtypeName']}"/></label>
                    </c:if>
                    <input class="form-control" type="text" name="roomtypeName" maxlength="30"
                           placeholder="<fmt:message key ="roomtypeName"/>"
                           value="${requestScope.roomtypeName}">
                </div>
                <div class="form-group">
                    <label for="intNumberStep"><fmt:message key="adults"/></label><br/>
                    <c:if test="${roomtypeError['maxAdults']!=null}">
                        <label class="col-lg-10 error"><fmt:message key="${roomtypeError['maxAdults']}"/></label>
                    </c:if>
                    <input id="intNumberStep" type="number" name="maxAdults" min="1" max="10" step="1"
                           value="${requestScope.maxAdults eq null ? 1 : requestScope.maxAdults}"
                           onkeydown="blockInput()"/>
                </div>
                <div class="form-group">
                    <label for="intNumberMax"><fmt:message key="children"/></label><br/>
                    <c:if test="${roomtypeError['maxChildren']!=null}">
                        <label class="col-lg-10 error"><fmt:message key="${roomtypeError['maxChildren']}"/></label>
                    </c:if>
                    <input id="intNumberMax" type="number" name="maxChildren" min="0" max="10" step="1"
                           value="${requestScope.maxChildren eq null ? 0 : requestScope.maxChildren}"
                           onkeydown="blockInput()"/>
                </div>

                <div class="form-group">
                    <label for="hotelId"><fmt:message key="hotel"/></label><br/>
                    <select class="form-control" id="hotelId" name="hotelId">
                        <c:forEach items="${requestScope.hotels}" var="hotel">
                            <option value="${hotel.id}">${hotel.name}</option>
                        </c:forEach>
                    </select>
                </div>
                <div class="form-group search">
                    <button type="submit" class="btn btn-success search">
                        <fmt:message key="addRoomType"/></button>
                </div>
            </div>
        </form>
    </div>
</div>