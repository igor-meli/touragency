<%@ include file="/jsp/jspheaders.jsp" %>
<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title><fmt:message key="title"/></title>
    <%@ include file="/jsp/cssincl.jsp" %>
</head>
<body>

<%@ include file="/jsp/header.jsp" %>
<div id="alert-container"></div>
<div class="content-main">
    <div class="panel panel-primary content-panel">
        <div class="panel-heading">
            <h3 class="panel-title"><fmt:message key="roomType"/></h3>
        </div>
        <div class="panel-body">
            <c:forEach items="${requestScope.roomtypes}" var="roomtype">
                <span class="error">${roomtype.hotel.name}</span><br/>
                <b>${roomtype.name}</b>
                (<fmt:message key="adults"/>: ${roomtype.maxAdults},
                <fmt:message key="children"/>: ${roomtype.maxChildren})

                <a href="<c:url value="/admin/editRoomType?id=${roomtype.id}"/>">
                    <img src="<c:url value="/images/edit.jpg"/>"/>
                </a>
                <img class="cursorPointer" src="<c:url value="/images/delete.jpg"/>"
                     onclick="deleteRoomType(${roomtype.id})"/>
                <br/>
                <hr/>
            </c:forEach>
        </div>
    </div>

    <%@ include file="/admin/room/addRoomType.jsp" %>
</div>
<%@ include file="/jsp/jslibs.jsp" %>
</body>
</html>
