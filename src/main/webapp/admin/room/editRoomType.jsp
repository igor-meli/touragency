<%@ include file="/jsp/jspheaders.jsp" %>
<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title><fmt:message key="title"/></title>
    <%@ include file="/jsp/cssincl.jsp" %>
</head>
<body>

<%@ include file="/jsp/header.jsp" %>

<form method="post" name="update" action="<c:url value="/admin/editRoomType"/>" class="form-horizontal login-div">
    <fieldset>
        <legend><fmt:message key="edit"/></legend>
        <div class="form-group">
            <label class="col-lg-2 control-label"><fmt:message key="roomtypeName"/></label>
            <c:if test="${roomtypeError['roomtypeName']!=null}">
                <label class="col-lg-10 error"><fmt:message key="${roomtypeError['roomtypeName']}"/></label>
            </c:if>
            <div class="col-lg-10">
                <input class="form-control" type="text" name="roomtypeName" maxlength="30"
                       placeholder="<fmt:message key ="roomtypeName"/>"
                       value="${editInfo.roomtypeName}">
            </div>
        </div>
        <div class="form-group">
            <label class="col-lg-2 control-label"><fmt:message key="adults"/></label>
            <c:if test="${roomtypeError['maxAdults']!=null}">
                <label class="col-lg-10 error"><fmt:message key="${roomtypeError['maxAdults']}"/></label>
            </c:if>
            <div class="col-lg-10">
                <input id="intNumberStep" type="number" name="maxAdults" min="1" max="10" step="1"
                       value="${editInfo.maxAdults eq null ? 1 : editInfo.maxAdults}"
                       onkeydown="blockInput()"/>
            </div>
        </div>
        <div class="form-group">
            <label class="col-lg-2 control-label"><fmt:message key="children"/></label>
            <c:if test="${roomtypeError['maxChildren']!=null}">
                <label class="col-lg-10 error"><fmt:message key="${roomtypeError['maxChildren']}"/></label>
            </c:if>
            <div class="col-lg-10">
                <input id="intNumberMax" type="number" name="maxChildren" min="0" max="10" step="1"
                       value="${editInfo.maxChildren eq null ? 0 : editInfo.maxChildren}"
                       onkeydown="blockInput()"/>
            </div>
        </div>

        <input hidden="hidden" name="id" value="${editInfo.roomTypeId}">

        <div class="form-group">
            <div class="col-lg-10 col-lg-offset-2">
                <button type="submit" class="btn btn-primary btn-login"><fmt:message key="save"/></button>
            </div>
        </div>
    </fieldset>
</form>

<%@ include file="/jsp/jslibs.jsp" %>
</body>
</html>