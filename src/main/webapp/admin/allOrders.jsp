<%@ taglib prefix="paginator" uri="/WEB-INF/tlds/paginator" %>
<%@ include file="/jsp/jspheaders.jsp" %>
<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title><fmt:message key="title"/></title>
    <%@ include file="/jsp/cssincl.jsp" %>
</head>
<body>

<%@ include file="/jsp/header.jsp" %>
<div class="content-main">
    <div class="panel panel-primary orders-panel">
        <div class="panel-heading">
            <h3 class="panel-title"><fmt:message key="cngStatus"/></h3>
        </div>
        <div class="panel-body">
            <c:forEach items="${requestScope.orders}" var="order">
                <c:set var="panelColor"
                       value="${(order.status.id) eq 1? 'panel-booked':(order.status.id) eq 2?'panel-paid':'panel-canceled'}"
                       scope="page"/>

                <div class="${panelColor} panel-body" id="order${order.id}">
                    <div id="userInfo" class="status-control">
                        <b><fmt:message key="customer"/></b>: ${order.user.firstName} ${order.user.secondName} <br/>
                        <b><fmt:message key="email"/></b>: ${order.user.email} <br/>

                        <c:if test="${User.hasRights('cngStatus')}">
                            <select onchange="changeStatus(${order.id})" id="status${order.id}">
                                <c:forEach items="${requestScope.status}" var="status">
                                    <c:choose>
                                        <c:when test="${order.status.id == status.id}">
                                            <option selected="selected" value="${status.id}">${status.name}</option>
                                        </c:when>
                                        <c:otherwise>
                                            <option value="${status.id}">${status.name}</option>
                                        </c:otherwise>
                                    </c:choose>
                                </c:forEach>
                            </select>
                            <br/>
                        </c:if>
                        <c:if test="${User.hasRights('cngDiscount')}">
                            <fmt:message key="discount"/><br/>
                            <select onchange="changeOrderDiscountProgram(${order.id})"
                                    id="discount${order.id}">
                                <option selected="selected" value="0"><fmt:message key="none"/></option>
                                <c:forEach items="${requestScope.discounts}" var="discount">
                                    <c:choose>
                                        <c:when test="${order.discountProgram.id == discount.id}">
                                            <option selected="selected" value="${discount.id}">${discount.name}</option>
                                        </c:when>
                                        <c:otherwise>
                                            <option value="${discount.id}">${discount.name}</option>
                                        </c:otherwise>
                                    </c:choose>
                                </c:forEach>
                            </select>
                            <br/>
                        </c:if>
                    </div>
                    <b><fmt:message key="tour"/></b>:
                    <span style="text-decoration: underline;">
                        <a href="<c:url value="/tours?id=${order.offer.tour.id}"/>">${order.offer.tour.name}</a>
                    </span>
                    <br/>
                    <c:if test="${User.hasRights('cngDiscount')}">
                        <label for="intNumberStep${order.id}" class="offerDiscount">
                            <fmt:message key="discount"/>:
                            <c:choose>
                                <c:when test="${order.discountProgram==null}">
                                    <input id="intNumberStep${order.id}" type="number" min="0" max="99"
                                           disabled="disabled"
                                           value="${order.discount}"
                                           onkeydown="blockInput()"/>
                                </c:when>
                                <c:otherwise>
                                    <input id="intNumberStep${order.id}" type="number" min="0"
                                           max="${order.discountProgram.max}"
                                           step="${order.discountProgram.step}" value="${order.discount}"
                                           onkeydown="blockInput()"/>
                                    <input type="submit" onclick="changeOrderDiscount(${order.id})"
                                           value="<fmt:message key="save"/>"/>
                                </c:otherwise>
                            </c:choose>
                        </label>
                        <br/>
                    </c:if>


                        <%--<label class="price"> ${order.status.name}</label><br/>--%>
                    <b><fmt:message key="hotel"/></b>: ${order.offer.hotel.name}
                    (${order.offer.hotel.hotelType.name})<br>
                    <b><fmt:message key="city"/></b>: ${order.offer.hotel.city.city}
                    (${order.offer.hotel.city.country.country})<br/>
                    <b><fmt:message key="roomType"/></b>: ${order.offer.roomType.name}<br>
                    <b><fmt:message key="adults"/></b>: ${order.offer.roomType.maxAdults}<br/>
                    <b><fmt:message key="children"/></b>: ${order.offer.roomType.maxChildren}<br/>

                    <label><fmt:message key="price"/>: ${order.offer.price}$</label><br/>
                    <label class="offerPrice">
                        <fmt:message key="totalPrice"/>: <span id="totalPrice${order.id}">${order.totalPrice}$</span>
                    </label>
                    <br/>

                </div>

                <hr/>
            </c:forEach>

            <fmt:message key="next" var="next"/>
            <fmt:message key="previous" var="prev"/>
            <c:url var="searchUri" value="/admin/change/status?page=##"/>
            <paginator:display maxLinks="5" currPage="${currPage}" totalPages="${totalPages}" uri="${searchUri}"
                               next="${next}" previous="${prev}"/>
        </div>
    </div>

</div>
<%@ include file="/jsp/jslibs.jsp" %>
</body>
</html>
