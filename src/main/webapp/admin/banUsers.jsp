<%@ taglib prefix="paginator" uri="/WEB-INF/tlds/paginator" %>
<%@ include file="/jsp/jspheaders.jsp" %>
<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title><fmt:message key="title"/></title>
    <%@ include file="/jsp/cssincl.jsp" %>
</head>
<body>

<%@ include file="/jsp/header.jsp" %>

<div class="content-main">
    <div class="panel panel-primary orders-panel">
        <div class="panel-heading">
            <h3 class="panel-title">
                <label><fmt:message key="users"/></label>
            </h3>
        </div>
        <div class="panel-body">
            <c:forEach items="${users}" var="user">
                <c:set var="panelColor"
                       value="${(user.blocked) eq false? 'panel-booked':'panel-canceled'}"
                       scope="page"/>
                <div class="${panelColor}" id="user${user.id}">
                    <c:choose>
                        <c:when test="${user.blocked}">
                            <label style="float: right;">
                                <input type="checkbox" checked="true" id="banCheck${user.id}"
                                       onclick="banUser(${user.id})"/>
                                <fmt:message key="banUser"/>
                            </label>
                        </c:when>
                        <c:otherwise>
                            <label style="float: right;">
                                <input type="checkbox" id="banCheck${user.id}" onclick="banUser(${user.id})"/>
                                <fmt:message key="banUser"/>
                            </label>
                        </c:otherwise>
                    </c:choose>
                    <b>${user.login}</b> (${user.role.name}) ${user.email}<br/>
                        ${user.firstName} ${user.secondName}

                </div>
                <hr/>
            </c:forEach>
            <fmt:message key="next" var="next"/>
            <fmt:message key="previous" var="prev"/>
            <c:url var="searchUri" value="/admin/ban?page=##"/>
            <paginator:display maxLinks="5" currPage="${currPage}" totalPages="${totalPages}" uri="${searchUri}"
                               next="${next}" previous="${prev}"/>
        </div>
    </div>

</div>
<%@ include file="/jsp/jslibs.jsp" %>
</body>
</html>
