<%@ include file="/jsp/jspheaders.jsp" %>
<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title><fmt:message key="title"/></title>
    <%@ include file="/jsp/cssincl.jsp" %>
</head>
<body>

<%@ include file="/jsp/header.jsp" %>
<div id="alert-container"></div>
<div class="content-main">
    <div class="panel panel-primary content-panel">
        <div class="panel-heading">
            <h3 class="panel-title"><fmt:message key="tours"/></h3>
        </div>
        <div class="panel-body">
            <c:forEach items="${requestScope.tours}" var="tour">
                <span style="color: red">
                    <b>${tour.name}</b>
                </span>
                <a href="<c:url value="/admin/editTour?id=${tour.id}"/>">
                    <img src="<c:url value="/images/edit.jpg"/>"/>
                </a>
                <img class="cursorPointer" src="<c:url value="/images/delete.jpg"/>"
                     onclick="deleteTour(${tour.id})"/><br>
                <b><fmt:message key="type"/></b>: ${tour.type.name}<br>
                <b><fmt:message key="desc"/></b>:<br>
                ${tour.description}<br>
                <hr/>
            </c:forEach>
        </div>
    </div>

    <%@ include file="/admin/tour/addTour.jsp" %>
</div>
<%@ include file="/jsp/jslibs.jsp" %>
</body>
</html>
