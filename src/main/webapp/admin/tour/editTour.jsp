<%@ include file="/jsp/jspheaders.jsp" %>
<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title><fmt:message key="title"/></title>
    <%@ include file="/jsp/cssincl.jsp" %>
</head>
<body>

<%@ include file="/jsp/header.jsp" %>

<form method="post" name="update" action="<c:url value="/admin/editTour"/>" class="form-horizontal login-div">
    <fieldset>
        <legend><fmt:message key="edit"/></legend>
        <div class="form-group">
            <label class="col-lg-2 control-label"><fmt:message key="tour"/></label>
            <c:if test="${tourError['tourName']!=null}">
                <label class="col-lg-10 error"><fmt:message key="${tourError['tourName']}"/></label>
            </c:if>
            <div class="col-lg-10">
                <input class="form-control" type="text" name="tourName" maxlength="100"
                       placeholder="<fmt:message key ="tour"/>" value="${editInfo.tourName}">
            </div>
        </div>

        <div class="form-group">
            <label class="col-lg-2 control-label"><fmt:message key="desc"/></label>
            <c:if test="${tourError['description']!=null}">
                <label class="col-lg-10 error"><fmt:message key="${tourError['description']}"/></label>
            </c:if>
            <div class="col-lg-10">
                <textarea class="form-control" placeholder="<fmt:message key ="desc"/>"
                          name="description"
                          maxlength="500" rows="9">${editInfo.description}</textarea>
            </div>
        </div>

        <div class="form-group">
            <label class="col-lg-2 control-label"><fmt:message key="type"/></label>

            <div class="col-lg-10">
                <select class="form-control" id="tourType" name="tourTypeId">
                    <c:forEach items="${editInfo.tourTypes}" var="tourType">
                        <c:choose>
                            <c:when test="${editInfo.selectedTourType.id == tourType.id}">
                                <option selected="selected" value="${tourType.id}">${tourType.name}</option>
                            </c:when>
                            <c:otherwise>
                                <option value="${tourType.id}">${tourType.name}</option>
                            </c:otherwise>
                        </c:choose>
                    </c:forEach>
                </select>
            </div>
        </div>

        <input hidden="hidden" name="tourId" value="${editInfo.tourId}">

        <div class="form-group">
            <div class="col-lg-10 col-lg-offset-2">
                <button type="submit" class="btn btn-primary btn-login"><fmt:message key="save"/></button>
            </div>
        </div>
    </fieldset>
</form>

<%@ include file="/jsp/jslibs.jsp" %>
</body>
</html>