<div id="left-menu">
    <div class="panel panel-primary">
        <div class="panel-heading">
            <h3 class="panel-title"><fmt:message key="addTour"/></h3>
        </div>
        <form method="POST" name="addTour" action="<c:url value="/admin/addTour"/>">
            <div class="panel-body">
                <div class="form-group">
                    <c:if test="${tourError['tourName']!=null}">
                        <label class="col-lg-10 error"><fmt:message key="${tourError['tourName']}"/></label>
                    </c:if>
                    <input class="form-control" type="text" name="tourName" maxlength="100"
                           placeholder="<fmt:message key ="tour"/>"
                           value="${tourName}">
                </div>

                <div class="form-group">
                    <c:if test="${tourError['description']!=null}">
                        <label class="col-lg-10 error"><fmt:message key="${tourError['description']}"/></label>
                    </c:if>
                    <textarea class="form-control"
                              placeholder="<fmt:message key ="desc"/>" name="description"
                              maxlength="500" cols="40" rows="3">${description}</textarea>
                </div>

                <div class="form-group">
                    <label for="tourType"><fmt:message key="type"/></label><br/>
                    <select class="form-control" id="tourType" name="tourTypeId">
                        <c:forEach items="${requestScope.tourTypes}" var="tourType">
                            <c:choose>
                                <c:when test="${selectedTourType.id == tourType.id}">
                                    <option selected="selected" value="${tourType.id}">${tourType.name}</option>
                                </c:when>
                                <c:otherwise>
                                    <option value="${tourType.id}">${tourType.name}</option>
                                </c:otherwise>
                            </c:choose>
                        </c:forEach>
                    </select>
                </div>
                <div class="form-group search">
                    <button type="submit" class="btn btn-success search">
                        <fmt:message key="addTour"/></button>
                </div>
            </div>
        </form>
    </div>
</div>