<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--<jsp:include page="jsp/header.jsp" flush="true"/>--%>
<%@ include file="/jsp/jspheaders.jsp" %>
<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title><fmt:message key="title"/></title>
    <%@ include file="/jsp/cssincl.jsp" %>
</head>
<body>

<%@ include file="/jsp/header.jsp" %>

<div class="content-main">
    <div class="panel panel-primary orders-panel">
        <div class="panel-heading">
            <h3 class="panel-title">
                <label><fmt:message key="tours"/></label>
            </h3>
        </div>
        <div class="panel-body">

            <c:forEach items="${tours}" var="tour">
                <b>${tour.name}</b>
                <c:choose>
                    <c:when test="${tour.hot}">
                        <label style="float: right;">
                            <input type="checkbox" checked="true" onclick="changeHot(${tour.id})"/><fmt:message
                                key="hot"/>
                        </label>
                    </c:when>
                    <c:otherwise>
                        <label style="float: right;">
                            <input type="checkbox" onclick="changeHot(${tour.id})"/><fmt:message key="hot"/>
                        </label>
                    </c:otherwise>
                </c:choose>
                <hr/>
            </c:forEach>
        </div>
    </div>

</div>
<%@ include file="/jsp/jslibs.jsp" %>
</body>
</html>
