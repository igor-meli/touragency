<%@ include file="/jsp/jspheaders.jsp" %>
<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title><fmt:message key="title"/></title>
    <%@ include file="/jsp/cssincl.jsp" %>
</head>
<body>

<%@ include file="/jsp/header.jsp" %>

<form method="post" name="update" action="<c:url value="/admin"/>" class="form-horizontal login-div">
    <fieldset>
        <legend><fmt:message key="profile"/></legend>
        <c:if test="${profileMsg!=null}">
            <label class="col-lg-10">
                <fmt:message key="${profileMsg}"/></label>
        </c:if>
        <div class="form-group">
            <label class="col-lg-2 control-label"><fmt:message key="login"/></label>

            <div class="col-lg-10">
                <label class="form-control">${editInfo.login}</label>
            </div>
        </div>
        <div class="form-group">
            <label class="col-lg-2 control-label"><fmt:message key="password"/></label>
            <c:if test="${profileError['password']!=null}">
                <label class="col-lg-10 error"><fmt:message key="${profileError['password']}"/></label>
            </c:if>
            <div class="col-lg-10">
                <input type="password" class="form-control" name="password" placeholder="<fmt:message
                                        key="password"/>" value="${editInfo.password}">
            </div>
        </div>
        <div class="form-group">
            <label class="col-lg-2 control-label"><fmt:message key="firstName"/></label>
            <c:if test="${profileError['firstName']!=null}">
                <label class="col-lg-10 error"><fmt:message key="${profileError['firstName']}"/></label>
            </c:if>
            <div class="col-lg-10">
                <input type="text" class="form-control"
                       name="firstName" placeholder="<fmt:message key="firstName"/>" value="${editInfo.firstName}">
            </div>
        </div>
        <div class="form-group">
            <label class="col-lg-2 control-label"><fmt:message key="secondName"/></label>
            <c:if test="${profileError['secondName']!=null}">
                <label class="col-lg-10 error"><fmt:message key="${profileError['secondName']}"/></label>
            </c:if>
            <div class="col-lg-10">
                <input type="text" class="form-control"
                       name="secondName" placeholder="<fmt:message key="secondName"/>" value="${editInfo.secondName}">
            </div>

        </div>
        <div class="form-group">
            <label class="col-lg-2 control-label"><fmt:message key="email"/></label>
            <c:if test="${profileError['email']!=null}">
                <label class="col-lg-10 error"><fmt:message key="${profileError['email']}"/></label>
            </c:if>
            <div class="col-lg-10">
                <input type="text" class="form-control"
                       name="email" placeholder="<fmt:message key="email"/>" value="${editInfo.email}">
            </div>
        </div>
        <div class="form-group">
            <div class="col-lg-10 col-lg-offset-2">
                <button type="submit" class="btn btn-primary btn-login"><fmt:message key="save"/></button>
            </div>
        </div>
    </fieldset>
</form>

<%@ include file="/jsp/jslibs.jsp" %>
</body>
</html>
