<%@ taglib prefix="paginator" uri="/WEB-INF/tlds/paginator" %>
<%@ include file="/jsp/jspheaders.jsp" %>
<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title><fmt:message key="title"/></title>
    <%@ include file="/jsp/cssincl.jsp" %>
</head>
<body>

<%@ include file="/jsp/header.jsp" %>
<div class="content-main">
    <div class="panel panel-primary orders-panel">
        <div class="panel-heading">
            <h3 class="panel-title"><fmt:message key="orders"/></h3>
        </div>
        <div class="panel-body">
            <c:forEach items="${requestScope.orders}" var="order">

                <c:set var="panelColor"
                       value="${(order.status.id) eq 1? 'panel-booked':
                       (order.status.id) eq 2?'panel-paid':'panel-canceled'}"
                       scope="page"/>
                <div class="${panelColor} panel-body">
                    <b><fmt:message key="tour"/></b>:
                    <span style="text-decoration: underline;">
                    <a href="<c:url value="/tours?id=${order.offer.tour.id}"/>">${order.offer.tour.name}</a>
                    </span>
                    <label class="price"> ${order.status.name}</label><br/>
                    <b><fmt:message key="hotel"/></b>: ${order.offer.hotel.name}
                    (${order.offer.hotel.hotelType.name})<br>
                    <b><fmt:message key="city"/></b>: ${order.offer.hotel.city.city}
                    (${order.offer.hotel.city.country.country})<br/>
                    <b><fmt:message key="roomType"/></b>: ${order.offer.roomType.name}<br>
                    <b><fmt:message key="adults"/></b>: ${order.offer.roomType.maxAdults}<br/>
                    <b><fmt:message key="children"/></b>: ${order.offer.roomType.maxChildren}<br/>
                    <c:if test="${order.status.id == 1}">
                        <a href="<c:url value="/admin/orders/cancel?id=${order.id}"/>" class="buyButt"><fmt:message
                                key="cancel"/></a>
                    </c:if>
                    <label><fmt:message key="price"/>: ${order.offer.price}$</label><br/>
                    <label class="offerDiscount"><fmt:message key="discount"/>: ${order.discount}%</label><br/>
                    <label class="offerPrice"><fmt:message key="totalPrice"/>: ${order.totalPrice}$</label><br/>

                </div>

                <hr/>
            </c:forEach>

            <fmt:message key="next" var="next"/>
            <fmt:message key="previous" var="prev"/>
            <c:url var="searchUri" value="/admin/orders?page=##"/>
            <paginator:display maxLinks="5" currPage="${currPage}" totalPages="${totalPages}" uri="${searchUri}"
                               next="${next}" previous="${prev}"/>
        </div>
    </div>

</div>
<%@ include file="/jsp/jslibs.jsp" %>
</body>
</html>
