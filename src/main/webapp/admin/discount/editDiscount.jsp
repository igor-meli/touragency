<%@ include file="/jsp/jspheaders.jsp" %>
<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title><fmt:message key="title"/></title>
    <%@ include file="/jsp/cssincl.jsp" %>
</head>
<body>

<%@ include file="/jsp/header.jsp" %>

<form method="post" name="update" action="<c:url value="/admin/editDiscount"/>" class="form-horizontal login-div">
    <fieldset>
        <legend><fmt:message key="editDiscount"/></legend>
        <div class="form-group">
            <label class="col-lg-2 control-label"><fmt:message key="discount"/></label>
            <c:if test="${discountError['discountName']!=null}">
                <label class="col-lg-10 error"><fmt:message key="${discountError['discountName']}"/></label>
            </c:if>
            <div class="col-lg-10">
                <input class="form-control" type="text" name="discountName" maxlength="30"
                       placeholder="<fmt:message key ="discountName"/>"
                       value="${editInfo.discountName}">
            </div>
        </div>
        <div class="form-group">
            <label class="col-lg-2 control-label"><fmt:message key="step"/></label>
            <c:if test="${discountError['step']!=null}">
                <label class="col-lg-10 error"><fmt:message key="${discountError['step']}"/></label>
            </c:if>
            <div class="col-lg-10">
                <input id="intNumberStep" type="number" name="step" min="1" max="100" step="1"
                       value="${editInfo.step eq null ? 1 : editInfo.step}"
                       onkeydown="blockInput()"/>
            </div>
        </div>
        <div class="form-group">
            <label class="col-lg-2 control-label"><fmt:message key="max"/></label>
            <c:if test="${discountError['max']!=null}">
                <label class="col-lg-10 error"><fmt:message key="${discountError['max']}"/></label>
            </c:if>
            <div class="col-lg-10">
                <input id="intNumberMax" type="number" name="max" min="1" max="100" step="1"
                       value="${editInfo.max eq null ? 1 : editInfo.max}"
                       onkeydown="blockInput()"/>
            </div>
        </div>
        <input hidden="hidden" name="id" value="${editInfo.discountId}">

        <div class="form-group">
            <div class="col-lg-10 col-lg-offset-2">
                <button type="submit" class="btn btn-primary btn-login"><fmt:message key="save"/></button>
            </div>
        </div>
    </fieldset>
</form>

<%@ include file="/jsp/jslibs.jsp" %>
</body>
</html>