<%@ include file="/jsp/jspheaders.jsp" %>
<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title><fmt:message key="title"/></title>
    <%@ include file="/jsp/cssincl.jsp" %>
</head>
<body>

<%@ include file="/jsp/header.jsp" %>
<div id="alert-container"></div>
<div class="content-main">
    <div class="panel panel-primary content-panel">
        <div class="panel-heading">
            <h3 class="panel-title"><fmt:message key="discountPrograms"/></h3>
        </div>
        <div class="panel-body">
            <c:forEach items="${requestScope.discounts}" var="discount">
                <b>${discount.name}</b>
                (<fmt:message key="step"/>: ${discount.step}%, <fmt:message key="max"/>: ${discount.max}%)

                <a href="<c:url value="/admin/editDiscount?id=${discount.id}"/>">
                    <img src="<c:url value="/images/edit.jpg"/>"/>
                </a>
                <img class="cursorPointer" src="<c:url value="/images/delete.jpg"/>"
                     onclick="deleteDiscountProgram(${discount.id})"/>
                <br/>
                <hr/>
            </c:forEach>
        </div>
    </div>

    <%@ include file="/admin/discount/addNewDiscount.jsp" %>
</div>
<%@ include file="/jsp/jslibs.jsp" %>
</body>
</html>
