<div id="left-menu">
    <div class="panel panel-primary">
        <div class="panel-heading">
            <h3 class="panel-title"><fmt:message key="addDiscountProgram"/></h3>
        </div>
        <form method="post" name="addDisc" action="<c:url value="/admin/addDiscount"/>">
            <div class="panel-body">
                <div class="form-group">
                    <c:if test="${discountError['discountName']!=null}">
                        <label class="col-lg-10 error"><fmt:message key="${discountError['discountName']}"/></label>
                    </c:if>
                    <input class="form-control" type="text" name="discountName" maxlength="30"
                           placeholder="<fmt:message key ="discountName"/>"
                           value="${requestScope.discountName}">
                </div>
                <div class="form-group">
                    <label for="intNumberStep"><fmt:message key="step"/></label><br/>
                    <c:if test="${discountError['step']!=null}">
                        <label class="col-lg-10 error"><fmt:message key="${discountError['step']}"/></label>
                    </c:if>
                    <input id="intNumberStep" type="number" name="step" min="1" max="100" step="1"
                           value="${requestScope.step eq null ? 1 : requestScope.step}"
                           onkeydown="blockInput()"/>
                </div>
                <div class="form-group">
                    <label for="intNumberMax"><fmt:message key="max"/></label><br/>
                    <c:if test="${discountError['max']!=null}">
                        <label class="col-lg-10 error"><fmt:message key="${discountError['max']}"/></label>
                    </c:if>
                    <input id="intNumberMax" type="number" name="max" min="1" max="100" step="1"
                           value="${requestScope.max eq null ? 1 : requestScope.max}"
                           onkeydown="blockInput()"/>
                </div>

                <div class="form-group search">
                    <button type="submit" class="btn btn-success search"><fmt:message
                            key="addDiscountProgram"/></button>
                </div>
            </div>
        </form>
    </div>
</div>