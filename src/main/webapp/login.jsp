<%@ include file="/jsp/jspheaders.jsp" %>

<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title><fmt:message key="title"/></title>
    <%@ include file="/jsp/cssincl.jsp" %>
</head>
<body>

<%@ include file="/jsp/header.jsp" %>

<form method="post" name="signin" action="<c:url value="/loginUser"/>" class="form-horizontal login-div">
    <fieldset>
        <legend><fmt:message key="sign.in"/></legend>
        <c:if test="${signinError!=null}">
            <label class="col-lg-10 error">
                <fmt:message key="${signinError}"/></label>
        </c:if>
        <div class="form-group">
            <label class="col-lg-2 control-label"><fmt:message key="login"/></label>

            <div class="col-lg-10">
                <input type="text" class="form-control" name="login" placeholder="<fmt:message key="login"/>"
                       value="${login}">
            </div>
        </div>
        <div class="form-group">
            <label class="col-lg-2 control-label"><fmt:message key="password"/></label>

            <div class="col-lg-10">
                <input type="password" class="form-control" name="password" placeholder="<fmt:message key="password"/>">
            </div>
        </div>
        <div class="form-group">
            <div class="col-lg-10 col-lg-offset-2">
                <button type="submit" class="btn btn-primary btn-login"><fmt:message key="sign.in"/></button>
            </div>
        </div>
    </fieldset>
</form>

<%@ include file="/jsp/jslibs.jsp" %>
</body>
</html>
