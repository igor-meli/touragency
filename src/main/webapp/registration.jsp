<%--<jsp:include page="jsp/header.jsp" flush="true"/>--%>
<%@ include file="/jsp/jspheaders.jsp" %>
<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title><fmt:message key="title"/></title>
    <%@ include file="/jsp/cssincl.jsp" %>
</head>
<body>

<%@ include file="/jsp/header.jsp" %>

<form method="post" name="signup" action="<c:url value="/registerUser"/>" class="form-horizontal login-div">
    <fieldset>
        <legend><fmt:message key="sign.up"/></legend>
        <div class="form-group">
            <label class="col-lg-2 control-label"><fmt:message key="login"/></label>
            <c:if test="${signupError['login']!=null}">
                <label class="col-lg-10 error"><fmt:message key="${signupError['login']}"/></label>
            </c:if>
            <div class="col-lg-10">
                <input type="text" class="form-control"
                       name="login" placeholder="<fmt:message key="login"/>" value="${login}">
            </div>
        </div>
        <div class="form-group">
            <label class="col-lg-2 control-label"><fmt:message key="password"/></label>
            <c:if test="${signupError['password']!=null}">
                <label class="col-lg-10 error"><fmt:message key="${signupError['password']}"/></label>
            </c:if>
            <div class="col-lg-10">
                <input type="password" class="form-control" name="password" placeholder="<fmt:message
                                        key="password"/>" value="${password}">
            </div>
        </div>
        <div class="form-group">
            <label class="col-lg-2 control-label"><fmt:message key="firstName"/></label>
            <c:if test="${signupError['firstName']!=null}">
                <label class="col-lg-10 error"><fmt:message key="${signupError['firstName']}"/></label>
            </c:if>
            <div class="col-lg-10">
                <input type="text" class="form-control"
                       name="firstName" placeholder="<fmt:message key="firstName"/>" value="${firstName}">
            </div>
        </div>
        <div class="form-group">
            <label class="col-lg-2 control-label"><fmt:message key="secondName"/></label>
            <c:if test="${signupError['secondName']!=null}">
                <label class="col-lg-10 error"><fmt:message key="${signupError['secondName']}"/></label>
            </c:if>
            <div class="col-lg-10">
                <input type="text" class="form-control"
                       name="secondName" placeholder="<fmt:message key="secondName"/>" value="${secondName}">
            </div>

        </div>
        <div class="form-group">
            <label class="col-lg-2 control-label"><fmt:message key="email"/></label>
            <c:if test="${signupError['email']!=null}">
                <label class="col-lg-10 error"><fmt:message key="${signupError['email']}"/></label>
            </c:if>
            <div class="col-lg-10">
                <input type="text" class="form-control"
                       name="email" placeholder="<fmt:message key="email"/>" value="${email}">
            </div>
        </div>
        <div class="form-group">
            <div class="col-lg-10 col-lg-offset-2">
                <button type="submit" class="btn btn-primary btn-login"><fmt:message key="sign.up"/></button>
            </div>
        </div>
    </fieldset>
</form>

<%@ include file="/jsp/jslibs.jsp" %>
</body>
</html>
