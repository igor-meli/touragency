<div id="navbar" class="navbar navbar-default navbar-fixed-top">
    <div class="container">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse"
                    data-target=".navbar-responsive-collapse">
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="<c:url value="/"/>"><fmt:message key="site"/></a>
        </div>
        <ul class="nav navbar-nav navbar-right">
            <c:choose>
                <c:when test="${sessionScope.User!=null}">
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">${User.firstName} ${User.secondName}
                            <b class="caret"></b></a>
                        <ul class="dropdown-menu">
                            <li><a href="<c:url value="/admin"/>"><fmt:message key="profile"/></a></li>
                            <li><a href="<c:url value="/admin/orders"/>"><fmt:message key="orders"/></a></li>
                            <li><a href="<c:url value="/logout"/>"><fmt:message key="logout"/></a></li>
                            <c:if test="${User.isInRole('Manager') || User.isInRole('Admin')}">
                                <li class="divider"></li>
                                <li class="dropdown-header"><fmt:message key="managerActions"/></li>
                                <li><a href="<c:url value="/admin/hotness"/>"><fmt:message key="cngHot"/></a>
                                </li>
                                <li><a href="<c:url value="/admin/change/status"/>"><fmt:message key="cngStatus"/></a>
                                </li>
                                <li><a href="<c:url value="/admin/discounts"/>">
                                    <fmt:message key="cngDiscountProgram"/></a></li>
                            </c:if>
                            <c:if test="${User.isInRole('Admin')}">
                                <li class="divider"></li>
                                <li class="dropdown-header"><fmt:message key="adminActions"/></li>
                                <li><a href="<c:url value="/admin/ban"/>"><fmt:message key="banUser"/></a></li>
                                <li><a href="<c:url value="/admin/roomtypes"/>"><fmt:message key="rooms"/></a></li>
                                <li><a href="<c:url value="/admin/offers"/>"><fmt:message key="offers"/></a></li>
                                <li><a href="<c:url value="/admin/tours"/>"><fmt:message key="tours"/></a></li>
                            </c:if>
                        </ul>
                    </li>
                </c:when>
                <c:otherwise>
                    <li><a href="<c:url value="/signup"/>"><fmt:message key="sign.up"/></a></li>
                    <li><a href="<c:url value="/login"/>"><fmt:message key="sign.in"/></a></li>
                </c:otherwise>
            </c:choose>
            <li>
                <a href="/ru${requestScope['url']}${requestScope['query']}">
                    <img src="<c:url value="/images/rus.png"/>">
                </a>
            </li>
            <li>
                <a href="/en${requestScope['url']}${requestScope['query']}">
                    <img src="<c:url value="/images/gb.png"/>">
                </a>
            </li>
        </ul>
    </div>
</div>
<div class="main-image">
    <img src="<c:url value="/images/main.jpg"/>" width=100% height=100%>
</div>