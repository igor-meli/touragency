<div id="left-menu">
    <div class="panel panel-primary">
        <div class="panel-heading">
            <h3 class="panel-title"><fmt:message key="search"/></h3>
        </div>
        <form method="get" name="search" onsubmit="return validateSearch()" action="<c:url value="/search/tours"/>">
            <div class="panel-body">
                <div class="form-group">
                    <label for="crit1">
                        <select class="form-control" id="crit1" name="tourType">
                            <option value="">-- <fmt:message key="type"/> --</option>
                            <c:forEach items="${requestScope.tourSearchTypes}" var="tourType">
                                <c:choose>
                                    <c:when test="${requestScope.searchedTourType == tourType.id}">
                                        <option selected="selected" value="${tourType.id}">${tourType.name}</option>
                                    </c:when>
                                    <c:otherwise>
                                        <option value="${tourType.id}">${tourType.name}</option>
                                    </c:otherwise>
                                </c:choose>
                            </c:forEach>
                        </select>
                    </label>
                </div>
                <div class="form-group">
                    <div id="crit2" style="color: red; display: hidden">
                        <input class="form-control" type="text" name="price" maxlength="5"
                               placeholder="<fmt:message key ="price"/>"
                               value="${requestScope.searchedPrice}">
                    </div>
                </div>
                <div class="form-group">
                    <div id="crit3" style="color: red; display: hidden">
                        <input class="form-control" type="text" name="adults" maxlength="2"
                               placeholder="<fmt:message key = "adults"/>" value="${requestScope.searchedAdults}">
                    </div>
                </div>
                <div class="form-group">
                    <div id="crit4" style="color: red; display: hidden">
                        <input class="form-control" type="text" name="children" maxlength="2"
                               placeholder="<fmt:message key = "children"/>" value="${requestScope.searchedChildren}">
                    </div>
                </div>
                <div class="form-group">
                    <label for="crit6">
                        <select class="form-control" id="crit6" name="hotelType">
                            <option value="">-- <fmt:message key="hotel.type"/> --</option>
                            <c:forEach items="${requestScope.hotelSearchTypes}" var="hotelType">
                                <c:choose>
                                    <c:when test="${requestScope.searchedHotelType == hotelType.id}">
                                        <option selected="selected" value="${hotelType.id}">${hotelType.name}</option>
                                    </c:when>
                                    <c:otherwise>
                                        <option value="${hotelType.id}">${hotelType.name}</option>
                                    </c:otherwise>
                                </c:choose>
                            </c:forEach>
                        </select>
                    </label>
                </div>
                <div class="form-group search">
                    <button type="submit" class="btn btn-success search"><fmt:message key="search"/></button>
                </div>
            </div>
        </form>
    </div>
</div>