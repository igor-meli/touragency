<%@ include file="/jsp/jspheaders.jsp" %>
<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title><fmt:message key="title"/></title>
    <%@ include file="/jsp/cssincl.jsp" %>
</head>
<body>

<c:forEach items="${managerUsers}" var="user">
    <b>${user.login}</b> (${user.role.name}) ${user.email}<br/>
    ${user.firstName} ${user.secondName} -  ${user.countApproved}
    <br>
    <hr/>
</c:forEach>


<%@ include file="/jsp/jslibs.jsp" %>
</body>
</html>